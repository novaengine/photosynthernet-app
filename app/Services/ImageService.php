<?php

namespace App\Services;

use App\DTO\ImageDTO;
use App\Events\ImageUploaded;
use App\Exceptions\FileException;
use App\Libraries\SpacesClient;
use App\Models\Album;
use App\Models\AlbumImages;
use App\Models\ColorInfo;
use App\Models\Image;
use App\Models\Url;
use App\Repositories\ImageRepository;
use App\Repositories\Interfaces\ImageRepositoryInterface;
use App\Traits\UsesCache;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageService {

    const STORAGE_PATH = 'public/images';

    public function __construct() {

    }

    public static function create(ImageDTO $image) : array {

        $result = ImageService::uploadOne($image->file);

        $saved_image = new Image();
        $saved_image->title = $image->title;
        $saved_image->description = $image->description;
        $saved_image->user_id = $image->user_id;
        $saved_image->original_file_name = $result['original_file_name'];
        $saved_image->file_name = $result['new_name'];



        if(isset($image->analysis_profile_id)) {
            $saved_image->analysis_profile = $image->analysis_profile_id;
        }

        $saved_image->save();

        $image->original_file_name = $result['original_file_name'];
        $image->file_name = $result['new_name'];
        $image->id = $saved_image->id;


        $url = new Url();
        $url->slug = uniqid();
        $url->image_id = $saved_image->id;
        $url->visibility = $image->visibility;
        $url->save();

        if(isset($image->album_id)) {
            AlbumImages::create([
                'album_id' => $image->album_id,
                'image_id' => $image->id
            ]);
        }


        ImageService::queueImage($image);

        Cache::tags(ImageRepository::USER_CACHE_TAGS)->flush();
        Cache::tags(ImageRepository::PUBLIC_CACHE_TAGS)->flush();

        return [
            'id' => $saved_image->id,
            'slug' => $url->slug
        ];

    }

    public static function queueImage(ImageDTO $image) {
        unset($image->file);
        event(new ImageUploaded($image));
    }

    public static function uploadOne(UploadedFile $file) {

        $original_file_name = $file->getClientOriginalName();

        $error = $file->getError();
        if($error != 0) {
            throw new FileException('File Error : ' . $error);
        }

        $path = $file->storePublicly(self::STORAGE_PATH, ['disk' => 's3', 'bucket' => Config::get('filesystems.disks.s3.bucket')]);

        $path_parts = explode('/', $path);
        $new_name = empty($path_parts) ? '' : $path_parts[count($path_parts) - 1];

        return [
            'original_file_name' => $original_file_name,
            'new_name' => $new_name
        ];
    }

    public function uplaodMany() {

    }

}