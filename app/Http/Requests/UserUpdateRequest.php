<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserUpdateRequest extends FormRequest {

    public function rules() {

        return [
            'data.name' => 'max:255',
            'data.reset_token' => 'max:255'
        ];

    }

}