<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Image;
use App\Models\Url;
use App\Models\UserResetToken;
use App\Services\StorageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{

    public function index(Request $request){
        return view('welcome')->with([
            'settings' => [
                'module' => 'index'
            ]
        ]);
    }

    public function image(Request $request, $slug) {
        $schema_data = [
            '@context' => 'https://schema.org',
            '@type' => 'Image',
            '@graph' => []
        ];

        $meta = [
            'title' => '',
            'description' => '',
        ];
        $graph = [];

        $url = Url::where('slug', $slug)->where('visibility', 'public')->first();


        if(!empty($url)) {
            $images = Image::with('url')->whereHas('url', function($query) use($slug) {
                $query = $query->where('slug', $slug);
                return $query;
            });

            $images = $images->first();

            if(!empty($images->images)) {
                $meta['title'] = 'Album : ' . $images->title;
                $meta['description'] = $images->description;
                foreach($images->images as $image) {
                    $url = $base_url = Config::get('app.url');
                    $graph[] = [
                        '@type' => 'Photograph',
                        'name' => $image->title,
                        'description' => $image->description,
                        'image' => $url . '/storage/images/' . $image->file_name,
                        'thumbnailUrl' => $url . '/storage/images/thumbs/' . $image->file_name,
                    ];
                }
            } else {
                $url = $base_url = Config::get('app.url');
                $meta['title'] = 'Image : ' . $images->title;
                $meta['description'] = $images->description;
                $graph[] = [
                    '@type' => 'Photograph',
                    'name' => $images->title,
                    'description' => $images->description,
                    'image' => $url . '/storage/images/' . $images->file_name,
                    'thumbnailUrl' => $url . '/storage/images/thumbs/' . $images->file_name,
                ];
            }

            $schema_data['@graph'] = $graph;


        }




        return View::make('welcome')->with([
            'schema_data' => $schema_data,
            'meta' => $meta,
            'settings' => [
                'module' => 'single_image'
            ]
        ]);
    }

    public function album(Request $request, $slug) {
        $schema_data = [
            '@context' => 'https://schema.org',
            '@type' => 'ImageGallery',
            '@graph' => []
        ];

        $meta = [
            'title' => '',
            'description' => '',
        ];
        $graph = [];

        $url = Url::where('slug', $slug)->where('visibility', 'public')->first();


        if(!empty($url)) {

            $images = Album::with(['url', 'images', 'images.url'])->whereHas('url',function($query) use($slug) {
                $query = $query->where('slug', $slug);
                return $query;
            });

            $images = $images->first();

            if(!empty($images->images)) {
                $meta['title'] = 'Album : ' . $images->title;
                $meta['description'] = $images->description;
                foreach($images->images as $image) {
                    $url = $base_url = Config::get('app.url');
                    $graph[] = [
                        '@type' => 'Photograph',
                        'name' => $image->title,
                        'description' => $image->description,
                        'image' => $url . '/storage/images/' . $image->file_name,
                        'thumbnailUrl' => $url . '/storage/images/thumbs/' . $image->file_name,
                    ];
                }
            }

            $schema_data['@graph'] = $graph;
        }




        return View::make('welcome')->with([
            'schema_data' => $schema_data,
            'meta' => $meta,
            'settings' => [
                'module' => 'single_image',
                'is_album' => true,
            ]
        ]);
    }

    public function login(Request $request) {

        return View::make('welcome')->with([
            'settings' => [
                'module' => 'login'
            ]
        ]);
    }

    public function upload(Request $request) {

        return View::make('welcome')->with([
            'settings' => [
                'module' => 'upload'
            ]
        ]);
    }

    public function search(Request $request) {

        return View::make('welcome')->with([
            'settings' => [
                'module' => 'search'
            ]
        ]);
    }

    public function register(Request $request) {

        return View::make('welcome')->with([
            'settings' => [
                'module' => 'register'
            ]
        ]);
    }

    public function showStudio(Request $request) {

        return View::make('welcome')->with([
            'settings' => [
                'module' => 'studio',
                'cloud_url' => StorageService::getServingPath()
            ]
        ]);
    }

    public function showForgot(Request $request) {

        $settings = [
            'module' => 'forgot',
        ];


        if($request->has('token')) {
            $token = $request->get('token');

            $reset_token = UserResetToken::where('token', $token)
                ->first();

            if(!empty($reset_token)) {
                $settings['token'] = $reset_token->token;
                $settings['user_id'] = $reset_token->user_id;
            }

        }

        return View::make('welcome')->with([
            'settings' => $settings
        ]);
    }

    /**
     * @param Request $request
     *
     * If they can get through to this method then they have
     * successfully made it through the middleware, meaning their token is valid
     *
     * @return \Illuminate\Http\Response
     */
    public function validateToken(Request $request) {

        if(Auth::check()) {
            return response()->json([
                'id' => (string) Auth::user()->id
            ], 200);
        }

        return response()->json([], 401);
    }

    public function profile() {
        return View::make('welcome')->with([
            'settings' => [
                'module' => 'profile'
            ]
        ]);
    }

    public function logout() {
        return View::make('logout');
    }
}
