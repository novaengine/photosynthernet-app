<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use OpenApi\Annotations as OA;
use App\Http\Requests\AnalysisProfileRequest;
use App\Models\AnalysisProfile;
use App\Repositories\Interfaces\ImageRepositoryInterface;
use App\Traits\JSONAPI;
use App\Traits\ManageFiles;
use App\Traits\Pagination;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AnalysisProfileController extends Controller
{
    use Pagination, JSONAPI, ManageFiles;

    /**
     * @var ImageRepositoryInterface
     */
    private $imageRepository;

    const DEFAULT_PER_PAGE = 12;
    const DEFAULT_FIRST_PAGE = 1;

    public function __construct(ImageRepositoryInterface $imageRepository) {
        $this->imageRepository = $imageRepository;
    }

    public function index(Request $request){
        $retval = [
            'data' => [],
        ];

        $user = Auth::user();
        $user_id = $user->id;

        $profiles = AnalysisProfile::where('user_id', $user_id)->get();
        $retval['data'] = $profiles;

        return response()->api($retval);
    }

    public function read(Request $request, $id){

        return response([], Response::HTTP_NOT_IMPLEMENTED);
    }


    /**
     *
     * @OA\Post(
     *      path="/analysis-profiles",
     *      operationId="create@AnalysisProfileController",
     *      tags={"Analysis Profiles"},
     *      summary="Creates an Analysis Profile",
     *      description="Creates an analysis profile, which is a set of configuration
     *                   options that determine the core algorithm used to generate the palette
     *                   for all uploaded images.",
     *
     *      @OA\RequestBody(
     *          required=true,
     *          description="The analysis Profile",
     *          @OA\JsonContent(
     *              required={"data.name", "data.segments"},
     *              @OA\Property(
     *                  property="data",
     *                  type="object",
     *                  @OA\Property(
     *                      property="name",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="segments",
     *                      type="integer"
     *                  )
     *              )
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful Analysis Profile Response",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="object",
     *                  description="The primary id of the created analysis profile",
     *                  @OA\Property(
     *                      property="id",
     *                      type="integer"
     *                  )
     *              )
     *          )
     *      )
     *
     *
     * )
     *
     * @param Request $request
     * @param $slug
     * @return mixed
     */
    public function create(AnalysisProfileRequest $r) {

        $user = Auth::user();

        $segments = !empty($r->input('data.segments')) ? $r->input('data.segments') : 1000;
        $superpixel_algorithm = 'slic';

        $analysis_profile = new AnalysisProfile();
        $analysis_profile->name = $r->input('data.name');
        $analysis_profile->segments = $segments;
        $analysis_profile->superpixel_algorithm = $superpixel_algorithm;
        $analysis_profile->user_id = $user->id;
        $analysis_profile->save();

        return response([
            'data' => [
                'id' => $analysis_profile->id
            ]
        ], Response::HTTP_CREATED);
    }

    public function update(Request $r, $id) {

        return response([], Response::HTTP_NOT_IMPLEMENTED);
    }

    public function delete(Request $r, $id) {

        return response([], Response::HTTP_NOT_IMPLEMENTED);
    }

}
