<?php namespace App\Traits;


use Illuminate\Support\Facades\Config;

trait Pagination {

    public static function getPagination($current_page, $base_url = '', $total_count = 500, $limit = 20) {

        $first_page = 1;
        $next_page = $current_page + 1;
        $prev_page = $current_page - 1;
        $last_page = ceil($total_count / $limit);

        if($prev_page < 1) {
            $prev_page = 1;
        }

        if($prev_page > $last_page) {
            $prev_page = $last_page - 1;
        }

        if($next_page > $last_page) {
            $next_page = $last_page;
        }

        if($current_page > $last_page) {
            $current_page = $last_page;
        }

        $middle = self::getMiddlePages($last_page, $current_page);

        $url = Config::get('app.url');
        $retval = [
            'page' => [
                'total_records' => $total_count,
                'current_page' => $current_page,
                'current_limit' => $limit,
                'first' => $first_page,
                'middle' => $middle
            ],
            'links' => [
                'first' => $url . $base_url . '?page=' . $first_page . '&limit=' . $limit
            ]
        ];

        // This is my code I can be as clever or as dumb as I like
        // I leave deciding if the below is clever or dumb as an exercise to the reader
        $build = ['prev', 'next', 'last'];
        foreach($build as $key) {
            $var = $key . '_page';
            $retval['page'][$key] = $$var;
            $retval['links'][$key] = $url . $base_url . '?page=' . $$var . '&limit=' . $limit;
        }

        foreach($middle as $p) {
            $retval['links']['middle'][] = $url . $base_url . '?page=' . $p . '&limit=' . $limit;
        }

        return $retval;

    }

    private static function getMiddlePages($last_page, $current_page) {
        $middle = [];
        if($last_page > 8) {

            $middle_page = $current_page;
            $mid_prev = $middle_page - 1;
            $mid_next = $middle_page + 1;

            if($mid_prev < 2) {
                $mid_prev = 2;
                $mid_next = 3;
            }

            if($mid_next >= $last_page - 1) {
                $mid_prev = $last_page - 2;

                if($current_page == $last_page - 2) {
                    $mid_prev = $last_page - 3;
                }

                $mid_next = $last_page - 1;
            }

            for($i = $mid_prev; $i <= $mid_next; $i++) {
                $middle[] = $i;
            }
        } else {
            $middle_page = $current_page;
            $mid_prev = $middle_page - 1;
            $mid_next = $middle_page + 1;

            if($mid_prev < 2) {
                $mid_prev = 2;
                $mid_next = 3;
            }

            if($mid_next >= $last_page) {
                $mid_next = $last_page - 1;
            }

            for($i = $mid_prev; $i <= $mid_next; $i++) {
                $middle[] = $i;
            }

        }

        return $middle;
    }

}
