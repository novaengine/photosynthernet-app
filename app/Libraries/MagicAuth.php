<?php namespace App\Libraries;

use App\Models\Client;
use App\User;
use Illuminate\Auth\TokenGuard;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;

class MagicAuth extends TokenGuard {


    protected $user;
    protected $client;
    protected $request;

    /**
     * Create a new authentication guard.
     *
     * @param UserProvider $p
     * @param Request $request
     */
    public function __construct(UserProvider $p, Request $request)
    {
        parent::__construct($p, $request);
        $this->request  = $request;
        $this->user     = null;
        $this->client   = null;
    }

    /**
     * @return User
     */
    public function getUser() : User {
        return $this->user;
    }


    public function token() {
        $bearer = $this->request->bearerToken();
        $token_id = (new \Lcobucci\JWT\Parser())->parse($bearer);
        return $token_id;
    }

    public function user() {

        $bearer = $this->request->bearerToken();

        if(empty($bearer)) {
            return new \stdClass();
        }

        $token_id = (new \Lcobucci\JWT\Parser())->parse($bearer);

        $audience = $token_id->getClaim('aud');
        $subject = $token_id->getclaim('sub');

        if(!empty($subject)) {
            if(!empty($this->user) && $this->user->id == $subject) {
                // We have the user, just return the user
                return self::getUser();
            }

            $this->user = User::find($subject);

            return $this->user;
        }

        if(!empty($audience)) {

            if(!empty($this->client) && $this->client->id == $audience) {
                return self::getUser();
            }
            $client = Client::find($audience);
            $this->client = $client;

            $this->user = $client->user;

            return $this->user;
        }

    }
}