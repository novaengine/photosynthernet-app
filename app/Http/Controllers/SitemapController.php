<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Image;
use App\Models\Url;
use App\Models\UserResetToken;
use App\Repositories\Interfaces\ImageRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;

class SitemapController extends Controller {

    /**
     * @var ImageRepositoryInterface
     */
    private $imageRepository;


    public function __construct(ImageRepositoryInterface $imageRepository) {
        $this->imageRepository = $imageRepository;

    }

    public function index(Request $request){
        $this->imageRepository->setLimit(100);
        $this->imageRepository->getAllPublicModel();
        $total_public_images = $this->imageRepository->getCount();
        $total_public_images = $total_public_images > 0 ? $total_public_images : 1;

        $pages = ceil($total_public_images / $this->imageRepository->getLimit());

        $res = View::make('sitemapindex')->with([
            'pages' => $pages
        ]);


        return response($res)->header('Content-Type', 'application/xml');
    }

    public function map(Request $request, $page){

        $this->imageRepository->setPage($page);
        $this->imageRepository->setLimit(100);

        $images = $this->imageRepository->getPublic();

        $res = View::make('sitemap')->with([
            'images' => $images
        ]);
        return response($res)->header('Content-Type', 'application/xml');
    }

    public function robots(Request $request){


        return View::make('robots');

    }
}
