<?php namespace App\Virtual\Requests;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UserRegistration",
 *     description="Required user info to register",
 *     @OA\Info(
 *      title="Requests",
 *     )
 * )
 */
class UserRegistration
{

    /**
     * @OA\Property(
     *     title="data",
     *     description="Required user information",
     *     type="object",
     *     format="document",
     *     @OA\Property(
     *         property="email",
     *         description="RFC5322 compliant email address : https://www.rfc-editor.org/rfc/rfc5322.txt",
     *         example="test@example.com",
     *         type="string",
     *         format="email"
     *     ),
     *     @OA\Property(
     *         property="password",
     *         description="Minimum 8 character password",
     *         example="AReallyStrongPassword",
     *         type="string"
     *     ),
     *     @OA\Property(
     *         property="password_confirmation",
     *         description="Confirm the password property by replicating it exactly here",
     *         example="AReallyStrongPassword",
     *         type="string"
     *     ),
     *     @OA\Property(
     *         property="terms",
     *         description="Please confirm you've read and agree to the terms and conditions of using the site here : https://k8s.photosynthernet.dev/terms",
     *         example=true,
     *         type="bool"
     *     )
     * )
     *
     * @var string
     */
    public $data = [];

}