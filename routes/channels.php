<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('image_updates.{id}', function ($user, $id) {
    var_dump($user, $id);
    return (int) $user->id === (int) $id;
});


Broadcast::channel('test', function () {
    return true;
});
