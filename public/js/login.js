const login = {
    $parent_container : undefined,

    init($parent_container) {
        const self = this;

        self.$parent_container = $parent_container

        $('.main-container').addClass('fancy-bg');

        self.renderLogin();
    },

    bindActions() {
        const self = this;

        const $login = $('[data-js="login"]');
        $login.unbind();
        $login.on('click', function() {
            self.performLogin();
        });

        const $email = $('[data-js="email"], [data-js="password"]');
        $email.unbind();
        $email.on('keyup', function(e) {

            if(e.which === 13) {
                self.performLogin();
            }

        });

    },

    getLoginCredentials() {
        const ids = ['email', 'password'];
        let data = {};

        for(let i in ids) {
            const id = ids[i];
            const $el = $('[id="'+id+'"]');

            data[id] = $el.val();
        }

        return data;
    },

    performLogin() {
        const self = this;
        let creds = self.getLoginCredentials();

        const prms = api.post('/login', {
            data : {
                email : creds.email,
                password : creds.password,
            }
        });

        prms.done(function(r) {

            app.state.user = r.data;
            app.saveState();

            let success_message = 'Successfully logged in dude. Good Job!';
            window.setTimeout(function() {
                toast.rise(success_message)
            }, 750)
            window.location.href = '/?toast=' + encodeURIComponent(success_message);

        });

        prms.catch(function(data) {
            let payload = self.getLoginCredentials();
            payload.status = data.status;
            payload.response = data.responseJSON;

            payload = app.cleanupErrors(payload);

            self.renderLogin(payload)
        })

    },

    renderLogin(data) {
        const self = this;

        data = data || {};

        self.$parent_container.render('login', data).done(function() {
            self.bindActions();
        });

    }

}

modules.login = login;