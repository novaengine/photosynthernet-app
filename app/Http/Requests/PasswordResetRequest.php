<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PasswordResetRequest extends FormRequest {

    public function rules() {

        return [
            'email' => 'required',
        ];

    }

}