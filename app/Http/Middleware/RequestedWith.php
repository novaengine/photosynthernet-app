<?php

namespace App\Http\Middleware;

use Closure;


class RequestedWith {
    public function handle($request, Closure $next, ...$guards) {
        $request->headers->set('X-Requested-With', 'XMLHttpRequest');

        return $next($request);
    }
}
