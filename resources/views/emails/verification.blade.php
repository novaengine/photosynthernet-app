<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8"/>
</head>
<body>
<h2>Verify Your Email Address</h2>
<a href="{{$url}}/api/users/{{$user_id}}/verify?verification_token={{$token}}">Click here to verify your email!</a>

<h3>Why verify?</h3>

<p>
    Verifying your email allows us to ensure that you own the email address you are trying to register as. This feature
    grants you the security that nobody else will be able to make an account and pose as your given online identity.
</p>

<p>
    Secondly, once you verify, you are then allowed to upload images to the photosynthernet service!
</p>

</body>
</html>