<?php

namespace App\Console\Commands;

use App\Events\ImageUpdate;
use App\Events\PaletteGenerated;
use App\Models\Image;
use App\Repositories\ImageRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class PaletteSubscriber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:subscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        while(true) {
            try {
                $response = Redis::blpop('test-channel', 5);
                if(!empty($response)) {
                    $message = $response[1];
                    $message = json_decode($message, true);

                    $palette = $message['palette'];
                    $image_id = $message['image_id'];

                    $image = Image::where('id', $image_id)->first();

                    $meta = $image->meta;
                    if(!empty($message['meta'])) {
                        $meta = array_merge($message['meta'], $meta);
                    }

                    var_dump('palette',$palette, $image_id);

                    DB::update('UPDATE images SET hex_codes = ?, meta = ? WHERE id = ?', [json_encode($palette), json_encode($meta), $image_id]);

                    $user_id = DB::select('SELECT * FROM images WHERE id = ?', [$image_id])[0]->user_id;

                    var_dump('user_id', $user_id);

                    $conn = Redis::connection('pubsub');
                    $res = $conn->publish($user_id, json_encode([
                        'c' => 'palette_finished',
                        'palette' => $palette,
                    ]));

                    var_dump('res', $res);

                    Cache::tags(ImageRepository::USER_CACHE_TAGS)->flush();
                    Cache::tags(ImageRepository::PUBLIC_CACHE_TAGS)->flush();

                    event(new PaletteGenerated());

                }
            } catch (\Exception $e) {
                var_dump(get_class($e), $e->getMessage());
                break;
            }
        }
    }
}