# Communicating Status of Palette Generation and more in realtime

* Status: draft
* Deciders: James Horton
* Date: 2020-09-29

Technical Story: https://gitlab.com/Valeness/photosynthernet-app/-/issues/14

## Context and Problem Statement

When users upload an image and are waiting for their palette to be generated they
have to refresh the page manually until it is updated. This should be served up 
realtime so not only will they see the palette when it is ready, but also see 
the status of the palette generation so they will have no need to continually refresh
the page.


## Decision Drivers <!-- optional -->

* Users are confused when they upload an image and don't see a palette 
* Users are refreshing the page unnecessarily creating server load
* James wants to lol

## Considered Options

* Redis-backed Golang Websocket Server
* Long Polling 
* MQTT Server

## Decision Outcome

Chosen option: TBD

### Positive Consequences <!-- optional -->

* The technology stack will be able to facilitate realtime communication to the user 
  for other issues

### Negative Consequences <!-- optional -->

* Additional infrastructure/GCP spend
* Increased System Complexity

## Pros and Cons of the Options <!-- optional -->

### Redis-backed Golang Websocket Server

Pros
* Golang has a good concurrency model that lends itself to high throughput 
  and scalability within message-centric environments
* Golang StdLib will be able to facilitate websocket connections
* Backed by Redis, which is already a large part of the messaging stack

Cons
* Lots of work will need to be done from the ground up
* No mature boilerplate frameworks so edge cases may not be initially accounted for

### Long Polling

Pros
* Can be done with just the existing api and no additional infrastructure
* Can be scaled horizontally with the existing api

Cons
* Long Polling occupies an open HTTP connection while no messages are being sent
* HTTP is not the most bandwidth-efficient protocol so 
  message throughput is limited
* 

### MQTT

Pros
* Mature and Battle Tested Protocol
* Lightweight Messages allow for very high throughput

Cons
* Websocket instrumentation is not built-in and is instead provided via a third 
  party plugin requiring manual compilation
* Broker performance may suffer to ensure message persistence, which interferes with 
  the demands of QOS 1 and 2
* Additional Infrastructure

## Links 

