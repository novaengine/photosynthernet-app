@extends('layouts.main')

@section('content')
@parent

<div class="container login-container"></div>

@endsection


@section('scripts')

    <script>

        const login = {

            init() {
                const self = this;
                self.render()
            },

            render(data) {
                const self = this;

                if(data === undefined) {
                    data = {}
                }

                $('.login-container').render('login', data).done(function (r) {
                    self.bindActions();
                    app.bindActions();
                })
            },

            loginSuccess() {
                const self = this;

                self.render({
                    success : true,
                    email : email,
                });
                window.setTimeout(function() {
                    window.location.href = '/';
                }, 1000);
            },

            loginFail(e) {
                const self = this;

                console.log(e);

                if(e.status === 401) {
                    const email = $('[name=email]').val();
                    console.log(email);
                    console.log(self);
                    self.render({
                        email : email,
                        error : "Incorrect Username or Email"
                    });
                }
            },

            attemptLogin() {
                const self = this;

                const email = $('[name=email]').val();
                const password = $('[name=password]').val();

                app.login(email, password)
                    .done(self.loginSuccess.bind(self))
                    .fail(self.loginFail.bind(self))
                ;
            },

            bindActions() {
                const self = this;

                const $login = $('[id=login]');
                $login.unbind();
                $login.on('click', self.attemptLogin.bind(self))

                const $login_form = $('[name=email], [name=password]');

                $login_form.on('keyup', function(e) {
                    if(e.which !== 13) {
                        return false;
                    }

                    self.attemptLogin();
                })

            }
        }

        login.init();

    </script>

@endsection
