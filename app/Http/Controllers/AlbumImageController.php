<?php

namespace App\Http\Controllers;

use App\Console\Commands\PaletteSubscriber;
use App\DTO\BaseDTO;
use App\DTO\ImageDTO;
use App\Events\ImageUpdate;
use App\Events\ImageUploaded;
use App\Jobs\SaveImage;
use App\Models\Album;
use App\Models\AlbumImages;
use App\Models\Image;
use App\Models\Url;
use App\Repositories\Interfaces\ImageRepositoryInterface;
use App\Services\ImageService;
use App\Services\StorageService;
use App\Traits\JSONAPI;
use App\Traits\ManageFiles;
use App\Traits\Pagination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;

class AlbumImageController extends Controller
{
    use Pagination, JSONAPI, ManageFiles;

    public function __construct(ImageRepositoryInterface $imageRepository) {
        $this->imageRepository = $imageRepository;
    }

    public function index(Request $r, $slug) {
        $retval = [
            'data' => [],
            'meta' => [
                'filepath' => Config::get('filesystems.image_storage_path'),
                'cloud' => [
                    'url' => StorageService::getServingPath(),
                ]
            ],

        ];

        $album = Album::with(['url'])->whereHas('url', function($query) use ($slug) {
            $query = $query->where('slug', $slug);
            return $query;
        })->get();

        $album = $album->first();

        if(empty($album)) {
            return response([], 404);
        }

        $images = $this->imageRepository->getSingle($slug, true);
        $url = $album->url[0];

        if(empty($images)) {
            return response([], 404);
        }

        $image_visibility = $url->visibility;

        if(Auth::check() && $image_visibility == 'private') {
            $user_id = $r->user()->id;

            if($user_id != $album->user_id) {

                return response([
                    'errors' => [
                        [
                            'title' => 'Not Found',
                            'status' => 404
                        ]
                    ]
                ], 404);
            }

            $retval['data'] = $images;
            $retval['meta']['title'] = $album->title;

        } else if(!Auth::check() && $image_visibility == 'private') {

            return response([
                'errors' => [
                    [
                        'title' => 'Not Found',
                        'status' => 404
                    ]
                ]
            ], 404);
        } else if($image_visibility == 'public') {

            $retval['data'] = $images;
            $retval['meta']['title'] = $album->title;
        }

        return response()->api($retval);

    }

    public function create(Request $r, $slug) {

        $album = Album::with(['url'])->whereHas('url', function($query) use ($slug) {
            $query = $query->where('slug', $slug);
            return $query;
        })->get();

        $album = $album->first();

        $image_dto = new ImageDTO($r->all());
        $image_dto->setUserId($r->user()->id);
        $image_dto->setAlbumId($album->id);

        $upload_result = ImageService::create($image_dto);


        return response()->json($upload_result);

    }

    public function createAlbumImage(Request $r, $slug) {

        $album = new Album;
        $album->title = $r->get('title');
        $album->description = $r->get('description');
        $album->user_id = Auth::user()->id;
        $album->save();

        $visibility = empty($r->get('visibility')) ? 'private' : $r->get('visibility');


        $url = new Url();
        $url->slug = uniqid();
        $url->image_id = $album->id;
        $url->visibility = $visibility;
        $url->is_album = true;
        $url->save();

        return [
            'id' => $album->id,
            'slug' => $url->slug
        ];

    }

}
