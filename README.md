# Photosynthernet
Photosynthernet is an image hosting platform that will determine the color palette of any uploaded image.

![Single Page](https://k8s.photosynthernet.dev/storage/images/YLY7jb1CPr7K5HAPYmtGE9nOuvtT1sSH3uTB8eob.png)
![Superpixel](https://k8s.photosynthernet.dev/storage/images/DydGfx7sTUOiXXr9Lc2I9OfffN1Qo380Wmomgr7J.png)
![Listing Page](https://k8s.photosynthernet.dev/storage/images/tAoOHsh8BoS3eSGjJ2OsVthCEozYX0xnOAI8hGN7.png)

## Documentation 
![Entity Relationship Diagram](/docs/diagrams/erd.png)
![Kubernetes Infrastructure Diagram](/docs/diagrams/infra.png)

### Honorable Mentions
This project has a lot to do with color science, which involves a lot more mathematics than I am
able to understand and process on my own. For this I stand on the shoulders of classical giants that 
know FAR more than I do on these topics. The following list serves to recognize these giants.

* Peter Chamberlain 
    * https://forum.blackmagicdesign.com/viewtopic.php?f=21%5C&t=40284
    * You can find his response in docs/lut_file_format_explanation.md in the event the forum 
    goes down
* Vaguilera 
    * For their Color Curve Library here : https://github.com/vaguilera/ColorCurve
    * License Attached within /public/js/vendor/curve.license.md
* Compuphase for their work on color distance and palette extraction
    * https://www.compuphase.com/cmetric.htm
* cameramanben for his work on LUT File generation (Seriously, nobody deals with 1D Luts these days...)
    * http://cameramanben.github.io/LUTCalc/LUTCalc/index.html
