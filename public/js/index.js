const index = {

    $parent_container : undefined,
    scroll_timeout : undefined,

    visibility : 'public',

    init($parent_container) {
        const self = this;

        self.$parent_container = $parent_container

        let page = 1;
        if(self.settings.page !== undefined) {
            page = self.settings.page;
        }

        history.replaceState({action : 'render_images', location : $parent_container.selector, page : page}, '', app.getUrl() + '?page=' + page)
        self.renderImages(page);
    },

    renderImages(page) {
        const self = this;
        PImage.index(page, self.visibility).done(function(r) {

            self.renderWithData(r);

        });
    },

    lazyLoadImageRender() {
        $('img[data-src]').each(function() {

            const top = $(this).offset().top;
            let thresh = (window.pageYOffset + window.innerHeight);
            // Threshold is 20% more than the height of the window to provide a buffer to load
            // before the user actually scrolls there
            thresh += (thresh * 0.2);

            if(top <= thresh && !$(this).attr('src')) {
                $(this).attr('src', $(this).data('src'));
            }

        });
    },

    renderWithData(data) {
        const self = this;

        let total = 0
        let remaining = 0;
        for(let i in data.data) {
            let image = data.data[i];
            let size_class = 3
            if(image.attributes.meta.aspect_ratio < 1) {
                size_class = 2
            } else if(image.attributes.meta.aspect_ratio >= 1.7) {
                size_class = 4
            }
            remaining = 12 - total;

            if(remaining < size_class) {
                // I don't fit here
                data.data[i - 1].attributes.meta.size_class += remaining;
                data.data[i].attributes.meta.size_class = size_class;
                total = 0
            } else {
                // I fit
                data.data[i].attributes.meta.size_class = size_class;
            }
            total += size_class

            data.data[i].attributes.meta.remaining = remaining;
            data.data[i].attributes.meta.total = total;


            if(total >= 12) {
                total = 0;
            }


        }

        self.$parent_container.render('welcome', data).done(function(r) {
            self.bindActions();

            self.lazyLoadImageRender();

        })
    },

    bindActions() {
        const self = this;

        app.bindLazyLoad();

        const $pagination = $('.pagination a');
        $pagination.unbind();
        $pagination.on('click', function() {
            const page = $(this).data('page');
            self.renderImages(page)

            const url = window.location.origin + window.location.pathname;
            history.pushState({action : 'render_images', page : page}, '', url + '?page=' + page)

        })
    },

}

modules.index = index;