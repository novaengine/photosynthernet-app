FROM php:7.4-fpm-alpine3.13

ENV TZ=UTC

RUN export RUNLEVEL=1 && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apk -q update && \
    apk -q add gnupg git zip unzip wget libpng-dev jpeg-dev libxml2-dev postgresql-dev && \
    docker-php-ext-configure gd --with-jpeg && \
    docker-php-ext-install gd pgsql xml pdo pdo_pgsql pcntl

RUN apk add --no-cache \
        $PHPIZE_DEPS && \
        apk add nodejs yarn

RUN pecl install redis && docker-php-ext-enable redis

## Install when you want to enable cachegrind traces
## Or don't, this is a bad idea on prod
#RUN apk add --no-cache $PHPIZE_DEPS \
#    && pecl install xdebug \
#    && docker-php-ext-enable xdebug \
#    && apk add kcachegrind

ARG composer_url=https://raw.githubusercontent.com/composer/getcomposer.org/ba0141a67b9bd1733409b71c28973f7901db201d/web/installer

#RUN DEBIAN_FRONTEND=noninteractive apt-get install -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" php-pear php7.3-dev cron && \
#    mkdir -p /var/www/
#
#RUN pecl install redis
#
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH=$PATH:vendor/bin
#
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer && \
    curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig && \
    php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" && \
    php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer --version=2.0.4 && \
    rm -f /tmp/composer-setup.*
#
RUN mkdir -p /var/www && chown -R www-data:www-data /var/www
#
USER www-data
WORKDIR /var/www
#
COPY composer.json composer.json
COPY composer.lock composer.lock
COPY gulpfile.js gulpfile.js
RUN mkdir -p database/seeds && mkdir -p database/factories && mkdir -p database/migrations
COPY artisan artisan
RUN composer install --no-autoloader

COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn install

COPY --chown=www-data:www-data ./ /var/www/

RUN yarn production

# This will run the autoload and scripts and all that stuff since we already installed the packages above
RUN composer install

USER root
COPY build/config/www.conf /usr/local/etc/php-fpm.d/www.conf
ENV DEBIAN_FRONTEND=noninteractive

COPY build/config/php.ini /usr/local/etc/php/php.ini

RUN mkdir -p /shared

ADD build/entrypoint.sh /entrypoint.sh
ADD build/worker/entrypoint.sh /worker-entrypoint.sh
ADD build/subscriber/entrypoint.sh /subscriber-entrypoint.sh
ADD build/jobs/entrypoint.sh /jobs-entrypoint.sh

COPY build/shared/ /shared

RUN ls -l /shared

RUN chmod +x /entrypoint.sh && \
    chmod +x /worker-entrypoint.sh && \
    chmod +x /jobs-entrypoint.sh && \
    chmod +x /subscriber-entrypoint.sh && \
    chmod +x /shared/* && \
    chmod +x /var/www/build/run/tests.sh

RUN apk -q add --update bash

ENTRYPOINT ["/entrypoint.sh"]
