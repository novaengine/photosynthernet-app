/**
 * Polyfills
 */
// Function for string formatting. Use like
// 'This is a {0} string that can be {1} {2}.'.format('Cool', 'formatted')
// Output >> This is a Cool string that can be formatted {2}.
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number] : match;
        });
    };
}

/**
 * Handlebars Extensions
 */

Handlebars.registerHelper('json', function(context) {
    return JSON.stringify(context);
});

Handlebars.registerHelper('noteq', function(a, b, options) {
    console.log(a != b)
    return a != b ? options.fn(this) : '';
});
Handlebars.registerHelper('ifeq', function(a, b, opts) {
    if(a == b)
        return opts.fn(this);
    else
        return opts.inverse(this);
})

Handlebars.registerHelper('ifmod', function(a, b, opts) {
    if((a + 1) % b === 0)
        return opts.fn(this);
    else
        return opts.inverse(this);
})

Handlebars.registerHelper('toRGB', function(a, b, opts) {
    let c;
    a = '#' + a;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(a)){
        c= a.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return [(c>>16)&255, (c>>8)&255, c&255].join(',');
    }
})

/**
 * JQuery Extensions
 */

jQuery.fn.extend({
    render : function(template_name, data) {

        let $sel = $(this);

        const is_user_logged_in = app.isUserLoggedIn();
        const is_mobile = window.innerWidth < 1024;
        const is_upload_page = modules['upload'].initialized;

        const injected_data = {
            is_user_logged_in : is_user_logged_in,
            is_mobile : is_mobile,
            is_upload_page : is_upload_page,
        }
        // console.log(injected_data)

        return templates.compile(template_name, {
            ...data,
            ...injected_data
        }).done(function(r) {
            $sel.html(r);
        })
    },

    loading(is_loading) {
        if(is_loading) {
            $(this).attr('data-old-val', $(this).html())
            $(this).html('<i class="fas fa-circle-notch fa-spin"></i>');
        } else {
            $(this).html($(this).attr('data-old-val'));
        }
    }
    ,

    progressBar : function(settings) {
        const bar = progress.create($(this), settings);
        bar.render();

        return bar;
    }
})

jQuery.fn.findExclude = function( selector, mask, result ) {
    var result = typeof result !== 'undefined' ? result : new jQuery();
    this.children().each( function(){
        var thisObject = jQuery( this );
        if( thisObject.is( selector ) )
            result.push( this );
        if( !thisObject.is( mask ) )
            thisObject.findExclude( selector, mask, result );
    });
    return result;
}

const control = {

    data : {},
    default_state : {
        layout : 'list',
        active : false
    },

    store_key : 'control',
    active_class : 'control-panel-active',

    init() {
        const self = this;

        // Get Control Info from Store
        let data = app.store.get(self.store_key);
        if(data !== null) {
            self.data = data;
        } else {
            self.data = self.default_state;
        }

        self.save();

        // Define Container
        self.$parent_container = $('[data-js=control_panel_container]');
    },

    bindActions() {
        const self = this;

        const $control_panel = $('[data-js=control_panel]');
        $control_panel.unbind();
        $control_panel.on('click', function(e) {
            let is_closable = $(e.target).data('closable') === 'closable' || $(e.target).attr('fill') === 'currentColor';

            let was_active = $(this).hasClass(self.active_class) || $(this).hasClass(self.active_class + '-rerender');

            if((was_active && is_closable) || !was_active) {
                if($(this).hasClass(self.active_class)) {
                    $(this).toggleClass(self.active_class);
                } else if($(this).hasClass(self.active_class + '-rerender')) {
                    $(this).toggleClass(self.active_class + '-rerender');
                } else {
                    $(this).toggleClass(self.active_class);
                }
                self.data.active = !was_active;
            }

            self.save();


        })

        const $change_layout = $('[data-js="change_layout"]');
        $change_layout.unbind();
        $change_layout.on('click', function() {
            let choosen_layout = $(this).data('layout');
            self.data.layout = choosen_layout;
            self.data.rerender = true;

            observer.emit('single_image', 'layout_change', {
                layout : choosen_layout
            })

            self.render();
            self.save();
        })
    },

    save() {
        const self = this;
        app.store.put(self.store_key, self.data);
    },


    render() {
        const self = this;

        // self.$parent_container.render('control_panel', self.data).done(function(r) {
        //     self.bindActions();
        //     self.data.rerender = false;
        // })
    }
}

const debug = {

}

const templates = {
    list : {},

    partial_map : {
        image : [
            'palette',
            'color_info'
        ],
        search : [
            'color_picker',
            'palette',
            'image_list'
        ],
        welcome : [
            'image_list'
        ]
    },

    compile(template_name, data) {
        const self = this;
        const prms = $.Deferred();

        console.log("Compiling Template : " + template_name);

        if(self.list[template_name] === undefined) {

            if(self.partial_map[template_name] !== undefined)  {

                const last_prms = $.Deferred();
                let remaining = self.partial_map[template_name].length;

                $.get('/templates/'+template_name+'.handlebars').done(function(r) {

                    let prmses = [];
                    $.each(self.partial_map[template_name], function(ind, val) {
                        let res = $.get('/templates/partial_'+val+'.handlebars').done(function(result) {
                            console.log("Registering Partial " + val)
                            console.log("Remaining " + remaining)

                            let text = result;
                            let tmpl = Handlebars.compile(text);
                            self.list['partial_' + val] = tmpl

                            Handlebars.registerPartial(val, result);

                            remaining--;
                            if(remaining <= 0) {
                                last_prms.resolve();
                            }

                        });
                        prmses.push(res)
                    })

                    last_prms.done(function() {

                        console.log('Final Partial Rendered')

                        let text = r;
                        let tmpl = Handlebars.compile(text);
                        self.list[template_name] = tmpl
                        prms.resolve(tmpl(data))
                    })
                });

            } else {
                $.get('/templates/'+template_name+'.handlebars').done(function(r) {
                    let text = r;
                    let tmpl = Handlebars.compile(text);
                    self.list[template_name] = tmpl
                    prms.resolve(tmpl(data))
                })
            }
        } else {
            prms.resolve(self.list[template_name](data));
        }

        return prms;
    },
}

const nav = {
    render(selector, data) {
        const self = this;
        const $sel = $(selector);
        $sel.render('nav_actions', data).done(function() {
            self.bindActions();
        })

        self.renderMobile();

    },

    renderMobile() {
        const self = this;

        const $mobile_sel = $('[data-js="mobile_nav_container"]');
        $mobile_sel.render('mobile_nav').done(function() {
            self.bindActions();
        });


    },

    bindActions() {
        const self = this;

        const $search_input = $('[id=search]');
        $search_input.unbind('input');
        $search_input.on('keypress', function(e) {
            if(e.which == 13) {
                const query = $('[id="search"]').val();
                PImage.search(query).done(function(r) {

                    for(let i in r.data) {
                        const image = r.data[i]
                        r.data[i] = PImage.isPublic(image);

                        console.log("meg", r.data[i]);

                    }

                    index.$parent_container = app.$parent_container;
                    index.renderWithData(r);
                });
            }
        })

        const $search = $('#submit_search');
        $search.unbind();
        $search.bind('click', function (e) {
            const query = $('[id="search"]').val();
            PImage.search(query).done(function(r) {

                for(let i in r.data) {
                    const image = r.data[i]
                    r.data[i] = PImage.isPublic(image);

                    console.log("meg", r.data[i]);

                }

                index.$parent_container = app.$parent_container;
                index.renderWithData(r);
            });
        });
    }
}

const communication = {
    sock : undefined,

    events : {},

    init() {
        const self = this;

        self.sock = new WebSocket("wss://" + window.location.host + "/ws");

        self.sock.onopen = function (event) {

            console.log("Opening websocket");

            self.sock.send(JSON.stringify({
                content : "heartbeat"
            }));
        };

        self.registerEvent('palette_generation', function(message) {
            try {
                let content = message.content;
                content = JSON.parse(content);

                let command = content.c;
                switch(command) {
                    case 'd':
                        if(app.settings.module !== 'single_image') {
                            break;
                        }
                        $('.load-message').text('Determining Dominant Palette Colors...')
                        break;

                    case 'palette_finished':

                        if(app.settings.module !== 'single_image') {
                            break;
                        }
                        single_image.renderImage();

                        break;

                    case 'e':

                        if(app.settings.module !== 'single_image') {
                            break;
                        }

                        const hex_codes = content.palette;
                        const render_data = {
                            attributes : {
                                hex_codes : hex_codes
                            }
                        }

                        const image_id = content.image_id;

                        $('[data-js="palette"][data-id="'+image_id+'"]').render('partial_palette', render_data)


                        break;
                    default:
                        if(app.settings.module !== 'single_image') {
                            break;
                        }
                        $('.load-message').text('');
                        break;

                }

                // image.data.attributes.hex_codes = content.palette
                // image.renderPalette(image.data)

            } catch(e) {
                console.log("Message content is not valid JSON", message.content)
                console.error(e);
            }

        });

        self.listen();

    },

    registerEvent(key, event) {
        const self = this;

        self.events[key] = event;
    },

    listen() {
        const self = this;

        self.sock.onmessage = function (event) {
            try {
                const message = JSON.parse(event.data);
                console.log("Message: ", message);

                for(let event_key in self.events) {

                    try {
                        let callable = self.events[event_key];
                        callable(message)
                    } catch(e) {
                        console.error(e);
                    }

                }

            } catch(e) {
                console.log('Websocket event is not valid JSON', event.data);
                console.error(e)
                return false;
            }
        }
    }

}

const api = {

    headers : {},

    init(auth_token) {
        const self = this;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        if(auth_token !== undefined) {
            self.headers['Authorization'] = 'Bearer ' + auth_token;
        }

    },

    get(slug, data) {
        const self = this;

        return $.ajax({
            url : '/api' + slug,
            data : data,
            type : 'get',
            headers : self.headers
        })
    },

    post(slug, data) {
        const self = this;

        return $.ajax({
            url : '/api' + slug ,
            type : 'post',
            data : data,
            headers : self.headers
        });
    },

    patch(slug, data) {
        const self = this;

        return $.ajax({
            url : '/api' + slug ,
            type : 'patch',
            data : data,
            headers : self.headers
        });
    },

    file(slug, data) {
        const self = this;

        return $.ajax({
            url : '/api' + slug,
            type : 'post',
            headers : self.headers,
            cache : false,
            contentType : false,
            processData : false,
            data : data,
        });
    },

    delete(slug) {
        const self = this;

        return $.ajax({
            url : '/api' + slug,
            type : 'delete',
            headers : self.headers
        })
    }

}

const app = {

    resize_timeout : undefined,

    settings : {
        module : ''
    },

    state : {

        user : {
            token : '',
        }

    },

    store : {
        get(key) {
            return JSON.parse(window.localStorage.getItem(key))
        },

        put(key, data) {
            window.localStorage.setItem(key, JSON.stringify(data))
        },
    },

    init() {
        const self = this;

        self.setSettings();
        self.loadState();

        $('.main-container').removeClass('fancy-bg');

        const is_logged_in = self.isUserLoggedIn();
        let module = self.settings.module;
        self.$parent_container = $('.app-container');

        modules[module].settings = self.settings;
        modules[module].initialized = true;

        nav.render('.main-actions',{
            logged_in : is_logged_in
        });

        let url = new URL(window.location.href);

        if(url.searchParams.has('toast')) {
            let toast_message = url.searchParams.get('toast');
            toast.rise(toast_message);
        }

        if(url.searchParams.has('page')) {
            const page = url.searchParams.get('page');
            if(!isNaN(page)) {
                self.settings.page = page;
            }
        }

        let auth_token;
        if(is_logged_in) {
            document.cookie = 'X-Authorization=' + self.state.user.token + '; path=/';
            auth_token = self.state.user.token;
        }

        api.init(auth_token);

        communication.init();

        self.bindActions();


        modules[module].init(self.$parent_container);

        control.init();
        control.render();

    },

    bindLazyLoad() {
        const self = this;

        const $document = $(document);
        $document.unbind('scroll');
        $document.on('scroll', function(){
            window.clearTimeout(self.scroll_timeout);

            self.scroll_timeout = window.setTimeout(function() {
                index.lazyLoadImageRender();
            }, 70);

        });
    },

    dragged_over : false,
    dragleave_timeout : 0,

    bindActions() {
        const self = this;

        const $main_container = $('[data-js=main_container], [data-js="global_image_upload"]');
        $main_container.unbind();
        $main_container.on('dragover', function(e) {
            e.preventDefault();
            e.stopPropagation();
            window.clearTimeout(self.dragleave_timeout);

            self.dragleave_timeout = window.setTimeout(function() {
                $('[data-js=global_image_upload]').removeClass('global-image-upload-active');
                self.dragged_over = false;
            }, 200)
            if(self.dragged_over) {
                return true;
            }
            self.dragged_over = true;
            $('[data-js=global_image_upload]').addClass('global-image-upload-active');

        }).on("dragleave", function(e) {
            e.preventDefault();
            e.stopPropagation();
        });


        $main_container.on('drop', function(e) {
            if(e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files.length) {
                e.preventDefault();
                e.stopPropagation();

                let files = e.originalEvent.dataTransfer.files;
                const url = window.location.origin + '/upload';
                history.pushState({action : 'render_upload', files : files}, 'Photosynthernet | Upload', url)
                modules['upload'].$parent_container = self.$parent_container;
                modules['upload'].loadLibraries().done(function(r) {
                    modules['upload'].readyImages(files);
                });
            }
        })

        const $history = $(window);
        $history.unbind('popstate');
        $history.on('popstate', function(event) {

            const state = event.originalEvent.state;

            console.log(state);

            if(state !== null && state.action !== undefined) {

                switch(state.action) {
                    case 'render_images':

                        index.renderImages(state.page)

                        break;
                }
            }

            return false;
        });

        const $window = $(window);
        $window.unbind('resize');
        $window.on('resize', function() {

            if(self.resize_timeout) {
                window.clearTimeout(self.resize_timeout);
            }

            self.resize_timeout = window.setTimeout(function() {
                nav.renderMobile();

                if(self.settings.module === 'index') {
                    index.lazyLoadImageRender();
                }

            }, 40);

        });

    },

    isUserLoggedIn() {
        const self = this;

        return self.state.user.token !== '' && self.state.user !== false;
    },

    logout() {
        const self = this;
        const prms = api.post('/logout');
        prms.done(function(r) {
            self.state.user = false;
            self.state.user.token = false;

            self.saveState();

            window.location.href = '/';
        });

        return prms;
    },

    loadState() {
        const self = this;
        let stored_state = self.store.get('state');

        if(stored_state !== undefined && stored_state !== null) {
            self.state = stored_state;
        }

    },

    saveState() {
        const self = this;
        self.store.put('state', self.state)
    },

    setSettings() {
        const self = this;

        const path = self.getUrlParts()

        self.settings.path = path;
    },

    getUrl() {
        const url = window.location.origin + window.location.pathname;

        return url;
    },

    getUrlParts() {
        const self = this;
        const url = window.location.pathname;

        // noinspection UnnecessaryLocalVariableJS
        const parts = url.split('/');

        return parts;
    },

    loadLibraries(scripts, styles) {
        const prms = $.Deferred();
        const all_promises = [];

        if(!scripts) {
            scripts = [];
        }

        if(!styles) {
            styles = [];
        }

        for(let i in scripts) {
            const script = scripts[i];
            const res = $.getScript(script);
            all_promises.push(res);
        }

        for(let i in styles) {
            const style = styles[i];
            $('<link/>', {
                rel: 'stylesheet',
                type: 'text/css',
                href: style
            }).appendTo('head');
        }


        $.when(...all_promises).done(function() {
            prms.resolve();
        });

        return prms;
    },

    cleanupErrors(payload) {
        let errors = {};
        for(let key in payload.response.errors) {
            let error = payload.response.errors[key];
            let truncated_name = key.replace('data.', '');

            if(typeof error === 'array') {
                for(let i in error) {
                    error[i] = error[i].replace('data.', '');
                }
            }

            errors[truncated_name] = error;
        }

        payload.response.errors = errors;

        return payload;
    },

}

/**
 * Utilities
 */

const progress = {
    template : '<progress class="progress is-success" value="{{value}}" max="{{max}}">{{value}}%</progress>',

    settings : null,
    $el : null,

    create($el, settings) {
        const self = this;

        if(settings === undefined) {
            settings = {};
        }
        settings.max = settings.max || 100;
        settings.value = settings.value || 0;

        self.tmpl = Handlebars.compile(self.template);
        self.settings = settings;
        self.$el = $el;

        return Object.assign({}, self);
    },

    remove() {
        const self = this;
        $('.progress', self.$el).remove();
    },

    render() {
        const self = this;
        const html = self.tmpl(self.settings);

        self.$el.html(html);
    },

    increment(step) {
        const self = this;
        step = step || 1;

        self.settings.value += step;
        $('.progress', self.$el).val(self.settings.value);
    }
}

const toast = {
    template : '<div id="toast-{{id}}" class="notification {{class}}"><button data-id="{{id}}" class="delete" data-js="dismiss"></button>{{message}}</div>',

    /**
     * Function name by Tanner Lueders
     * @param message
     * @param callback
     */
    rise(message, settings, callback) {
        const self = this;

        if(settings === undefined) {
            settings = {};
        }

        const uniqid = Date.now();

        const tmpl = Handlebars.compile(self.template);
        const html = tmpl({
            message : message,
            id : uniqid,
            'class' : settings.class || 'is-success'
        });

        $('body').append(html);
        self.bindActions();

        window.setTimeout(function() {
            $('[id="toast-'+uniqid+'"]').addClass('slide-in')
        }, 2);

        window.setTimeout(function() {
            $('[id="toast-'+uniqid+'"]').removeClass('slide-in');
        }, settings.timeout || 2000)

    },

    bindActions() {

        const $toast_dismiss = $('[data-js="dismiss"]');
        $toast_dismiss.unbind();
        $toast_dismiss.on('click', function() {
            const id = $(this).data('id');

            $('[id="toast-'+id+'"]').remove();

        })
    }
}

const timer = {

    alarms : {},

    default : 300,

    cancel_flag : 'alarm_cancel',

    async _set(module, duration) {
        const self = this;

        if(self.alarms[module] !== undefined) {
            self.alarms[module].cancel();
        } else {
            delete self.alarms[module];
        }

        const alarm = self.createAlarm(duration);

        self.alarms[module] = alarm;
        await alarm;

        return true;
    },

    set(module, method, duration) {
        const self = this;
        const alarm = self._set(module, duration);

        if(duration === undefined) {
            duration = self.default;
        }

        // Set the module to something random if it is not set
        // It should always be set though, idk why I put this here
        if(module === undefined) {
            module = 'default';
        }

        alarm.then(function() {
            method();
        }).catch(function (err) {
            if(err === self.cancel_flag) {
                console.warn('Alarm Was Cancelled');
            }
        });

        return alarm;
    },

    createAlarm(duration) {
        const self = this;

        // Create a secondary promise that enables creation of a public rejection method
        let resolve, reject;
        const alarm = new Promise((r, rej) => {
            resolve = r;
            reject = rej;
        });

        alarm._timeout = setTimeout(function() {
            resolve();
        }, duration);

        // Custom Promise Rejection!
        alarm.cancel = function() {
            reject(self.cancel_flag);
        }

        return alarm;

    }

}

const router = {

    routes : {
        '/search' : 'search',
    },

    route(uri) {
        const self = this;

        let module = self.routes[uri];

        if(module === undefined) {
            console.error('Module for uri '+uri+' not found')
            return false;
        }

        modules[module].render();

    }

}

const observer = {

    bus : {},

    emit(channel, message, data) {
        const self = this;

        if(self.bus[channel] === undefined) {
            return false;
        }

        for(let subscriber in self.bus[channel]) {
            self.bus[channel][subscriber].callback(message, data, self.bus[channel][subscriber]);
            self.bus[channel][subscriber].stats.invocations += 1;
        }

    },

    unsubscribe(channel, ref) {
        const self = this;
        if(self.bus[channel] === undefined) {
            return true;
        }

        for(let i in self.bus[channel]) {
            console.log("Ref Battle!!")
            console.log(self.bus[channel][i].ref, ref)
            if(self.bus[channel][i].ref === ref) {
                delete self.bus[channel][i]
            }
        }
    },

    subscribe(channel, callback) {
        const self = this;

        if(self.bus[channel] === undefined) {
            self.bus[channel] = [];
        }

        try {
            throw new Error();
        } catch(e) {
            console.log(e.caller);
        }

        let sub = {
            ref : Array(13).join((Math.random().toString(36)+'00000000000000000').slice(2, 18)).slice(0, 12),
            callback : callback,
            stats : {
                invocations : 0,
            }
        }

        self.bus[channel].push(sub);
        return sub
    },

}

let modules = {}
