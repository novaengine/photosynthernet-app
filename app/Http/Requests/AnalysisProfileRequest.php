<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class AnalysisProfileRequest extends FormRequest {

    public function rules() {

        return [
            'data.name' => 'required|string',
            'data.segments' => 'integer',
        ];

    }

}