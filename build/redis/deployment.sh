# Authenticate with doctl
doctl auth init -t="$DOCTL_TOKEN"
doctl kubernetes cluster kubeconfig save $CLUSTER_ID

## Enables envsubst
apk update && apk add gettext

# Do the deployment
cat build/redis/deployment.yml | envsubst | kubectl apply -f -
cat build/redis/service.yml | envsubst | kubectl apply -f -
#kubectl rollout restart deployment ps-redis
