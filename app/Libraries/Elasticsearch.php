<?php namespace App\Libraries;

use App\Libraries\Interfaces\SearchDriver;
use Elasticsearch\Client;
use Illuminate\Support\Facades\Config;
use Elasticsearch\ClientBuilder;

class Elasticsearch implements SearchDriver {

    /**
     * @var Client
     */
    private static $client;

    /**
     * @return Client
     */
    public static function getClient() : Client {
        return self::$client;
    }

    /**
     * Set client to null to sever the connection to elasticsearch
     */
    public static function disconnect() : void {
        self::$client = null;
    }

    /**
     * @param string $connection
     * @throws \Exception
     *
     * Attempt to set the client property using the configuration stored in config/services.php
     *
     * @see Client
     *
     */
    public static function setClient(string $connection = 'default') : void {
        $config = Config::get('services.elasticsearch.' . $connection);

        if(empty($config)) {
            throw new \Exception('Elasticsearch not configured for this connection');
        }

        $host = $config['host'];
        $port = $config['port'];

        $connection_string = $host . ':' . $port;

        self::$client = ClientBuilder::create()->setHosts(['http://' . $connection_string])->build();
    }

    /**
     * @param string $connection_name
     * @return Client
     * @throws \Exception
     */
    public static function connection(string $connection_name = 'default') : Client {

        if(!isset(self::$client)) {
            self::setClient($connection_name);
        }

        return self::getClient();

    }

    /**
     * @param string $index_key
     * @return bool
     */
    public static function checkIfIndexExists(string $index_key): bool
    {
        return self::$client->indices()->exists([
            'index' => $index_key
        ]);
    }
}