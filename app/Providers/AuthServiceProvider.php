<?php

namespace App\Providers;

use App\Http\Auth\MagicUserProvider;
use App\Libraries\MagicAuth;
use Illuminate\Auth\DatabaseUserProvider;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Passport;
use Laravel\Passport\PassportUserProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Auth::extend('magic', function ($app, $name, array $config) {

            $user_provider = new DatabaseUserProvider(DB::connection(), new BcryptHasher(), 'users');

            return new MagicAuth(new PassportUserProvider($user_provider, 'passport'), $app->make('request'));
        });

    }
}
