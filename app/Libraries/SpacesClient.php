<?php namespace App\Libraries;

use App\Libraries\Interfaces\SearchDriver;
use Elasticsearch\Client;
use Illuminate\Support\Facades\Config;
use Elasticsearch\ClientBuilder;
use \Spaces as DOSpaces;
use Space;


class SpacesClient implements SearchDriver {

    /**
     * @var ?Space
     */
    private static ?Space $client;

    /**
     * @return Space
     */
    public static function getClient() : Space {
        return self::$client;
    }

    /**
     * Set client to null to sever the connection to elasticsearch
     */
    public static function disconnect() : void {
        self::$client = null;
    }

    /**
     * @param string $connection
     * @throws \Exception
     *
     * Attempt to set the client property using the configuration stored in config/services.php
     *
     * @see Space
     *
     */
    public static function setClient(string $connection = 'default') : void {
        $config = Config::get('services.spaces.' . $connection);

        if(empty($config)) {
            throw new \Exception('DigitalOcean Spaces is not configured for this connection');
        }

        $api_key = $config['api_key'];
        $api_secret = $config['api_secret'];
        $space_name = $config['name'];

        $Spaces = new DOSpaces($api_key, $api_secret);
        $Space = $Spaces->space($space_name, 'sfo3');

        self::$client = $Space;
    }

    /**
     * @param string $connection_name
     * @return Space
     * @throws \Exception
     */
    public static function connection(string $connection_name = 'default') : Space {

        if(!isset(self::$client)) {
            self::setClient($connection_name);
        }

        return self::getClient();

    }
}