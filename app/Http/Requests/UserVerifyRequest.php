<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserVerifyRequest extends FormRequest {

    public function rules() {

        return [
            'verification_token' => 'required'
        ];

    }

}