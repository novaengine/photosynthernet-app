<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class UploadLimit
{
    /**
     * Modify the upload limit for php
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param int $limit
     * @return mixed
     */
    public function handle($request, Closure $next, $limit = 100)
    {
        ini_set('upload_max_filesize', $limit);
        ini_set('post_max_size ', $limit);

        return $next($request);
    }
}
