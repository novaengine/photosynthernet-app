<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    @if(!empty($meta) && !empty($meta['title']))
        <title>Photosynthernet {{$meta['title']}}</title>
    @else
        <title>Photosynthernet</title>
    @endif
{{--    <link rel="stylesheet" href="https://unpkg.com/bulma@0.8.1/css/bulma.min.css"/>--}}
{{--    <script src="https://kit.fontawesome.com/f47243f9bb.js" crossorigin="anonymous"></script>--}}
{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma-accordion@2.0.1/dist/css/bulma-accordion.min.css" integrity="sha256-k/VMvlKcclhSysYXbH8Yx+jwieT5h0yEbL3sd2N8BNw=" crossorigin="anonymous">--}}
{{--    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.css">--}}


    <meta name="csrf-token" content="{{ csrf_token() }}">


{{--    @if(\Illuminate\Support\Facades\App::environment() === 'local')--}}
{{--        <link type="text/css" href="/css/app.css" rel="stylesheet">--}}
{{--    @else--}}
        <link type="text/css" href="/css/app-min.css" rel="stylesheet">
{{--    @endif--}}

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="Palette Generation, Color Extraction, Image Host, Perceptually Uniform Color Space">

    @if(!empty($schema_data) && !empty($schema_data['@graph']))
        <meta property="og:image" content="{{$schema_data['@graph'][0]['thumbnailUrl']}}">
        <meta property="og:image:type" content="image/jpeg">
        <meta property="og:image:width" content="250">
        <meta property="og:image:height" content="250">
    @endif

    @if(!empty($meta))
        <meta name="description" content="{{$meta['description']}}">
    @endif


</head>