<?php

namespace App\Http\Controllers;

use App\Http\Requests\AnalysisProfileRequest;
use App\Models\ColorInfo;
use App\Traits\JSONAPI;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ColorInfoController extends Controller
{
    use JSONAPI;

    public function index(Request $request){
        $retval = [
            'data' => [],
        ];

        $color_info = ColorInfo::select(['id', 'name', 'hex'])->with([
            'related.color' => function($query) {
            $query->select('id', 'name', 'hex');
        }])->has('related')->get();
        $retval['data'] = $color_info;

        return response()->api($retval);
    }

    public function read(Request $request, $id){

        return response([], Response::HTTP_NOT_IMPLEMENTED);
    }

    public function create(AnalysisProfileRequest $r) {

    }

    public function update(Request $r, $id) {

        return response([], Response::HTTP_NOT_IMPLEMENTED);
    }

    public function delete(Request $r, $id) {

        return response([], Response::HTTP_NOT_IMPLEMENTED);
    }

}
