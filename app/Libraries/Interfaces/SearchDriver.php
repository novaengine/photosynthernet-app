<?php namespace App\Libraries\Interfaces;

interface SearchDriver {

    /**
     * @return mixed
     */
    public static function getClient();

    /**
     * Sever the connection to search provider
     */
    public static function disconnect() : void;

    /**
     * @param string $connection
     */
    public static function setClient(string $connection = 'default') : void;

    /**
     * @param string $connection_name
     * @return mixed
     */
    public static function connection(string $connection_name = 'default');

}