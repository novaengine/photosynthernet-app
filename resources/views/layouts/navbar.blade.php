<!-- START NAV -->

<nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item" href="/">
            <img src="/assets/images/logo-beta.png" width="224" height="28">
        </a>

        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>

    <div id="navbarBasicExample" class="navbar-menu">
        <div class="navbar-start">

            <div class="navbar-item field">
                <p class="control has-icons-right">
                    <input id="search" class="input" type="text" placeholder="Search by Hex Value" style="border-top-right-radius: 0; border-bottom-right-radius: 0;">
                </p>

                <div class="buttons">
                    <button id="submit_search" style="border-top-left-radius: 0; border-bottom-left-radius: 0;" class="button is-primary">Search</button>
                </div>

            </div>
        </div>

        <div class="navbar-end">
            <div class="navbar-item">
                <div class="buttons main-actions">
                    <a class="button is-primary" href="/login">
                        <strong>Login</strong>
                    </a>

                    <a class="button is-secondary" href="/register">
                        <strong>Register</strong>
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- END NAV -->
