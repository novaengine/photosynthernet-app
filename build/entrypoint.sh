#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
sh /var/www/buildenv.sh

php /var/www/artisan key:generate
php /var/www/artisan config:cache

php /var/www/artisan migrate

php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"
php artisan l5-swagger:generate

php-fpm -F -R