sh build/shared/clusterlogin.sh
sh build/shared/runtime-dependencies.sh

# Do the deployment
cat build/worker/deployment.yml | envsubst | kubectl apply -f -
kubectl rollout restart deployment photosynthernet-worker
