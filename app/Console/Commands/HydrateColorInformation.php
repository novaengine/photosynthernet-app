<?php

namespace App\Console\Commands;

use App\Libraries\Elasticsearch;
use App\Models\ColorInfo;
use App\Models\ColorRelationship;
use App\Models\Image;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PHPUnit\Util\Color;
use Symfony\Component\Console\Helper\ProgressBar;

class HydrateColorInformation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:colors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch color information and store in our database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle() {

        // Get the color info json

        $this->info('Hydrating Color Information from Color-Names list');
        $success = file_put_contents('/tmp/colors.json', file_get_contents('https://raw.githubusercontent.com/meodai/color-names/master/dist/colornames.json'));

        // Load the file into memory

        $colors = file_get_contents('/tmp/colors.json');
        $colors = json_decode($colors);

        $this->info('Loaded Color List Into Memory');

        DB::beginTransaction();

        ColorInfo::truncate();

        $this->info('Truncated Color Info Database');

        // Seed the initial basic colors
        $basic = [
            [
                'name' => 'Blue',
                'hex' => '0000FF',
            ],
            [
                'name' => 'Brown',
                'hex' => '964B00',
            ],
            [
                'name' => 'Red',
                'hex' => 'FF0000',
            ],
            [
                'name' => 'Purple',
                'hex' => '800080',
            ],
            [
                'name' => 'Yellow',
                'hex' => 'FF0000',
            ],
            [
                'name' => 'Pink',
                'hex' => 'FFC0CB',
            ],
            [
                'name' => 'Green',
                'hex' => '00FF00',
            ],
            [
                'name' => 'Teal',
                'hex' => '008080',
            ],
            [
                'name' => 'Orange',
                'hex' => 'FF8000',
            ],
        ];

        $basic_color_names = array_map(function($item) {
            return strtolower($item);
        }, array_column($basic, 'name'));

        $qs = [];
        $query_data = [];
        foreach($basic as $color) {
            $qs[] = '(?, ?, ?, ?)';
            $query_data[] = $color['name'];
            $query_data[] = str_replace('#', '', $color['hex']);
            $query_data[] = Carbon::now();
            $query_data[] = Carbon::now();
        }

        DB::insert('INSERT INTO color_info ("name", "hex", "created_at", "updated_at") VALUES ' . implode(', ', $qs), $query_data);

        // Get the basic colors ids
        $basic_color_info = ColorInfo::all();
        $formatted_basic_color_info = [];
        $basic_color_ids = [];
        foreach($basic_color_info as $color) {
            $color_name = strtolower($color->name);
            $formatted_basic_color_info[$color_name] = $color->id;
            $basic_color_ids[] = $color->id;
        }

        $this->info('Configured Basic Colors (Like Red, Blue, Green, etc.)');

        $this->info('Beginning Insert into color_info Database');

        $p = $this->output->createProgressBar();
        $p->start(count($colors));

        $colors_chunk = array_chunk($colors, 200);
        foreach($colors_chunk as $k => $color_chunk) {
            $qs = [];
            $query_data = [];
            foreach($color_chunk as $color) {
                $qs[] = '(?, ?, ?, ?)';
                $query_data[] = $color->name;
                $query_data[] = str_replace('#', '', $color->hex);
                $query_data[] = Carbon::now();
                $query_data[] = Carbon::now();
            }

            DB::insert('INSERT INTO color_info ("name", "hex", "created_at", "updated_at") VALUES ' . implode(', ', $qs), $query_data);

            $p->advance(count($color_chunk));

        }

        $p->finish();
        echo PHP_EOL;

        DB::commit();

        $this->info('Hydrating Color Relationships');

        $offset = 0;
        $limit = 500;
        $color_info_model = ColorInfo::whereNotIn('id', $basic_color_ids)->offset($offset)->limit($limit);
        $color_info = $color_info_model->get();

        $p = $this->output->createProgressBar();
        $p->start(ColorInfo::whereNotIn('id', $basic_color_ids)->count());

        ColorRelationship::truncate();

        $exceptions = [
            'earth' => 'brown'
        ];

        while($color_info->count() > 0) {

            $relationships_query_data = [];
            $relationships_qs = [];

            foreach($color_info as $color) {
                $color_name = strtolower($color->name);
                foreach($basic_color_names as $basic_name) {

                    $basic_name = strtolower($basic_name);

                    $name_parts = explode(' ', $color_name);
                    foreach($name_parts as $part) {

                        $is_exception = !empty($exceptions[$part]);
                        if($is_exception) {
                            $basic_name = $exceptions[$part];
                        }

                        if($part === $basic_name || $is_exception) {
                            $relationships_qs[] = '(?, ?)';
                            $relationships_query_data[] = $color->id;
                            $relationships_query_data[] = $formatted_basic_color_info[$basic_name];
                        }

                        if($is_exception) {
                            break;
                        }

                    }


                }
                $p->advance();
            }

            if(!empty($relationships_query_data)) {
                DB::beginTransaction();

                DB::insert('INSERT INTO color_relationships ("origin_color_id", "related_color_id") VALUES ' . implode(', ', $relationships_qs), $relationships_query_data);

                DB::commit();
            }

            $offset += $limit;
            $color_info = $color_info_model->offset($offset)->get();
        }

        $p->finish();
        echo PHP_EOL;

        // Cleanup
        unlink('/tmp/colors.json');

    }

}
