<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class VerifiedEmail
{
    /**
     * Modify the upload limit for php
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param int $limit
     * @return mixed
     */
    public function handle($request, Closure $next, $limit = 100)
    {
        $response = response([
            'errors' => [
                [
                    'title' => 'Please verify your email.'
                ]
            ]
        ], 401);

        if(!Auth::check()) {
            return $response;
        }

        $user = Auth::user();
        if($user->status === 'unverified') {
            return $response;
        }



        return $next($request);
    }
}
