<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'elasticsearch' => [

        'default' => [
            'host' => env('ELASTICSEARCH_HOST', 'elasticsearch'),
            'port' => env('ELASTICSEARCH_PORT', '9200'),
        ]
    ],

    'spaces' => [

        'default' => [
            'api_key' => '5BYWMKKJ4UFSE5UKVGKH',
            'api_secret' => 'cAJhsGIk9LW7PXDF7qvZclPiuKss+lpsFvkEgrMZqoM',
            'name' => 'pnet-local-cdn',
        ]
    ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

];
