<?php namespace App\Virtual\Responses;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="ServerError",
 *     description="Unexpected Server Error",
 *     @OA\Info(
 *      title="Responses",
 *     )
 * )
 */
class ServerError
{

    /**
     * @OA\Property(
     *     title="errors",
     *     description="More detailed description of the image.",
     *     type="array",
     *     format="object",
     *     @OA\Items(
     *          type="object",
     *          @OA\Property(
     *              property="id",
     *              description="A unique identifier for this particular occurrence of the problem.",
     *              example="ERR-1234"
     *          ),
     *          @OA\Property(
     *              property="title",
     *              description="Quick Easily Identifiable Error Title",
     *              example="Unable to connect to database"
     *          ),
     *          @OA\Property(
     *              property="status",
     *              description="The HTTP status code applicable to this problem, expressed as a string value",
     *              example="500"
     *          ),
     *          @OA\Property(
     *              property="code",
     *              description="An application-specific error code, expressed as a string value",
     *              example="14"
     *          ),
     *          @OA\Property(
     *              property="detail",
     *              description="A human-readable explanation specific to this occurrence of the problem. Like title, this field’s value can be localized",
     *              example="There was an error with the input parameters. Please verify that a primary color profile is configured in the user settings page"
     *          ),
     *     )
     * )
     *
     * @var string
     */
    public $errors = [];

}