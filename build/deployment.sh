sh build/shared/clusterlogin.sh
sh build/shared/runtime-dependencies.sh

# Do the deployment
cat build/deployment.yml | envsubst | kubectl apply -f -
cat build/service.yml | envsubst | kubectl apply -f -

cat build/webserver/deployment.yml | envsubst | kubectl apply -f -
cat build/webserver/service.yml | envsubst | kubectl apply -f -
cat build/webserver/hpa.yml | envsubst | kubectl apply -f -
cat build/hpa.yml | envsubst | kubectl apply -f -
#cat build/webserver/hpa.yml | envsubst | kubectl apply -f -


kubectl rollout restart deployment photosynthernet
kubectl rollout restart deployment ps-webserver
kubectl get po