#! /bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
bash /var/www/buildenv.sh

php /var/www/artisan key:generate
php /var/www/artisan config:cache

php /var/www/artisan migrate

mkdir -p /var/www/public/storage/app/tmp/thumbs;

php artisan queue:work
