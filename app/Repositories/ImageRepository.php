<?php

namespace App\Repositories;

use App\Models\Album;
use App\Models\ColorInfo;
use App\Models\Image;
use App\Repositories\Interfaces\ImageRepositoryInterface;
use App\Traits\UsesCache;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class ImageRepository implements ImageRepositoryInterface
{

    use UsesCache;

    const USER_CACHE_TAGS = ['all_images_by_user'];
    const SINGLE_CACHE_TAGS = ['single_image'];
    const PUBLIC_CACHE_TAGS = ['all_public_images'];

    const CACHE_EXPIRATION = 1800; // 60 seconds * 30 = 30 minutes

    /**
     * @var integer
     */
    private $limit = 12;

    /**
     * @var integer
     */
    private $page = 1;

    /**
     * @var string
     */
    private $cacheKey;

    /**
     * @var Builder
     */
    private $currentModel;

    /**
     * @var integer
     */
    private $count;

    public function all() {
        return Image::all();
    }

    public function getSingle(string $slug, bool $is_album = false) : array {
        $cache_key = $this->generateCacheKey([
            'tags' => self::SINGLE_CACHE_TAGS,
            'slug' => $slug
        ]);

        if(Cache::tags(self::SINGLE_CACHE_TAGS)->has($cache_key)) {
            return $this->getCache($cache_key, self::SINGLE_CACHE_TAGS);
        }

        if($is_album) {
            $Album = new Album();
            $images = $Album->with(['url', 'images'])->whereHas('url', function($query) use ($slug) {
                $query = $query->where('slug', $slug);
                return $query;
            })->get();

            $images = $images->first()->images->all();

        } else {
            $Image = new Image();
            $images = $Image->with(['url'])->whereHas('url', function($query) use ($slug) {
                $query = $query->where('slug', $slug);
                return $query;
            })->get();
            $images = $images->all();
        }


        $images = $this->hydrateImages($images);
        $this->setCache($cache_key, $images, self::USER_CACHE_TAGS);

        return $images;

    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getAllByUser(User $user): array {
        $user_id = $user->id;

        $page = $this->getPage();
        $limit = $this->getLimit();

        $cache_key = $this->generateCacheKey([
            'page' => $page,
            'limit' => $limit,
            'tags' => self::USER_CACHE_TAGS,
            'user_id' => $user->id
        ]);

        $Image = new Image();
        $Image = $Image->with(['url', 'albumImages.album.url'])->where('user_id', $user_id);

        $image_count = $Image->count();
        $this->setCount($image_count);
        if(Cache::tags(self::USER_CACHE_TAGS)->has($cache_key)) {
            return $this->getCache($cache_key, self::USER_CACHE_TAGS);
        }

        $offset = ($page * $limit) - $limit;

        $images = $Image->offset($offset)
            ->limit($limit)
            ->orderBy('id', 'desc')
            ->get();

        $images = $this->hydrateImages($images->all());

        $this->setCache($cache_key, $images, self::USER_CACHE_TAGS);

        return $images;
    }

    public function getAllPublicModel() : Builder {
        $Image = new Image();
        $Image = $Image->withUrls()->where('urls.visibility', 'public');

        $image_count = $Image->count();
        $this->setCount($image_count);

        return $Image;
    }

    /**
     * @return array
     */
    public function getPublic(): array {
        $page = $this->getPage();
        $limit = $this->getLimit();

        $cache_key = $this->generateCacheKey([
            'page' => $page,
            'limit' => $limit,
            'tags' => self::PUBLIC_CACHE_TAGS
        ]);


        $Image = $this->getAllPublicModel();

        if(Cache::tags(self::PUBLIC_CACHE_TAGS)->has($cache_key)) {
            return $this->getCache($cache_key, self::PUBLIC_CACHE_TAGS);
        }

        $images = $Image->offset(($page * $limit) - $limit)
            ->limit($limit)
            ->orderBy('id', 'desc')
            ->get();

        $images = $this->hydrateImages($images->all());

        $this->setCache($cache_key, $images, self::PUBLIC_CACHE_TAGS);

        return $images;

    }

    /**
     * @return int
     */
    public function getCount(): int {
        return $this->count;
    }

    private function extractHexCodes(&$images) {
        $hex_codes = array_column($images, 'hex_codes');
        $flattened_hex_codes = [];
        array_walk_recursive($hex_codes, function($a) use (&$flattened_hex_codes) { $flattened_hex_codes[(string)$a] = true; });

        $flattened_hex_codes = array_keys($flattened_hex_codes);

        // I hate this
        $flattened_hex_codes = array_map(function($item){return (string)$item;}, $flattened_hex_codes);

        return $flattened_hex_codes;
    }

    /**
     * @param array $images
     * @return array
     */
    private function hydrateImages(array $images): array {

        $base_url = Config::get('app.url');

        $hydrated_images = [];

        $hex_codes = $this->extractHexCodes($images);

        $colors = ColorInfo::whereIn('hex', $hex_codes)->get();

        $mapped_colors = [];
        foreach($colors as $color) {
            $mapped_colors[$color->hex] = $color->name;
        }

        foreach($images as $image) {
            $file_path = storage_path('app/public/images/') . $image->file_name;


            $albums = [];
            if(isset($image->albumImages) && !empty($image->albumImages->album)) {
                foreach($image->albumImages->album as $album) {

                    foreach($album->url as $url) {
                        $albums[] = [
                            'title' => $album->title,
                            'link' => $base_url . '/albums/' . $url->slug
                        ];
                    }
                }

            }

            $color_tags = [];
            foreach($image->hex_codes as $hex) {
                if(isset($mapped_colors[$hex])) {
                    $color_tags[] = [
                        'hex' => $hex,
                        'name' => $mapped_colors[$hex],
                    ];
                }
            }

            $image_meta = $image->meta;

            if(isset($image_meta['primary_colors'])) {

                $pc = [];
                foreach($image_meta['primary_colors'] as $hex => $primary_colors) {
                    if(is_array($primary_colors)) {
                        foreach($primary_colors as $color) {
                            $pc[] = $color;
                        }
                    } else {
                        $pc[] = $primary_colors;
                    }
                }

                $image_meta['primary_colors'] = array_unique($pc);

                sort($image_meta['primary_colors']);
                $image->meta = $image_meta;
            }
            $image->color_tags = $color_tags;
            $image->albums = $albums;
            unset($image->albumImages);

            $hydrated_images[] = $image;


            $image->is_public = false;
            foreach($image->url as $url) {
                if($url->visibility === 'public') {
                    $image->is_public = true;
                    break;
                }
            }

        }

        return $hydrated_images;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return Builder
     */
    public function getCurrentModel(): Builder
    {
        return $this->currentModel;
    }

    /**
     * @param Builder $currentModel
     */
    public function setCurrentModel(Builder $currentModel): void
    {
        $this->currentModel = $currentModel;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }

}