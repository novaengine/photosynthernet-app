<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="ColorInfo",
 *     description="Model for representing information about a particular color",
 *     @OA\Info(
 *      title="ColorInfo",
 *     )
 * )
 */
class ColorInfo extends Model
{

    protected $table = 'color_info';
    protected $casts = [
        'hex' => 'string'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'hex',
    ];

    /**
     * @OA\Property(
     *     property="id",
     *     description="Primary auto-incrementing sequential identifier",
     *     format="int64",
     *     example=123
     * )
     *
     * @var integer
     */
    public function getIdAttribute($value){ return $value; }

    public function related() {
        return $this->hasMany(ColorRelationship::class, 'origin_color_id');
    }

    /**
     * @OA\Property(
     *     property="name",
     *     description="Human-Identifiable Name for the Color",
     *     format="string",
     *     example="Firetruck Red"
     * )
     *
     * @var string
     */
    public function getNameAttribute($value){ return $value; }

    /**
     * @OA\Property(
     *     property="hex",
     *     description="Color as represented by a six character hex code",
     *     format="string",
     *     example="262626"
     * )
     *
     * @var string
     */
    public function getHexAttribute($value){ return $value; }

}
