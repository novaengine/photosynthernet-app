<?php

namespace App\Providers;

use App\Events\ImageUploaded;
use App\Events\PaletteGenerated;
use App\Listeners\Image;
use App\Listeners\IndexSearch;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ImageUploaded::class => [
            Image::class,
        ],

        PaletteGenerated::class => [
            IndexSearch::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
