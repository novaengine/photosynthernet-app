<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;


class Album extends Model
{

    protected $table = 'albums';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'visibility',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function url() {
        return $this->hasMany(Url::class, 'image_id')->whereRaw('is_album = true');
    }

    public function images() {
        return $this->belongsToMany(Image::class, 'album_images');
    }

    public function getHexCodesAttribute($value) {
        if(empty($value)) {
            $value = '[]';
        }
        $value = json_decode($value, true);
        return $value;
    }

}
