<?php

namespace App\Http\Middleware;

use Closure;


class Cors {
    public function handle($request, Closure $next, ...$guards) {
        $request->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Token-Auth, Authorization');

        return $next($request)->header('Access-Control-Allow-Origin', '*');
    }
}
