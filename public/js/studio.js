const studio = {

    $parent_container : undefined,

    slug : '',

    libraries_loaded : false,

    image_data : {},

    scroll_timeout : undefined,

    init($parent_container) {
        const self = this;

        $parent_container.removeClass('container');
        $parent_container.css('background', '#454545')
        self.$parent_container = $parent_container

        const path = app.settings.path;

        if(path.length > 2) {
            self.slug = path[2];
        }


        self.scripts = [
            'https://cdn.jsdelivr.net/npm/chart.js@3.3.2/dist/chart.min.js',
            '/js/vendor/curve.js',
        ]

        self.styles = [
            'https://cdn.jsdelivr.net/npm/bulma-divider@0.2.0/dist/css/bulma-divider.min.css',
        ]

        app.loadLibraries(self.scripts, self.styles).done(function(r) {
            self.render();
        });
    },

    curve_state : {
        red : {},
        green : {},
        blue : {}
    },

    presets : {
        default : {"red":[{"x":0,"y":0},{"x":0.25,"y":0.25},{"x":0.75,"y":0.75},{"x":1,"y":1}],"green":[{"x":0,"y":0},{"x":0.25,"y":0.25},{"x":0.75,"y":0.75},{"x":1,"y":1}],"blue":[{"x":0,"y":0},{"x":0.25,"y":0.25},{"x":0.75,"y":0.75},{"x":1,"y":1}]},
        solar : {
            "red": [
                {
                    "x": 0,
                    "y": 0
                },
                {
                    "x": 0.2885,
                    "y": 0.41600000000000004
                },
                {
                    "x": 0.6405,
                    "y": 0.8160000000000001
                },
                {
                    "x": 1,
                    "y": 1
                }
            ],
            "green": [
                {
                    "x": 0,
                    "y": 0
                },
                {
                    "x": 0.3765,
                    "y": 0.30000000000000004
                },
                {
                    "x": 0.7405,
                    "y": 0.696
                },
                {
                    "x": 1,
                    "y": 1
                }
            ],
            "blue": [
                {
                    "x": 0,
                    "y": 0
                },
                {
                    "x": 0.4605,
                    "y": 0.404
                },
                {
                    "x": 0.7085,
                    "y": 0.752
                },
                {
                    "x": 1,
                    "y": 1
                }
            ]
        },
        dusk : {
            "red": [
                {
                    "x": 0,
                    "y": 0
                },
                {
                    "x": 0.3485,
                    "y": 0.22799999999999998
                },
                {
                    "x": 0.6565,
                    "y": 0.484
                },
                {
                    "x": 1,
                    "y": 1
                }
            ],
            "green": [
                {
                    "x": 0,
                    "y": 0
                },
                {
                    "x": 0.3925,
                    "y": 0.21199999999999997
                },
                {
                    "x": 0.7285,
                    "y": 0.43999999999999995
                },
                {
                    "x": 1,
                    "y": 1
                }
            ],
            "blue": [
                {
                    "x": 0,
                    "y": 0
                },
                {
                    "x": 0.2925,
                    "y": 0.252
                },
                {
                    "x": 0.5925,
                    "y": 0.556
                },
                {
                    "x": 1,
                    "y": 1
                }
            ]
        }
    },

    red_curve : undefined,
    green_curve : undefined,
    blue_curve : undefined,

    handleCurveChange(curve, ctx, canvas, original_image_data, color) {
        const self = this;

        if(self.red_curve && self.green_curve && self.blue_curve) {
            let imageData = ctx.getImageData(0,0, canvas.width, canvas.height);
            let pix = imageData.data;

            // We get the corresponding value for every pixel in the image using the rgb array from every colorcurve
            for (let i = 0; i < imageData.width * imageData.height; i++) {

                let identifier;
                switch(color) {
                    case 'red':
                        identifier = i*4;
                        break;
                    case 'green':
                        identifier = i*4+1;
                        break;
                    case 'blue':
                        identifier = i*4+2
                        break;
                }

                pix[identifier]   = self[color + '_curve'].rgb[original_image_data[identifier]];
            }

            self.curve_state[color] = self[color + '_curve'].points;

            imageData.data = pix;
            ctx.putImageData(imageData, 0, 0);
        }
    },

    dumpStateToTextArea() {
        const self = this;

        for(let i in self.curve_state) {
            if(!self[i + '_curve']) {
                continue;
            }
            self.curve_state[i] = self[i + '_curve'].points;
        }

        const state = JSON.stringify(self.curve_state, null, 2)
        $('[data-js="curve_state_text"]').html(state)
    },

    render(data) {
        const self = this;

        PImage.getBySlug(self.slug).done(function(r) {

            let data = r;
            self.$parent_container.render('studio', data).done(function(r) {
                var canvas = document.getElementById("canvas");
                var ctx = canvas.getContext("2d");

                var background = new Image();
                // background.crossOrigin = "Anonymous";
                background.src = self.settings.cloud_url + data.data[0].attributes.file_name + '?' +  new Date().getTime();
                background.setAttribute('crossOrigin', 'anonymous');
// Make sure the image is loaded first otherwise nothing will draw.
                background.onload = function(){

                    let ratio = {
                        x : background.width / 500,
                        y : background.height / canvas.height
                    }
                    canvas.height = background.height;
                    canvas.width = background.width;

                    ctx.drawImage(background,0,0);
                    const original_image_data = ctx.getImageData(0,0, canvas.width, canvas.height).data;


                    self.red_curve = new ColorCurve('red_curve', function(e) {
                        self.handleCurveChange(this, ctx, canvas, original_image_data, 'red');
                        self.dumpStateToTextArea()
                    }, 'red');

                    self.green_curve = new ColorCurve('green_curve', function(e) {
                        self.handleCurveChange(this, ctx, canvas, original_image_data, 'green');
                        self.dumpStateToTextArea()
                    }, 'green');

                    self.blue_curve = new ColorCurve('blue_curve', function(e) {
                        self.handleCurveChange(this, ctx, canvas, original_image_data, 'blue');
                        self.dumpStateToTextArea()
                    }, 'blue');

                }

                self.bindActions();
            })
        });


    },

    renderImage(data) {
        const self = this;

        let prms;
        if(app.settings.is_album === true) {
            prms = Album.getImagesBySlug(self.slug);
        } else {

        }

        prms.done(function(r) {

            for(let i in r.data) {
                let image = r.data[i];
                image = self.hydrateActiveColor(image, image.attributes.hex_codes[0]);

                r.data[i] = image;
            }

            self.cacheImageData(r.data);

            self.$parent_container.render('image', {
                images : r.data,
                meta : r.meta,
            }).done(function(r) {
                self.bindActions();

                index.lazyLoadImageRender();

                // Always show the first tab on render
                $('[data-tab=0]').removeClass('is-hidden');
            })

        });
    },

    applyPreset(preset) {
        const self = this;

        for(let i in preset) {
            self[i + '_curve'].points = preset[i];

            self[i + '_curve'].draw();
            self[i + '_curve'].updateValues();
        }

        self.dumpStateToTextArea()
    },

    bindActions() {
        const self = this;

        const $slider = $('[data-js="slider"]');
        $slider.unbind();
        $slider.on('input', function() {
            // console.log($(this).val())
        })

        const $color_channel_select = $('[data-js="color_channel_select"]');
        $color_channel_select.unbind();
        $color_channel_select.on('input', function(e) {
            $('[data-js$="_curve"]').css('z-index', 0);
            $('[data-js$="_curve"]').css('opacity', 0.4);

            $('[data-js="'+$(this).val()+'_curve"]').css('z-index', 10);
            $('[data-js="'+$(this).val()+'_curve"]').css('opacity', 1);
        })

        const $color_grade_preset = $('[data-js="color_grade_preset"]');
        $color_grade_preset.unbind();
        $color_grade_preset.on('input', function(e) {
            let key = $(this).val() || 'default';
            if(self.presets[key] === undefined) {
                return;
            }

            let preset = self.presets[key]
            self.applyPreset(preset)
        })

        const $submit_preset = $('[data-js="submit_preset"]');
        $submit_preset.unbind();
        $submit_preset.on('click', function() {
            const preset_raw = $('[data-js="curve_state_text"]').val();

            try {
                const preset = JSON.parse(preset_raw)

                self.curve_state = preset;

                self.applyPreset(preset);

            } catch(e) {
                toast.rise('Invalid Json', {
                    class : 'is-danger'
                })
            }
        });

        const $delete_button = $('[data-js=delete_image]');
        $delete_button.unbind();
        $delete_button.on('click', function() {

            const $el = $(this);

            const confirmed = $(this).attr('data-confirm');
            if(confirmed === 'true') {
                const slug = $(this).data('slug')
                PImage.delete(slug).done(function() {
                    $el.find('[data-js=confirm_message]').html('')
                    $el.attr('data-confirm', 'true');
                    $el.removeClass('is-danger');
                    $el.addClass('is-success');
                    $el.find('svg').remove();
                    $el.prepend('<i class="fa fa-check"></i>');
                    $el.find('[data-js=confirm_message]').css('padding-left', '0')

                    window.setTimeout(function() {
                        $el.fadeOut();
                    }, 1500)

                });

            } else {
                $el.find('[data-js=confirm_message]').css('padding-left', '10px').html('Click to Confirm Deletion')
                $el.attr('data-confirm', 'true');
            }

        });

        const $tabs = $('.tabs .tab');
        $tabs.unbind();
        $tabs.on('click', function() {
            const show_container = $(this).data('js');
            const $parent = $(this).closest('[data-js="tabs_parent"]')

            let $clickable_tabs = $parent.find('.tabs .tab');
            $clickable_tabs.removeClass('is-active');
            $(this).addClass('is-active');

            const $selected_container = $parent.find('[data-tab="'+show_container+'"]');

            const $all_tabs = $parent.findExclude('[data-tab]', '[data-js="tabs_parent"]');
            $all_tabs.addClass('is-hidden');

            $selected_container.removeClass('is-hidden');

            $selected_container.find('img').each(function() {
                if(!$(this).attr('src')) {
                    $(this).attr('src', $(this).data('click-src'));
                }
            })

        });

        const $color_select = $('[data-js="color_select"]');
        $color_select.unbind();
        $color_select.on('click', function() {

            const id = $(this).attr('data-id');
            const $el = $('[data-js="color_info"][data-id="'+id+'"]');


            let image = self.image_data[id];
            image = self.hydrateActiveColor(image, $(this).data('color'));

            $el.render('partial_color_info', image).done(function(r) {
                self.bindActions();
            })



        });

        const $color_variant = $('[data-js="color_variant"]');
        $color_variant.unbind();
        $color_variant.on('click', function() {

            const id = $(this).attr('data-id');
            const $el = $('[data-js="color_info"][data-id="'+id+'"]');


            let image = self.image_data[id];
            image = self.hydrateActiveColor(image, $(this).data('color'));

            $el.render('partial_color_info', image).done(function(r) {
                self.bindActions();
            })



        });

        const $copy_color = $('.copy-color');
        $copy_color.unbind();
        $copy_color.on('click', function(e) {
            let color = $(this).find('.color-text').text();
            self.copyToClipboard(color);
            const $copy_response = $(this).parent().find('.toast-message');

            const child_count = $(this).parent().children().length;

            let width = $(this).parent().width();
            let color_width = width / child_count;

            let idx = $(this).index();

            let offset = (color_width * idx) - (color_width / 2) - 8;
            $copy_response.css('left', offset + 'px')


            $copy_response.html('Copied to Clipboard!')
            $copy_response.removeClass('hide');
            $copy_response.removeClass('--fly-fade-out-down');
            $copy_response.addClass('--fly-fade-in-up');

            if(self.timeout !== undefined) {
                window.clearTimeout(self.timeout);
            }

            self.timeout = window.setTimeout(function() {
                $copy_response.addClass('--fly-fade-out-down');
                $copy_response.removeClass('--fly-fade-in-up');

                self.timeout = window.setTimeout(function() {
                    $copy_response.attr('style', '');
                }, 200);

            }, 2000)
        });
    },

}

modules.studio = studio;

