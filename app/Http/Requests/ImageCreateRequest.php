<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ImageCreateRequest extends FormRequest {

    public function rules() {

        return [
            'title' => 'required:string',
            'description' => 'sometimes:string',
            'visibillity' => 'required:string',
            'file' => 'required:binary',
        ];

    }

}