<?php

namespace App\Console\Commands;

use App\Libraries\Elasticsearch;
use App\Models\ColorInfo;
use App\Models\Image;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class IndexSearch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:search';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Index images into elasticsearch';

    private $alias = 'image_search';
    const PAGE_SIZE = 250;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    private function createIndex() {
        $client = Elasticsearch::connection();

        $new_index = 'image_search_' . md5(uniqid());

        $this->deleteOrphanIndices($this->alias);

        $params = [
            'index' => $new_index,
            'body' => [
                'mappings' => [
                    'properties' => [
                        'title' => [
                            'type' => 'text',
                        ],
                        'description' => [
                            'type' => 'text'
                        ],
                        'album_title' => [
                            'type' => 'text',
                        ],
                        'album_description' => [
                            'type' => 'text'
                        ],
                        'file_name' => [
                            'type' => 'text',
                            'index' => false,
                        ],
                        'colors' => [
                            'type' => 'text'
                        ],

                        'color_names' => [
                            'type' => 'text'
                        ],

                        'close_colors' => [
                            'type' => 'text'
                        ],

                        'url' => [
                            'type' => 'flattened'
                        ]
                    ]
                ],
                'settings' => [
                    'number_of_shards' => 2,
                    'number_of_replicas' => 0,
                    'max_ngram_diff' => 30,
                ]
            ],
        ];

        $client->indices()->create($params);

        return $new_index;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle() {
        $client = Elasticsearch::connection();

        // Create Index
        $new_index = $this->createIndex();


        // Get All Images
        $Image = new Image();
        $Image = $Image->with(['url', 'albumImages.album'])->whereHas('url', function($query) {
            return $query->where('visibility', 'public');
        })->whereNotNull('hex_codes')->limit(self::PAGE_SIZE);

        $page = 0;
        do {
            $images = $Image->get();

            if($images->count() <= 0) {
                $this->info('Reached the end of search batches after page ['.$page.']... Exiting...');
                break;
            }

            $page++;

            $batch_msg = "Indexing Page [".$page."] with [".$images->count()."] documents to ES index [" . $new_index . "]";
            $this->info($batch_msg);


            $payload = [
                'index' => $new_index,
                'body' => [],
            ];

            $all_colors = [];

            foreach($images as $image) {
                $meta = $image->meta;

                if(!empty($meta) && is_object($meta) && property_exists($meta, 'tints')) {
                    foreach($meta->tints as $base_color => $tints) {

                        foreach($tints as $tint) {
                            $all_colors[$tint] = 1;
                        }

                    }
                }

                if(!empty($meta) && is_object($meta) && property_exists($meta, 'shades')) {
                    foreach($meta->shades as $base_color => $shades) {

                        foreach($shades as $shade) {
                            $all_colors[$shade] = 1;
                        }

                    }
                }

                foreach($image->hex_codes as $hex) {
                    $all_colors[$hex] = 1;
                }



            }

            $all_colors = array_keys($all_colors);

            $all_colors = array_map(function($item) {
                return (string) $item;
            }, $all_colors);

            $color_info = ColorInfo::select(['name', 'hex'])->whereIn('hex', $all_colors)->get();

            $color_info_mapped = [];
            foreach($color_info as $color) {
                $color_info_mapped[$color->hex] = $color->name;
            }

            foreach($images as $image) {


                $album_title = '';

                if($image->albumImages !== null && $image->albumImages->album !== null) {
                    foreach($image->albumImages->album as $album) {
                        $album_title .= $album->title . ' ';
                    }
                }


                $id = $image->id;
                $title = $image->title;
                $description = $image->description;
                $colors = $image->hex_codes;

                $record = [
                    'index' => [
                        '_index' => $new_index,
                        '_id' => $id,
                    ],
                    'id' => $id
                ];

                $color_names = [];
                foreach($image->hex_codes as $hex) {
                    if(isset($color_info_mapped[$hex])) {
                        $color_names[] = $color_info_mapped[$hex];
                    }
                }

                $meta = $image->meta;
                $close_colors = [];
                if(!empty($meta['tints'])) {
                    foreach($meta['tints'] as $base_color => $tints) {

                        foreach($tints as $tint) {

                            $close_colors[] = $tint;
                            if(isset($color_info_mapped[$tint])) {
                                $color_names[] = $color_info_mapped[$tint];
                            }
                        }

                    }
                }

                if(!empty($meta['shades'])) {
                    foreach($meta['shades'] as $base_color => $shades) {

                        foreach($shades as $shade) {
                            $close_colors[] = $shade;
                            if(isset($color_info_mapped[$shade])) {
                                $color_names[] = $color_info_mapped[$shade];
                            }
                        }

                    }
                }

                if(isset($meta['primary_colors'])) {

                    $primary_colors = [];
                    array_map(function($item) use(&$primary_colors) {

                        if(is_array($item)) {
                            array_push($primary_colors, ...array_values($item));
                        } else {
                            array_push($primary_colors, $item);
                        }
                    }, $meta['primary_colors']);
                    $color_names = array_merge($color_names, $primary_colors);
                }

                $color_names = array_unique($color_names);
                sort($color_names);

                $payload['body'][] = $record;
                $payload['body'][] = [
                    'id' => $id,
                    'title' => $title,
                    'album_title' => $album_title,
                    'description' => $description,
                    'colors' => $colors,
                    'close_colors' => $close_colors,
                    'color_names' => $color_names,
                    'file_name' => $image->file_name,
                    'url' => json_decode(json_encode($image->url), true)
                ];

            }

            if(!empty($payload['body'])) {
                $res = $client->bulk($payload);

                if($res['errors']) {
                    foreach($res['items'] as $item) {

                        $item = $item['index'];

                        if($item['status'] !== 201) {
                            $msg = "Error Indexing Item : [id={$item['_id']}] because - {$item['error']['reason']}";
                            $this->warn($msg);
                            Log::error($msg, $item);
                        }
                    }
                }
            }

            $Image->offset(self::PAGE_SIZE * $page);
        } while(count($images) > 0);

        // Format Images (Tag with Albums)

        // Update Aliases
        $old_index = $this->findIndex($client, $this->alias);

        $params = [];
        $params['body'] = [
            'actions' => [
                [
                    'add' => [
                        'index' => $new_index,
                        'alias' => $this->alias
                    ]
                ]
            ]
        ];

        if(!empty($old_index)) {
            $params['body']['actions'][] = [
                'remove' => [
                    'index' => $old_index,
                    'alias' => $this->alias
                ]
            ];
        }

        try {
            $res = $client->indices()->updateAliases($params);
        } catch(\Exception $e) {
            var_dump($e->getMessage());
            Log::error($e);
        }

        $this->deleteOrphanIndices($this->alias);

    }

    private function findIndex($client, $alias_name) {
        $aliases = $client->indices()->getAliases();
        foreach($aliases as $index => $alias_mapping) {
            if(array_key_exists($alias_name, $alias_mapping['aliases'])) {
                return $index;
            }
        }

        return null;
    }

    private function deleteIndices($pattern, $exclude) {

        try {
            $client = Elasticsearch::connection();
            $old_indices = $client->cat()->indices(['index' => $pattern]);

            foreach($old_indices as $index) {
                $index_name = $index['index'];
                if($index_name == $exclude) {
                    continue;
                }

                $client->indices()->delete([
                    'index' => $index_name
                ]);
            }
        } catch(\Exception $e) {
            Log::error($e);
        }
    }


    private function deleteOrphanIndices($alias) {
        $client = Elasticsearch::connection();
        $exclude = $this->findIndex($client, $alias);
        $this->deleteIndices($alias . '_*', $exclude);
    }

}
