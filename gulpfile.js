const { src, dest, series, parallel }  = require("gulp");

const minify = require("gulp-minify");
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');

// Task to minify css using package cleanCSs
function minCSS() {
    return src([
        'resources/css/vendor/bulma.min.css',
        'resources/css/vendor/bulma-accordion.min.css',
        'resources/css/vendor/spectrum.min.css',
        'public/css/*.css'
    ])
        .pipe(cleanCSS())
        .pipe(concat('app-min.css'))
        .pipe(dest('public/css'))
}

function minJS() {
    return src([

        'resources/js/vendor/fa-all.js',
        'resources/js/vendor/jquery.min.js',
        'resources/js/vendor/handlebars.min.js',
        'resources/js/vendor/bulma-accordion.min.js',
        'resources/js/vendor/spectrum.min.js',


        'public/js/app.js',
        'public/js/models.js',
        'public/js/index.js',
        'public/js/profile.js',
        'public/js/forgot.js',
        'public/js/login.js',
        'public/js/register.js',
        'public/js/upload.js',
        'public/js/single_image.js',
        'public/js/search.js',
        'public/js/studio.js',
    ], { allowEmpty: true })
        .pipe(concat('app.js'))
        .pipe(minify({noSource: true}))
        .pipe(dest('public/js'))
}

exports.minJS = minJS;
exports.minCSS = minCSS;
exports.default = series(parallel(minCSS, minJS))