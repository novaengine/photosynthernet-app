# Authenticate with doctl
doctl auth init -t="$DOCTL_TOKEN"
doctl kubernetes cluster kubeconfig save $CLUSTER_ID

## Enables envsubst
apk update && apk add gettext

# Do the deployment
cat build/elasticsearch/deployment.yml | envsubst | kubectl apply -f -
cat build/elasticsearch/service.yml | envsubst | kubectl apply -f -
cat build/elasticsearch/daemonset.yml | envsubst | kubectl apply -f -
kubectl rollout restart deployment ps-elasticsearch
