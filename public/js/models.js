/**
 * Models
 */

const Clients = {

    cached : false,
    cachekey : 'clients',

    index() {
        const self = this;

        const prms = $.Deferred();

        if(self.cached) {
            prms.resolve(app.store.get(self.cachekey))
        } else {
            api.get('/users/' + app.state.user.id + '/clients').done(function(r) {

                self.cached = true;
                app.store.put(self.cachekey, r);

                prms.resolve(r);
            })
        }


        return prms;
    },

    create(data) {
        const self = this;

        const prms = $.Deferred();

        self.cached = false;

        api.post('/users/' + app.state.user.id + '/clients', {
            data : data
        }).done(function(r) {
            prms.resolve(r);
        })

        return prms;
    }
}

const PImage = {
    index(page, visibility) {
        const self = this;

        page = page ? page : 1;

        const prms = $.Deferred();

        let payload = {
            page : page,
            filter : {}
        };

        if(visibility !== undefined) {
            payload.filter.visibility = visibility;
        }

        api.get('/images', payload).done(function(r) {
            r = self.processImagesResponse(r);
            prms.resolve(r);
        });

        return prms;
    },

    delete(slug) {
        return api.delete('/images/' + slug);
    },

    queue(id) {
        return api.post('/images/' + id + '/_queue');
    },

    upload(slug, data) {
        return api.file(slug, data);
    },

    search(query, filters) {
        const self = this;

        const prms = $.Deferred();

        let colors = [];
        let palette = [];
        if(filters !== undefined) {
            for(let i in filters.colors.values) {
                if(filters.colors.values[i].selected === true) {
                    colors.push(filters.colors.values[i].id)
                }
            }
        }

        api.get('/search', {
            filter : {
                query : query,
                color_names : colors,
                palette: palette
            }
        }).done(function(r) {
            r = self.processImagesResponse(r);
            prms.resolve(r);
        });

        return prms;
    },

    isPublic(image) {
        let is_public = false;
        for(let u in image.attributes.url) {
            is_public = image.attributes.url[u].visibility === 'public';
            if(is_public) {
                break;
            }
        }

        image.attributes.is_public = is_public;

        return image;
    },

    getBySlug(slug) {
        const self = this;
        const prms = $.Deferred();

        api.get('/images/' + slug).done(function(r) {
            r = self.processResponseForPalette(r);

            for(let i in r.data) {
                const image = r.data[i]
                r.data[i].attributes.user_owned = app.isUserLoggedIn() && image.attributes.user_id === app.state.user.id;

                r.data[i] = self.isPublic(image);
            }

            prms.resolve(r);
        });

        return prms;
    },

    formatPaginationLinks(data) {
        let middle_links = {};

        data.meta = data.meta || {}

        let page = data.meta.page || {};

        $.each(page.middle, function(ind, val) {

            middle_links[val] = {
                url : data.links.middle[ind],
                page : val
            }

            if(data.meta.page.current_page === val) {
                middle_links[val].selected = true;
            }


        });

        let show_lead = page.current_page >= 3;
        let first_selected = page.current_page === 1 || page.current_page === page.first;
        let last_selected = page.current_page === page.last;
        let show_tail = page.current_page < page.last - 1;
        let show_pagination = !(page.last === undefined || page.last === 1);

        return {
            first_selected : first_selected,
            last_selected : last_selected,
            show_lead : show_lead,
            middle_links : middle_links,
            show_tail : show_tail,
            show_pagination : show_pagination
        }

    },

    backfillEmptyPalettesWithWhite(hex_codes) {
        if($.isEmptyObject(hex_codes)) {
            hex_codes = []
            for(let i = 0; i < 6; i++) {
                hex_codes.push('#fffff8');
            }
        }

        return hex_codes;

    },

    processResponseForPalette(r) {
        const self = this;

        $.each(r.data, function (ind, val) {

            let hex_codes = val.attributes.hex_codes;
            r.data[ind].attributes.has_palette = hex_codes.length > 0;

            // hex_codes = self.backfillEmptyPalettesWithWhite(hex_codes);
            r.data[ind].attributes.hex_codes = hex_codes;

        });

        return r;
    },

    processImagesResponse(r) {
        const self = this;

        let pagination_data = self.formatPaginationLinks(r);

        let formatted_data = {
            ...r,
            ...pagination_data
        };

        formatted_data = self.processResponseForPalette(formatted_data);

        return formatted_data;
    },
}

const Album = {
    create(data) {

        return $.ajax({
            url: '/api/albums',
            type: 'post',
            headers : {
                Authorization : 'Bearer ' + app.state.user.token,
            },
            data: data
        });
    },

    processAlbumsResponse(response) {
        for(let i in response.data) {
            const datum = response.data[i];
            const images = datum.attributes.images;

            const representative_image = images[0];

            response.data[i].attributes.representative_image = representative_image;
        }

        return response
    },

    getImagesBySlug(slug) {
        const self = this;
        const prms = $.Deferred();

        api.get('/albums/' + slug + '/images').done(function(r) {
            r = PImage.processResponseForPalette(r);

            for(let i in r.data) {
                const image = r.data[i]
                r.data[i].attributes.user_owned = app.isUserLoggedIn() && image.attributes.user_id === app.state.user.id;

                r.data[i] = PImage.isPublic(image);
            }

            prms.resolve(r);
        });

        return prms;
    },


    index(page, limit) {
        const self = this;
        const prms = $.Deferred();

        page = page ? page : 1;

        let payload = {
            include: 'images',
            page : page
        };

        if(limit !== undefined) {
            payload.limit = limit;
        }

        api.get('/albums', payload).done(function(r) {
            r = self.processAlbumsResponse(r);
            prms.resolve(r);
        });

        return prms;
    }
}

const User = {
    read(id) {
        return $.ajax({
            url : '/api/users/' + id,
            type : 'get',
            headers : {
                'Authorization' : 'Bearer ' + app.state.user.token
            }
        })
    },

    update(user_id, data) {
        return api.patch('/users/' + user_id, data);
    },

    reset(email) {

        return api.post('/password/reset', {
            email : email
        });

    },

    readLoggedInUser() {
        const self = this;

        if(!app.isUserLoggedIn()) {
            return false;
        }

        return self.read(app.state.user.id)
    }

}