# Authenticate with GKE
#echo "$SERVICE_ACCOUNT_KEY" > key.json
#gcloud auth activate-service-account --key-file=key.json
#gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${CLUSTER_ZONE} --project gleaming-cove-237218

# Do the deployment
cat build/nfs/volume.yml | envsubst | kubectl apply -f -
cat build/nfs/volume2.yml | envsubst | kubectl apply -f -
cat build/nfs/volume3.yml | envsubst | kubectl apply -f -
cat build/nfs/deployment.yml | envsubst | kubectl apply -f -
cat build/nfs/service.yml | envsubst | kubectl apply -f -
kubectl rollout restart statefulset nfs-server
