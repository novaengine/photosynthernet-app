<?php

namespace App\Console\Commands;

use App\DTO\ImageDTO;
use App\Events\ImageUpdate;
use App\Events\PaletteGenerated;
use App\Models\Image;
use App\Repositories\ImageRepository;
use App\Services\ImageService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class Reprocessor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:reprocess';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reprocess all images for thumbnails and palettes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $all_images = Image::get();
        foreach($all_images as $image){
            $dto = new ImageDTO();
            $dto->hydrateFromModel($image);
            ImageService::queueImage($dto);
        }
    }
}