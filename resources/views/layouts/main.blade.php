@include('layouts.head')

<body>

@include('layouts.navbar')

<div class="main-container" data-js="main_container">

    @section('content')
    @show

    <footer class="footer">
        <div class="container">

            <div class="columns">
                <div class="column is-4 is-offset-8">
                    <ul>
                        <li><a href="/about-us">About Us</a></li>
                        <li><a href="#">Open an Issue</a></li>
                        <li><a href="/terms">Terms and Conditions</a></li>
                    </ul>
                </div>
            </div>

            <div class="content has-text-centered">
                <p>Powered by <a href="https://novaengine.ai">NovaEngine</a></p>
            </div>
        </div>
    </footer>

</div>

<div class="mobile-nav" data-js="mobile_nav_container">

</div>

<div data-js="control_panel_container">

</div>

<div data-js="global_image_upload" class="columns is-vcentered global-image-upload">

{{--    <div class="columns">--}}
        <div class="column is-12 is-vcentered">
            <h4 class="has-text-centered title is-3"><i class="fa fa-image"></i></h4>
            <h3 class="has-text-centered title is-3">Upload Image</h3>

        </div>
{{--    </div>--}}

</div>

<script src="https://sdk.canva.com/designbutton/v2/api.js"></script>
<script async defer data-domain="{{str_replace('https://', '', \Illuminate\Support\Facades\Config::get('app.url'))}}" src="https://plausible.io/js/plausible.js"></script>
<script>window.plausible = window.plausible || function(event, options) {

    if(options !== undefined && options.callback !== undefined) {
        options.callback()
    }

}</script>

{{--<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.6/handlebars.min.js" integrity="sha512-zT3zHcFYbQwjHdKjCu6OMmETx8fJA9S7E6W7kBeFxultf75OPTYUJigEKX58qgyQMi1m1EgenfjMXlRZG8BXaw==" crossorigin="anonymous"></script>--}}

{{--<script src="https://cdn.jsdelivr.net/npm/bulma-accordion@2.0.1/dist/js/bulma-accordion.min.js" integrity="sha256-YhfQUELZ4w2haDln4batfqLYmnzZGMnZ8tmB/9BZWCk=" crossorigin="anonymous"></script>--}}

{{--<script src="https://cdn.jsdelivr.net/npm/spectrum-colorpicker2/dist/spectrum.min.js"></script>--}}



<!-- Bulma JS Requirements -->
<script>
    document.addEventListener('DOMContentLoaded', () => {
        // Get all "navbar-burger" elements
        const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
        // Check if there are any navbar burgers
        if ($navbarBurgers.length > 0) {

            // Add a click event on each of them
            $navbarBurgers.forEach( el => {
                el.addEventListener('click', () => {

                    // Get the target from the "data-target" attribute
                    const target = el.dataset.target;
                    const $target = document.getElementById(target);

                    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                    el.classList.toggle('is-active');
                    $target.classList.toggle('is-active');

                });
            });
        }

    });
</script>

<!-- Main Application Javascript -->

{{--@if(\Illuminate\Support\Facades\App::environment() === 'local')--}}
{{--    <?php $scripts = [--}}
{{--        '/js/app.js',--}}
{{--        '/js/models.js',--}}
{{--        '/js/index.js',--}}
{{--        '/js/profile.js',--}}
{{--        '/js/forgot.js',--}}
{{--        '/js/login.js',--}}
{{--        '/js/register.js',--}}
{{--        '/js/upload.js',--}}
{{--        '/js/single_image.js',--}}
{{--        '/js/search.js',--}}
{{--        '/js/studio.js',--}}
{{--    ]; ?>--}}

{{--        @foreach($scripts as $script)--}}
{{--            <script src="{{$script}}"></script>--}}
{{--        @endforeach--}}

{{--@else--}}
    <script src="/js/app-min.js"></script>
{{--@endif--}}

@section('scripts')
@show

</body>

</html>
