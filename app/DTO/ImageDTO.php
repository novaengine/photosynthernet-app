<?php namespace App\DTO;

use App\Models\Image;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageDTO extends BaseDTO {

    const ENUM_VISIBILITIES = [
        'private' => 1,
        'public' => 1,
    ];

    public int $id;
    public string $title = '';
    public ?string $description = '';
    public string $visibility = 'private';
    public UploadedFile $file;
    public int $user_id;
    public ?int $analysis_profile_id;
    public ?int $album_id;

    public string $file_name = '';
    public string $original_file_name = '';

    public function setVisibility($value = '') {

        if(!empty($value) & empty(self::ENUM_VISIBILITIES[$value])) {
            throw new \Exception('Attempting to set undefined visibility ' . $value);
        }

        $this->visibility = empty($value) ? 'private' : $value;
    }

    public function hydrateFromModel(Model $image) {
        parent::hydrateFromModel($image);
        $this->analysis_profile_id = $image->analysis_profile;
    }

}
