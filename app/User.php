<?php

namespace App;

use App\Models\UserResetToken;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="User",
 *     description="Model containing information about a registered photosynthernet user.",
 *     @OA\Info(
 *      title="User",
 *     )
 * )
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'verification_token', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function resets() {
        return $this->hasMany(UserResetToken::class, 'user_id', 'id');
    }

    /**
     * @OA\Property(
     *     property="id",
     *     description="Primary auto-incrementing sequential identifier",
     *     format="int64",
     *     example=456
     * )
     *
     * @var integer
     */
    public function getIdAttribute($value) { return $value; }

    /**
     * @OA\Property(
     *     property="name",
     *     description="Name that the user wishes to be identified by",
     *     format="string",
     *     example="Janet Jackson"
     * )
     *
     * @var string
     */
    public function getNameAttribute($value) { return $value; }

    /**
     * @OA\Property(
     *     property="email",
     *     description="Email that will be used for authentication and notifications",
     *     format="string",
     *     example="test@example.com"
     * )
     *
     * @var string
     */
    public function getEmailAttribute($value) { return $value; }

    /**
     * @OA\Property(
     *     property="password",
     *     description="Hashed password",
     *     format="string",
     *     example=""
     * )
     *
     * @var string
     */
    public function getPasswordAttribute($value) { return $value; }

    /**
     * @OA\Property(
     *     property="verification_token",
     *     description="Token used to verify the users email",
     *     format="string",
     *     example="fdgd$EGdg42gdf345"
     * )
     *
     * @var string
     */
    public function getVerificationTokenAttribute($value) { return $value; }

    /**
     * @OA\Property(
     *     property="status",
     *     description="String Enum with possible options (active, unverified)",
     *     format="string",
     *     example="active"
     * )
     *
     * @var string
     */
    public function getStatusAttribute($value) { return $value; }

    /**
     * @OA\Property(
     *     property="token",
     *     description="base64 encoded JWT token",
     *     format="jwt",
     *     example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNTdhODliYTJlMjAwMGE5OGVjNzA2NWFlMzQ0M2JhNDg3ZDA1YzZkN2RjMmRmOTQxMGQzMDMyNWRkMjRhNjA2N2E0NjE4NDIyMDQ3YjFjN2EiLCJpYXQiOjE2MTM0NDI5OTQsIm5iZiI6MTYxMzQ0Mjk5NCwiZXhwIjoxNjQ0OTc4OTk0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.KLdg5W6_Itwv6PQDoUGM4slqcWQVu4nlScbBhxWOhMw2RjQbI1DbtpbxNEho4_-e9Pr1LLjUYPhnbi_HYgAu-rXPY1lMA9skDSgTNNwi7owGJMnogaMvJRQrIKFXo6TAG3ZxXZSQZooKhuYtjQXLwGzjXl7jz-AcW8mY2tuGBsPQkU2w03ZlWMCbcO9PZbbtEs5Zv9sLgPNlukYWRR72OU83cifSzIGg0oezimhjq2hwB6KcEq3YErWLbV-MFmvXtikP8t9oFOcdP63uCeNoU4TlZw-tjBrJTUDTDrMDMVRAmxQrc60glVLEVw9WYh87eFZV5b4E0uPXQEVOkf5rHsUtiQJxqDmAA3RmVOJiNWE3q8bMLIllhTCyFqopDGjPLFajIm2qHaRtBYG1JDD3A4d0uoN5HjXXwGt0k0kaN1Q6NaFizSQtibfx330Y8bBpLjbF8inewNiJVmI1xNbURf67nROFlqOV_LuVj1zUUO8gLn-pag16ax7qORpNZD2rNnVdFlpsUMvxM2qG28qt8ZOHr8lafjWEeasdBVCfkAr7cbBzrSfD3lz03b5Oczag-UJ1OfR_H7-YNpEM5P1pxxFjRtbP9w3aZzf83pj-RrrsaierIU0nSfeHIOWnzHd9pER1vrX7SkohFFl7x15dh1rc2x5c09gW0mASwfgvzM"
     * )
     *
     * @var string
     */
    public function getTokenAttribute($value) { return $value; }

}
