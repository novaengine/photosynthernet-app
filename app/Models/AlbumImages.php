<?php

namespace App\Models;

use App\Traits\HasCompositePrimaryKey;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


class AlbumImages extends Model
{

    use HasCompositePrimaryKey;

    protected $table = 'album_images';

    public $timestamps = false;

    protected $primaryKey = ['album_id', 'image_id'];
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'album_id',
        'image_id'
    ];


    public function images() {
        return $this->hasMany(Image::class, 'id', 'image_id');
    }

    public function album() {
        return $this->hasMany(Album::class, 'id', 'album_id');
    }
}
