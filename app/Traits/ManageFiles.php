<?php namespace App\Traits;

use App\Libraries\SpacesClient;
use Illuminate\Support\Facades\Config;

trait ManageFiles {

    public static function storeFile($file) {
        $filename = $file->getClientOriginalName();
        $error = $file->getError();
        if($error != 0) {
            throw new \Exception('File Error : ' . $error);
        }

        SpacesClient::connection();

        $path = $file->store('public/images');

        $path_parts = explode('/', $path);
        $new_name = empty($path_parts) ? '' : $path_parts[count($path_parts) - 1];

        return [
            'original_file_name' => $filename,
            'file_name' => $new_name,
        ];
    }

}
