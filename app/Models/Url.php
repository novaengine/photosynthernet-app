<?php

namespace App\Models;

use App\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;


class Url extends Authenticatable
{

    protected $table = 'urls';
    protected $primaryKey = 'slug';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image_id',
        'visibility',
        'slug'
    ];

    protected $casts = [
        'is_album' => 'boolean'
    ];

    public function image() {
        return $this->belongsTo(Image::class, 'image_id');
    }

    public function getIsAlbumAttribute($value) {
        return (bool) $value;
    }
}
