<?php

namespace App\Http\Controllers;

use App\Console\Commands\PaletteSubscriber;
use App\Events\ImageUpdate;
use App\Events\ImageUploaded;
use App\Http\Requests\PasswordResetRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UserVerifyRequest;
use App\Jobs\SaveImage;
use App\Mail\GridMail;
use App\Models\Album;
use App\Models\Image;
use App\Models\Url;
use App\Models\UserResetToken;
use App\Traits\JSONAPI;
use App\Traits\ManageFiles;
use App\Traits\Pagination;
use App\User;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class UserController extends Controller
{
    use Pagination, JSONAPI, ManageFiles;

    private const UPDATE_ATTRIBUTES = [
        'name'
    ];

    public function read(Request $request, $id){
        $retval = [
            'data' => []
        ];

        $user = User::where('id', $id)->first();
        $authed_user = Auth::user();

        if($user->id == $authed_user->id) {
            unset($user->password);
            $retval['data'] = $user;
        }

        return response($retval, 202);

    }

    public function update(UserUpdateRequest $r, $id) {

        $retval = [
            'data' => []
        ];

        $user = User::where('id', $id)->first();
        $is_logged_in = Auth::check();

        // If the user is logged in then update like normal
        // If the user is not logged in then allow the update based on reset_token


        $authed_user = new User();
        if($is_logged_in) {
            $authed_user = Auth::user();
        } else if($r->has('meta.reset_token')) {
            $token = $r->input('meta.reset_token');
            $reset_token = UserResetToken::where('token', $token)
                            ->where('expiration', '>=', Carbon::now())
                            ->firstOrFail();

            $user_id = $reset_token->user_id;
            $authed_user = User::find($user_id);
        }


        if($user->id == $authed_user->id) {

            // Update the normal attributes
            foreach(self::UPDATE_ATTRIBUTES as $attribute) {

                $request_attribute = $r->input($attribute);

                if(empty($request_attribute)) {
                    continue;
                }

                $user->{$attribute} = $request_attribute;

            }

            // If they have sent a password, update that separately because we need to hash
            if($r->has('data.password')) {
                $r->validate([
                    'data.password' => [
                        'required',
                        'required_with:data.password_confirmation',
                        'same:data.password_confirmation',
                        'min:8',

                    ],
                    'data.password_confirmation' => 'required'
                ]);

                $pass = $r->input('data.password');

                $user->password = Hash::make($pass);
            }

            // Cleanup Token if everything is successful
            if(!empty($reset_token)) {
                $reset_token->forceDelete();
            }

            $user->save();
            $retval['data'] = $user;

        }

        return response($retval, 202);
    }

    /**
     * Unprivileged Endpoints
     */

    /**
     * @param PasswordResetRequest $r
     * @return Response
     */
    public function createReset(PasswordResetRequest $r) {
        $email = strtolower($r->get('email'));

        $user = User::whereRaw('LOWER(email) = ?', [$email])->first();

        if(empty($user)) {
            return response()->json(null, 202);
        }

        $user_id = $user->id;
        $token = (string) Str::uuid();
        $expiration = Carbon::now()->addHours(3);

        try {
            UserResetToken::create([
                'user_id' => $user_id,
                'token' => $token,
                'expiration' => $expiration,
            ]);

            $data = [
                'token' => $token,
                'email' => $user->email,
                'url' => Config::get('app.url'),
            ];

            Mail::to($user->email)->send(new GridMail($data));

        } catch(\Exception $e) {
            Log::error($e);
        }

        return response()->json(null, 202);
    }

    public function readReset(Request $r, $id) {
        $user = User::where('id', $id)->first();

        return response()->api([
            'data' => $user->resets
        ]);

    }


    public function verify(UserVerifyRequest $r, $id) {
        $token = $r->get('verification_token');
        $user = User::where('id', $id)->firstOrFail();

        if($token === $user->verification_token) {

            $user->status = 'active';
            $user->verification_token = '';
            $ret = $user->save();


            if($ret) {
                return redirect(Config::get('app.url') . '?toast=You have successfully verified your email address!');
            }
        }

    }

}
