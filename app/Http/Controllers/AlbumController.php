<?php

namespace App\Http\Controllers;

use App\Console\Commands\PaletteSubscriber;
use App\Events\ImageUpdate;
use App\Events\ImageUploaded;
use App\Jobs\SaveImage;
use App\Models\Album;
use App\Models\Image;
use App\Models\Url;
use App\Services\StorageService;
use App\Traits\JSONAPI;
use App\Traits\ManageFiles;
use App\Traits\Pagination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;

class AlbumController extends Controller
{
    use Pagination, JSONAPI, ManageFiles;


    public function index(Request $request){
        $retval = [
            'data' => [],
            'links' => [],
            'meta' => [
                'filepath' => Config::get('filesystems.image_storage_path'),
                'page' => [

                ],
                'cloud' => [
                    'url' => StorageService::getServingPath(),
                ],
            ]
        ];
        $Album = new Album();
        // If we are not logged in then get a handful of public images
        if(Auth::check()) {
            $user_id = $request->user()->id;
            $Album = $Album->with('url')->where('user_id', $user_id);
        } else {
            $Album = $Album->with(['url' => function($query) {
                return $query->where('visibility', 'public');
            }]);

            $Album = $Album->whereHas('url', function($query) {
                return $query->where('visibility', 'public');
            });
        }

        $includes = [];
        if($request->has('include')) {
            $includes = explode(',', $request->get('include'));
        }

        $available_includes = [
            'images' => 1,
        ];
        $valid_includes = [];
        foreach($includes as $include) {
            if(empty($available_includes[$include])) {
                continue;
            }

            $valid_includes[] = $include;
        }

        $Album = $Album->with($valid_includes);

        $first_page = 1;
        $page = empty($request->get('page')) ? $first_page : (int) $request->get('page');
        $limit = empty($request->get('limit')) ? 12 : (int) $request->get('limit');

        $total_count = $Album->count();

        $pagination = self::getPagination($page, '/api/images', $total_count, $limit);

        $retval['meta']['page'] = $pagination['page'];
        $retval['links'] = $pagination['links'];

        $retval['data'] = $Album->offset(($page * $limit) - $limit)
            ->limit($limit)
            ->orderBy('id', 'desc')
            ->get();


        return response()->api($retval);
    }

    public function read(Request $request, $slug){
        $retval = [
            'data' => [],
            'meta' => [
                'filepath' => Config::get('filesystems.image_storage_path')
            ]
        ];

        $album = Album::with(['url', 'images'])->whereHas('url',function($query) use($slug) {
            $query = $query->where('slug', $slug);
            return $query;
        })->first();

        if(empty($album)) {
            return response([], 404);
        }

        $image_visibility = $album->url[0]->visibility;

        if(Auth::check() && $image_visibility == 'private') {
            $user_id = $request->user()->id;

            if($user_id == $album->user_id) {
                $retval['data'] = $album;

                return response()->api($retval);
            } else {

                return response([
                    'errors' => [
                        [
                            'title' => 'Not Found',
                            'status' => 404
                        ]
                    ]
                ], 404);
            }

        } else if(!Auth::check() && $image_visibility == 'private') {
            return response([
                'errors' => [
                    [
                        'title' => 'Not Found',
                        'status' => 404
                    ]
                ]
            ], 404);
        } else if($image_visibility == 'public') {
            $retval['data'] = $album;

            return response()->api($retval);
        }

        return response([
            'errors' => [
                [
                    'title' => 'Not Found',
                    'status' => 404
                ]
            ]
        ], 404);

    }

    public function create(Request $r) {

        $album = new Album;
        $album->title = $r->get('title');
        $album->description = $r->get('description');
        $album->user_id = Auth::user()->id;
        $album->save();

        $visibility = empty($r->get('visibility')) ? 'private' : $r->get('visibility');

        $slug = uniqid();

        $url = new Url();
        $url->slug = $slug;
        $url->image_id = $album->id;
        $url->visibility = $visibility;
        $url->is_album = true;
        $url->save();

        $album = Album::with('url')->whereHas('url',function($query) use($slug) {
            $query = $query->where('slug', $slug);
            return $query;
        })->first();

        return [
            'id' => $album->id,
            'slug' => $url->slug
        ];

    }

}
