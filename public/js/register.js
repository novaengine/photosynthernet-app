const register = {
    $parent_container : undefined,

    init($parent_container) {
        const self = this;

        self.$parent_container = $parent_container;

        $('.main-container').addClass('fancy-bg');

        self.renderRegister()
    },

    bindActions() {
        const self = this;

        const $register = $('[data-js="register"]');
        $register.unbind();
        $register.on('click', function() {
            self.performRegistration();
        });

        const $inputs = $('[data-js="email"], [data-js="password"], [data-js="password_confirmation"]');
        $inputs.unbind();
        $inputs.on('keyup', function(e) {

            if(e.which === 13) {
                self.performRegistration();
            }

        });

    },

    getRegistrationCredentials() {
        const ids = ['email', 'password', 'password_confirmation', 'terms'];
        let data = {};

        for(let i in ids) {
            const id = ids[i];
            const $el = $('[id="'+id+'"]');

            if($el.attr('type') === 'checkbox') {
                data[id] = $el.is(':checked');
            } else {
                data[id] = $el.val();
            }
        }

        return data;
    },

    performRegistration() {
        const self = this;
        let creds = self.getRegistrationCredentials();

        const prms = api.post('/register', {
            data : {
                email : creds.email,
                password : creds.password,
                password_confirmation: creds.password_confirmation,
                terms: creds.terms,
            }
        });

        prms.done(function(r) {

            app.state.user = r.data;
            app.saveState();

            plausible('Registration', {
                callback: function() {
                    let success_message = 'Successfully registered and logged in dude. Good Job!';
                    window.setTimeout(function() {
                        toast.rise(success_message)
                    }, 750)
                    window.location.href = '/?toast=' + encodeURIComponent(success_message);
                }
            });

        });

        prms.catch(function(data) {
            let payload = self.getRegistrationCredentials();
            payload.status = data.status;
            payload.response = data.responseJSON;

            payload = app.cleanupErrors(payload);

            self.renderRegister(payload)
        })

    },

    renderRegister(data) {
        const self = this;

        data = data || {}
        console.log('Rendering with ', data);

        self.$parent_container.render('register', data).done(function() {
            self.bindActions();
        });
    },



}
modules.register = register;