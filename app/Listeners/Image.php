<?php

namespace App\Listeners;

use App\Events\ImageUploaded;
use App\Models\AnalysisProfile;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Models\Image as ImageModel;
use Illuminate\Support\Facades\Storage;

class Image implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ImageUploaded $event) {

        $image = $event->image;

        $id = DB::select('UPDATE images SET original_file_name = ?, file_name = ? WHERE id = ?', [
            $image->original_file_name,
            $image->file_name,
            $image->id
        ]);

        $image_model = ImageModel::where('id', $image->id)->first();

        $ret = $this->resize('/public/images/' . $image->file_name);

        // Get Analysis Profile Parameters
        $profile = AnalysisProfile::where('id', $image_model->analysis_profile)->first();

        $segments = !empty($profile->segments) ? $profile->segments : 1000;

        $image = ImageModel::where('id', $image->id)->first();
        $meta = $image->meta ?? [];
        $meta['aspect_ratio'] = $ret['aspect_ratio'];
        $image->meta = $meta;
        $image->save();

        $conn = Redis::connection();
        $res = $conn->command('LPUSH', [
            'queues:getimagepalette',
            json_encode([
                'job' => 'findPalette',
                'image_id' => $image->id,
                'file_path' => $image->file_name,
                'user_id' => $image->user_id,
                'segments' => $segments
            ])]);
    }

    private function resize($path) {

        $tmp_name = uniqid('file');
        $tmp_path = '/tmp/' . $tmp_name;
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        $total_tmp_path = $tmp_path . '.' . $extension;


        $file = Storage::disk('s3')->get($path);

        $result = Storage::disk('local')->put(
            $total_tmp_path,
            $file);

        $storage_tmp_path = 'app' . $tmp_path . '.' . $extension;


        Log::info('listeners.image.resize.saved_temp', [
            'tmp_path' => $storage_tmp_path ,
            'fetched_path' => $path,
            'fetch_result' => $result,
            'extension' => $extension
        ]);

        $info = getimagesize(storage_path($storage_tmp_path));

        $mime = $info['mime'];

        $mime_to_ext = [
            'image/png' => 'png',
            'image/jpg' => 'jpeg',
            'image/jpeg' => 'jpeg',
            'image/pjpeg' => 'jpeg',
        ];

        if(empty($mime_to_ext[$mime])) {
            Log::warning('listeners.image.resize.unsupported_mimetype', [
                'tmp_path' => $mime
            ]);
            return false;
        }

        $new_height = 500;

        $ext = $mime_to_ext[$mime];
        $method = "imagecreatefrom" . $ext;

        $src_img = $method(storage_path($storage_tmp_path));

        $old_x = imageSX($src_img);
        $old_y = imageSY($src_img);

        $aspect_ratio = ($old_x / $old_y);

        $coefficient =  $new_height / $old_y;
        if ($new_height / $old_x > $coefficient) {
            $coefficient = $new_height / $old_x;
        }
        $thumb_w = $coefficient * $old_x;
        $thumb_h = $coefficient * $old_y;

        Log::info('listeners.image.resize.dimensions', [
            'w' => $thumb_w,
            'h' => $thumb_h,
        ]);

        $dst_img =   ImageCreateTrueColor($thumb_w,$thumb_h);
        imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

        $dst_img = imagecrop($dst_img, ['x' => 0, 'y' => 0, 'width' => $thumb_w, 'height' => $thumb_h]);

        // New save location
        $name = basename($path);
        $filepath = str_replace($name, '', $path);
        $new_thumb_loc = 'public/images/thumbs/' . $name;
        $tmp_thumb_loc = storage_path('app/tmp/thumbs/' . $name);
        Storage::disk('local')->makeDirectory('tmp/thumbs');


        Log::info('listeners.image.resize.save_location', [
            'new_thumb_loc' => $tmp_thumb_loc
        ]);

        $img_method = "image" . $ext;

        $quality = 80;
        if($ext === "png") {
            $quality = $quality / 10;
        }

        $result = $img_method($dst_img,$tmp_thumb_loc,$quality);

        imagedestroy($dst_img);
        imagedestroy($src_img);

        Storage::disk('s3')->put($new_thumb_loc, Storage::disk('local')->get('/tmp/thumbs/' . $name), 'public');

        unlink(storage_path($storage_tmp_path));
        unlink($tmp_thumb_loc);


        return [
            'resize_result' => $result,
            'aspect_ratio' => $aspect_ratio
        ];
    }

    /**
     * Determine whether the listener should be queued.
     *
     * @param ImageUploaded $event
     * @return bool
     */
    public function shouldQueue(ImageUploaded $event)
    {
        return true;
    }
}
