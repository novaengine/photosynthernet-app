<?php

namespace App\Http\Controllers\Web;

use App\Events\ImageUpdate;
use App\Events\ImageUploaded;
use App\Http\Controllers\Controller;
use App\Jobs\SaveImage;
use App\Listeners\Image;
use App\Models\UserResetToken;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;

class AboutUsController extends Controller
{

    public function index(Request $request){
        return view('about');
    }
}
