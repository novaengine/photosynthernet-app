<?php

namespace App\Http\Controllers;

use App\Console\Commands\PaletteSubscriber;
use App\DTO\BaseDTO;
use App\DTO\ImageDTO;
use App\Events\ImageUpdate;
use App\Events\ImageUploaded;
use App\Jobs\SaveImage;
use App\Models\Album;
use App\Models\Image;
use App\Models\Url;
use App\Repositories\ImageRepository;
use App\Repositories\Interfaces\ImageRepositoryInterface;
use App\Services\ImageService;
use App\Services\StorageService;
use App\Traits\JSONAPI;
use App\Traits\ManageFiles;
use App\Traits\Pagination;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Exception;
use OpenApi\Annotations as OA;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use RuntimeException;

class ImageController extends Controller
{
    use Pagination, JSONAPI;

    /**
     * @var ImageRepositoryInterface
     */
    private $imageRepository;

    const DEFAULT_PER_PAGE = 12;
    const DEFAULT_FIRST_PAGE = 1;

    public function __construct(ImageRepositoryInterface $imageRepository) {
        $this->imageRepository = $imageRepository;
    }

    public function index(Request $request){
        $retval = [
            'data' => [],
            'links' => [],
            'meta' => [
                'cloud' => [
                    'url' => StorageService::getServingPath(),
                ],
                'filepath' => Config::get('filesystems.image_storage_path'),
                'page' => [

                ]
            ]
        ];

        $page = empty($request->get('page')) ? self::DEFAULT_FIRST_PAGE : (int) $request->get('page');
        $limit = empty($request->get('limit')) ? self::DEFAULT_PER_PAGE : (int) $request->get('limit');

        $this->imageRepository->setPage($page);
        $this->imageRepository->setLimit($limit);

        $filter = $request->get('filter');

        $is_public = !Auth::check() || (!empty($filter['visibility']) && $filter['visibility'] === 'public');

        $images = [];
        if(!$is_public) {
            /** @var User $user */
            $user = Auth::user();
            $images = $this->imageRepository->getAllByUser($user);
        } else {
            $images = $this->imageRepository->getPublic();
        }

        $total_count = $this->imageRepository->getCount();

        $retval['data'] = $images;

        $pagination = self::getPagination($page, '/api/images', $total_count, $limit);

        $retval['meta']['page'] = $pagination['page'];
        $retval['links'] = $pagination['links'];

        return response()->api($retval);
    }

    /**
     *
     * @OA\Get(
     *      path="/images/{slug}",
     *      operationId="read@ImageController",
     *      tags={"Images"},
     *      summary="Fetches a single image",
     *      description="Fetches a single public image if the user
     *                  is unauthenticated or a single private image if the user
     *                  is authenticated and is the image author.",
     *      @OA\Parameter(
     *          description="Unique image slug",
     *          in="path",
     *          name="slug",
     *          example="as3fds3lkj",
     *          @OA\Schema(
     *                type="string",
     *                format="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful image response",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="object",
     *                  format="object",
     *                  ref="#/components/schemas/Image"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Unknown Server Error Occurred",
     *          @OA\JsonContent(ref="#/components/schemas/ServerError")
     *      )
     *
     *
     * )
     *
     * @param Request $request
     * @param $slug
     * @return mixed
     */
    public function read(Request $request, $slug) {
        $retval = [
            'data' => [],
            'meta' => [
                'cloud' => [
                    'url' => StorageService::getServingPath(),
                ],
                'filepath' => Config::get('filesystems.image_storage_path')
            ]
        ];

        $url = Url::where('slug', $slug)->first();

        if(empty($url)) {
            return response([], 404);
        }

        $image = $this->imageRepository->getSingle($slug);

        if(empty($image)) {
            return response([], 404);
        }

        $image = $image[0];

        $image_visibility = $url->visibility;

        if(Auth::check() && $image_visibility == 'private') {
            $user_id = $request->user()->id;

            if($user_id != $image->user_id) {

                return response([
                    'errors' => [
                        [
                            'title' => 'Not Found',
                            'status' => 404
                        ]
                    ]
                ], 404);
            }

            $retval['data'] = [$image];
            $retval['meta']['title'] = $image->title;

        } else if(!Auth::check() && $image_visibility == 'private') {

            return response([
                'errors' => [
                    [
                        'title' => 'Not Found',
                        'status' => 404
                    ]
                ]
            ], 404);
        } else if($image_visibility == 'public') {

            $retval['data'] = [$image];
            $retval['meta']['title'] = $image->title;
        }

        return response()->api($retval);
    }

    public function search(Request $r) {
        $retval = [
            'data' => [],
            'meta' => [
                'cloud' => [
                    'url' => StorageService::getServingPath(),
                ],
                'filepath' => Config::get('filesystems.image_storage_path')
            ]
        ];

        $colors = [
            'crimson' => 'DC143C',
            'red' => 'DC143C',
            'maroon' => 'FF34B3',
            'brownish red' => '472929'
        ];

        $hex = $r->get('filter');
        $hex = explode(',', $hex['hex_codes']);

        foreach($hex as $k => $v) {
            $lower_v = strtolower($v);
            if(!empty($colors[$lower_v])) {
                $v = $colors[$v];
            }
            $hex[$k] = str_replace('#', '', $v);
        }

        $images = Image::with('url')->whereHas('url',function($query) {
            $query = $query->where('visibility', 'public');
            return $query;
        })->where(function($query) use($hex) {
            foreach($hex as $v) {
                $query = $query->orWhereJsonContains('hex_codes', [$v]);
            }
        })->get();

        $retval['data'] = $images;

        return response()->api($retval);

    }

    /**
     *
     * @OA\Post(
     *      path="/images",
     *      operationId="create@ImageController",
     *      tags={"Images"},
     *      summary="Uploads an image",
     *      description="Accepts a multi-part form body with an array of files.",
     *      @OA\Response(
     *          response=202,
     *          description="Image Uploaded",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="object",
     *                  format="object",
     *                  ref="#/components/schemas/Image"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Unknown Server Error Occurred",
     *          @OA\JsonContent(ref="#/components/schemas/ServerError")
     *      )
     *
     *
     * )
     *
     * @param Request $r
     * @return JsonResponse
     * @throws \Exception
     */
    public function create(Request $r) : JsonResponse {
        $image_dto = new ImageDTO($r->all());
        $image_dto->setUserId($r->user()->id);

        $upload_result = ImageService::create($image_dto);

        return response()->json($upload_result);
    }

    public function queue(Request $r, $id) : JsonResponse {
        $user = Auth::user();

        $image = Image::where('user_id', $user->id)->where('id', $id)->first();

        if(empty($image)) {
            return response()->json([], 404);
        }

        $image_dto = new ImageDTO();
        $image_dto->hydrateFromModel($image);

        ImageService::queueImage($image_dto);

        return response()->json(null, 202);

    }

    public function update(Request $r, $slug) {

        $image = Image::whereHas('url', function ($q) use ($slug) {
            $q->where('slug', $slug);
        })->first();

        if(empty($image)) {
            return response([], 404);
        }

        if ($image->user_id !== Auth::user()->id) {
            return response([], 401);
        }

        $updateable_fields = ['title', 'description'];
        foreach ($updateable_fields as $field) {
            $req = $r->get($field);
            if (!empty($req)) {
                $image->{$field} = $req;
            }
        }

        $image->save();

        // Did they update the visiblity flag?
        if (!empty($r->get('visibility'))) {
            $url = Url::where('slug', $slug)->first();

            $vis = $r->get('visibility');

            $url->visibility = $vis === 'public' ? 'public' : 'private';
            $url->save();
        }

        return response([], 202);
    }

    public function delete(Request $r, $slug) {


        $image = Image::whereHas('url', function ($q) use ($slug) {
            $q->where('slug', $slug);
        })->first();

        if(empty($image)) {
            return response([], 404);
        }

        if ($image->user_id !== Auth::user()->id) {
            return response([], 401);
        }

        try {
            $storage_path = storage_path();

            $files = [
                $storage_path . '/app/public/images/' . $image->file_name,
                $storage_path . '/app/public/images/thumbs/' . $image->file_name,
                $storage_path . '/app/public/images/superpixel/' . $image->file_name
            ];

            foreach($files as $path) {
                if(is_file($path)) {
                    unlink($path);
                }
            }

            Cache::tags(ImageRepository::USER_CACHE_TAGS)->flush();
            Cache::tags(ImageRepository::PUBLIC_CACHE_TAGS)->flush();
        } catch(\Exception $e) {
            Log::error($e);
            return response([], 500);
        }

        $image->forceDelete();
        return response([], 202);
    }

    /**
     * @param Request $r
     *
     * RPC endpoint for running the color indexing command. It's unsafe for now, but meh, what are they gonna do,
     * make sure the color index is super up to date? lol
     *
     * @return mixed
     */
    public function indexColors(Request $r) {
        Artisan::call('command:colors');

        return response([], Response::HTTP_NO_CONTENT);
    }

}
