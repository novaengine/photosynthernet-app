<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="Image",
 *     description="Image Model",
 *     @OA\Info(
 *      title="Image",
 *     )
 * )
 */
class Image extends Model
{

    protected $table = 'images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'original_file_name',
        'file_name',
        'palette_name',
        'hex_codes',
        'user_id',
        'title',
        'description'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function albumImages() {
        return $this->belongsTo(AlbumImages::class, 'id', 'image_id');
    }

    public function url() {
        return $this->hasMany(Url::class, 'image_id')->whereRaw('is_album = false');
    }

    public function getMetaAttribute($value) {
        if(empty($value)) {
            $value = '[]';
        }

        if(gettype($value) === 'string') {
            $value = json_decode($value, true);
        }

        return $value;
    }

    public function getHexCodesAttribute($value) {
        if(empty($value)) {
            $value = '[]';
        }
        $value = json_decode($value, true);

        $value = array_slice($value, 0, 6);

        return $value;
    }

    public function scopeWithUrls($query) {
        $query = $query->join('urls', 'images.id', '=', 'urls.image_id');
        $query = $query->select('images.*');
    }

    /**
     * @OA\Property(
     *     property="id",
     *     description="Primary auto-incrementing sequential identifier",
     *     format="int64",
     *     example=123
     * )
     *
     * @var integer
     */
    public function getIdAttribute($value){return $value;}

    /**
     * @OA\Property(
     *     property="original_file_name",
     *     description="The name of the file when it was first uploaded.
     *                  Cannot be used to access the file, only used for
     *                  meta reference.",
     *     format="string",
     *     example="Project_Final_Final-not-a-hack.jpg"
     * )
     *
     * @var string
     */
    public function getOriginalFileNameAttribute($value){return $value;}

    /**
     * @OA\Property(
     *     property="file_name",
     *     description="The name of the file as it is stored on the server.
     *                  Auto-generated and checked for collisions before saving.",
     *     format="string",
     *     example="f32jrkjxhvhbvhfgfdskjgh.jpeg"
     * )
     *
     * @var string
     */
    public function getFileNameAttribute($value) {return $value;}

    /**
     * @OA\Property(
     *     property="palette_name",
     *     description="The friendly name given to that particular image palette.",
     *     format="string",
     *     example="Ocean Breeze"
     * )
     *
     * @var string
     */
    public function getPaletteNameAttribute($value){return $value;}

    /**
     * @OA\Property(
     *     property="hex_codes",
     *     description="An array of colors formatted as hex codes. This property forms the palette.",
     *     format="json",
     *     example="['ffffff', '262626']"
     * )
     *
     * @var string
     */
    public function getHexCodes($value){return $value;}

    /**
     * @OA\Property(
     *     property="user_id",
     *     description="Primary identifier of the user that uploaded/owns the image.",
     *     format="int64",
     *     example="456"
     * )
     *
     * @var integer
     */
    public function getUserIdAttribute($value) {return $value;}

    /**
     * @OA\Property(
     *     property="title",
     *     description="User friendly title of the image",
     *     format="string",
     *     example="My View From the Hotel Window"
     * )
     *
     * @var string
     */
    public function getTitleAttribute($value){return $value;}

    /**
     * @OA\Property(
     *     property="descriptions",
     *     description="More detailed description of the image.",
     *     format="string",
     *     example="This is from my vacation to Cancun! We were right on the beach!"
     * )
     *
     * @var string
     */
    public function getDescriptionAttribute($value){return $value;}

}
