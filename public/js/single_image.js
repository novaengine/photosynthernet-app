const single_image = {

    $parent_container : undefined,

    slug : '',

    libraries_loaded : false,

    image_data : {},

    scroll_timeout : undefined,

    layout : 'list',

    init($parent_container) {
        const self = this;

        self.$parent_container = $parent_container

        const path = app.settings.path;

        if(path.length > 2) {
            self.slug = path[2];
        }


        self.scripts = [
            'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.6.0/chart.min.js'
        ]

        self.styles = [
            'https://cdn.jsdelivr.net/npm/bulma-divider@0.2.0/dist/css/bulma-divider.min.css',
        ]

        let control_data = app.store.get(control.store_key) || control.default_state;
        self.layout = control_data.layout;

        app.loadLibraries(self.scripts, self.styles).done(function(r) {
            self.renderImage();
            self.listen()
        });
    },

    listen() {
        const self = this;
        observer.subscribe('single_image', function(message, data, bus) {
            console.log(message, data, bus)
            switch(message) {
                case 'layout_change':
                    self.layout = data.layout;
                    self.renderImage(self.image_data)
                    break;
            }
        })
    },

    hydrateActiveColor(image, active_color) {

        image.attributes.active_color = active_color;

        if(image.attributes.meta['shades'] !== undefined && image.attributes.meta['shades'][active_color] !== undefined) {
            image.attributes.active_shades = image.attributes.meta['shades'][active_color]
        }

        if(image.attributes.meta['tints'] !== undefined && image.attributes.meta['tints'][active_color] !== undefined) {
            image.attributes.active_tints = image.attributes.meta['tints'][active_color]
        }

        return image;
    },

    cacheImageData(data) {
        const self = this;

        self.image_data = data;
    },

    renderImage(data) {
        const self = this;

        let prms;

        if(data === undefined) {
            if(app.settings.is_album === true) {
                prms = Album.getImagesBySlug(self.slug);
            } else {
                prms = PImage.getBySlug(self.slug);
            }
        } else {
            prms = $.Deferred().resolve(self.image_data);
        }


        prms.done(function(r) {


            console.log(r);
            for(let i in r.data) {
                let image = r.data[i];

                image = self.hydrateActiveColor(image, image.attributes.hex_codes[0]);
                r.data[i] = image;

            }
            r.meta.layout = self.layout
            self.cacheImageData(r);

            let image_data = r

            self.$parent_container.render('image', {
                images : r.data,
                meta : r.meta,
            }).done(function(r) {
                self.bindActions();

                index.lazyLoadImageRender();

                // Always show the first tab on render
                $('[data-tab=0]').removeClass('is-hidden');

                console.log(image_data);

                $.each(image_data.data, function(ind, image) {
                    const $parent = $('[data-image_id='+image.attributes.id+']');
                    const $el = $parent.find('[data-js="shade_tint_chart"]')

                    let labels = [];
                    let datasets = [];
                    let datacolors = [];

                    function sort_object(obj) {
                        let items = Object.keys(obj).map(function(key) {
                            return [key, obj[key]];
                        });
                        items.sort(function(first, second) {
                            return second[1] - first[1];
                        });
                        return items
                    }

                    console.log(image.attributes.meta.palette_score);

                    let palette_score = sort_object(image.attributes.meta.palette_score)

                    console.log(palette_score)

                    $.each(palette_score, function(oind, val){
                        labels.push('#' + val[0].toUpperCase())
                        datasets.push(val[1])
                        datacolors.push('#' + val[0])
                    })

                    const config = {
                        type: 'doughnut',
                        options: {
                            plugins : {
                                legend: {
                                    position: "left"
                                },
                            }
                        },
                        data: {
                            labels: labels,

                            datasets: [{
                                label: 'Palette Color Distribution',
                                data: datasets,
                                backgroundColor: datacolors,
                                hoverOffset: 4
                            }],
                        }
                    };
                    console.log(config)
                    const chart = new Chart(
                        $el[0],
                        config

                    )

                });

            })

        });
    },

    bindActions() {
        const self = this;

        app.bindLazyLoad();

        const $queue_button = $('[data-js=queue_image]');
        $queue_button.unbind();
        $queue_button.on('click', function() {

            const id = $(this).data('id');
            const $el = $(this);
            $el.find('svg').addClass('fa-spin');
            PImage.queue(id).done(function() {
                $el.find('svg').removeClass('fa-spin');

                toast.rise('Image has been queued for palette and thumbnail refresh', {
                    timeout : 5000
                })

            })

        })

        const $canva_button =  $('[data-js="canva_button"]');
        $canva_button.unbind();
        $canva_button.on('click', async function() {
            console.log('here')
            if (!window.Canva || !window.Canva.DesignButton) {
                return;
            }

            const api = await window.Canva.DesignButton.initialize({
                apiKey: "AAEAC0JBRDY1N3RaYW40",
            });

            api.createDesign({
                design: {
                    type: "Poster",
                },
            });


        });

        const $delete_button = $('[data-js=delete_image]');
        $delete_button.unbind();
        $delete_button.on('click', function() {

            const $el = $(this);

            const confirmed = $(this).attr('data-confirm');
            if(confirmed === 'true') {
                const slug = $(this).data('slug')
                PImage.delete(slug).done(function() {
                    $el.find('[data-js=confirm_message]').html('')
                    $el.attr('data-confirm', 'true');
                    $el.removeClass('is-danger');
                    $el.addClass('is-success');
                    $el.find('svg').remove();
                    $el.prepend('<i class="fa fa-check"></i>');
                    $el.find('[data-js=confirm_message]').css('padding-left', '0')

                    window.setTimeout(function() {
                        $el.fadeOut();
                    }, 1500)

                });

            } else {
                $el.find('[data-js=confirm_message]').css('padding-left', '10px').html('Confirm?')
                $el.attr('data-confirm', 'true');
            }

        });

        const $tabs = $('.tabs .tab');
        $tabs.unbind();
        $tabs.on('click', function() {
            const show_container = $(this).data('js');
            const $parent = $(this).closest('[data-js="tabs_parent"]')

            let $clickable_tabs = $parent.find('.tabs .tab');
            $clickable_tabs.removeClass('is-active');

            let tab_id = $(this).data('js');

            $('.tab[data-js="'+tab_id+'"]').addClass('is-active');

            const $selected_container = $parent.find('[data-tab="'+show_container+'"]');

            console.log($selected_container);

            const $all_tabs = $parent.findExclude('[data-tab]', '[data-js="tabs_parent"]');
            $all_tabs.addClass('is-hidden');

            $selected_container.removeClass('is-hidden');

            $selected_container.find('img').each(function() {
                if(!$(this).attr('src')) {
                    $(this).attr('src', $(this).data('click-src'));
                }
            })

        });

        const $color_select = $('[data-js="color_select"]');
        $color_select.unbind();
        $color_select.on('click', function() {

            const id = $(this).attr('data-id');
            const $el = $('[data-js="color_info"][data-id="'+id+'"]');


            let image = self.image_data[id];
            image = self.hydrateActiveColor(image, $(this).data('color'));

            $el.render('partial_color_info', image).done(function(r) {
                self.bindActions();
            })



        });

        const $color_variant = $('[data-js="color_variant"]');
        $color_variant.unbind();
        $color_variant.on('click', function() {

            const id = $(this).attr('data-id');
            const $el = $('[data-js="color_info"][data-id="'+id+'"]');


            let image = self.image_data[id];
            image = self.hydrateActiveColor(image, $(this).data('color'));

            $el.render('partial_color_info', image).done(function(r) {
                self.bindActions();
            })



        });

        const $copy_color = $('.copy-color');
        $copy_color.unbind();
        $copy_color.on('click', function(e) {
            let color = $(this).find('.color-text').text();
            self.copyToClipboard(color);
            const $copy_response = $(this).parent().find('.toast-message');

            const child_count = $(this).parent().children().length;

            let width = $(this).parent().width();
            let color_width = width / child_count;

            let idx = $(this).index();

            let offset = (color_width * idx) - (color_width / 2) - 8;
            $copy_response.css('left', offset + 'px')


            $copy_response.html('Copied to Clipboard!')
            $copy_response.removeClass('hide');
            $copy_response.removeClass('--fly-fade-out-down');
            $copy_response.addClass('--fly-fade-in-up');

            if(self.timeout !== undefined) {
                window.clearTimeout(self.timeout);
            }

            self.timeout = window.setTimeout(function() {
                $copy_response.addClass('--fly-fade-out-down');
                $copy_response.removeClass('--fly-fade-in-up');

                self.timeout = window.setTimeout(function() {
                    $copy_response.attr('style', '');
                }, 200);

            }, 2000)
        });
    },

    copyToClipboard(str) {
        const el = document.createElement('textarea');
        el.value = str;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }

}

modules.single_image = single_image;

