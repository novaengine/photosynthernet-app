<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;

class SaveImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $file_path;
    private $file_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file_path, $file_name)
    {
        $this->file_path = $file_path;
        $this->file_name = $file_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

//        DB::insert('INSERT INTO images (original_file_name, file_name) VALUES (?, ?)', [$this->file_name, $this->file_path]);
//
//        $conn = Redis::connection();
//        $res = $conn->command('LPUSH', ['queues:getimagepalette', json_encode(['job' => 'findPalette', 'file_path' => $this->file_path])]);
//        var_dump($res);
    }
}
