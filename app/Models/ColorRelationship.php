<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="ColorRelationship",
 *     description="Model for representing the relationships between compound colors and base colors",
 *     @OA\Info(
 *      title="ColorRelationship",
 *     )
 * )
 */
class ColorRelationship extends Model
{

    protected $table = 'color_relationships';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'origin_color_id',
        'related_color_id',
    ];

    public function color() {
        return $this->hasOne(ColorInfo::class, 'id', 'related_color_id');
    }

    /**
     * @OA\Property(
     *     property="id",
     *     description="Primary auto-incrementing sequential identifier",
     *     format="int64",
     *     example=123
     * )
     *
     * @var integer
     */
    public function getIdAttribute($value){ return $value; }

    /**
     * @OA\Property(
     *     property="origin_color_id",
     *     description="Usually the compound color",
     *     format="integer",
     *     example="123"
     * )
     *
     * @var string
     */
    public function getOriginColorIdAttribute($value){ return $value; }

    /**
     * @OA\Property(
     *     property="related_color_id",
     *     description="Usually the base color",
     *     format="integer",
     *     example="456"
     * )
     *
     * @var string
     */
    public function getRelatedColorIdAttribute($value){ return $value; }

}
