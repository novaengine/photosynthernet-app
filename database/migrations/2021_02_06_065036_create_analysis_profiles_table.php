<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalysisProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analysis_profiles', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name');
            $table->biginteger('user_id');
            $table->string('superpixel_algorithm')->default('slic');
            $table->integer('segments')->default(1000);

            $table->index('user_id', 'ap_user_id_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analysis_profiles');
    }
}
