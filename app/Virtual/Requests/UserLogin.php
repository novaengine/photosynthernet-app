<?php namespace App\Virtual\Requests;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="UserLogin",
 *     description="Required user info authenticate as a user",
 *     @OA\Info(
 *      title="Requests",
 *     )
 * )
 */
class UserLogin
{

    /**
     * @OA\Property(
     *     title="data",
     *     description="Required login information",
     *     type="object",
     *     format="document",
     *     @OA\Property(
     *         property="email",
     *         description="RFC5322 compliant email address : https://www.rfc-editor.org/rfc/rfc5322.txt",
     *         example="test@example.com",
     *         type="string",
     *         format="email"
     *     ),
     *     @OA\Property(
     *         property="password",
     *         description="Minimum 8 character password",
     *         example="AReallyStrongPassword",
     *         type="string"
     *     )
     * )
     *
     * @var string
     */
    public $data = [];

}