<?php

namespace App\Repositories\Interfaces;

use App\Models\Image;
use App\User;
use Illuminate\Database\Eloquent\Builder;

interface ImageRepositoryInterface
{
    public function all();

    /**
     * @param User $user
     * @return array
     */
    public function getAllByUser(User $user): array;

    public function getSingle(string $slug, bool $is_album = false): array;

    /**
     * @return array
     */
    public function getPublic(): array;
    public function getAllPublicModel(): Builder;

    public function getCount(): int;

    /**
     * Getters and Setters
     */


    public function getLimit(): int;
    public function setLimit(int $limit): void;

    public function getPage(): int;
    public function setPage(int $page): void;

    public function getCurrentModel(): Builder;
    public function setCurrentModel(Builder $currentModel): void;

}