const upload = {

    $parent_container : undefined,

    current_images : undefined,

    init($parent_container) {
        const self = this;
        self.$parent_container = $parent_container;

        self.loadLibraries().done(function(r) {
            self.renderStep1();
        });
    },

    loadLibraries() {
        const self = this;
        self.scripts = [
            'https://cdn.jsdelivr.net/npm/bulma-slider@2.0.4/dist/js/bulma-slider.min.js',
        ]

        self.styles = [
            'https://cdn.jsdelivr.net/npm/bulma-slider@2.0.4/dist/css/bulma-slider.min.css',
            'https://cdn.jsdelivr.net/npm/bulma-divider@0.2.0/dist/css/bulma-divider.min.css'
        ]

        return app.loadLibraries(self.scripts, self.styles)
    },

    renderStep1() {
        const self = this;

        self.$parent_container.render('upload-1').done(function() {
            $('.card').hide();
            self.bindActions();
        });
    },

    getSettings() {

        // Map of DOM [name] to an object containing information about that property
        // like default value and expected api json property name
        const settings_map = {
            visibility : {
                api_key : 'visibility',
                default : 'private',
            },
            album_title : {
                api_key : 'title',
                default : 'Album',
            },
            album_description : {
                api_key : 'description',
                default : 'Album Description',
            }
        }

        let data = {};

        for(let name in settings_map) {
            console.log(name);
            const meta = settings_map[name];

            const api_key = meta.api_key;
            const setting_default =  meta.default;

            const $sel = $('[name="'+name+'"]');
            const value = $sel.val() || setting_default;

            data[api_key] = value;
        }

        return data;
    },

    createAlbumIfNecessary(is_album) {
        const self = this;

        const album_promise = $.Deferred();
        const settings = self.getSettings();

        if(is_album) {

            // Check to see if we have an album selected
            let existing_album = $('[id="existing_album_title"]').val();
            if(existing_album !== '') {
                album_promise.resolve(existing_album);
                return album_promise;
            }


            const prms = Album.create(settings);
            prms.done(function(r) {
                let album_id = r.slug;
                album_promise.resolve(album_id);
            })
                .catch(function(r) {

                    let response = r.responseJSON;

                    if(response.errors === undefined) {
                        toast.rise('Something bad happened during upload, we don\'t know why. Sorry...' , {
                            class : 'is-danger'
                        });
                        return false;
                    }

                    for(let i in response.errors) {
                        const error = response.errors[i];
                        toast.rise(error.title, {
                            class: 'is-danger',
                            timeout : 5000,
                        });
                    }

                    return false;
                })
        } else {
            album_promise.resolve(false);
        }

        return album_promise;
    },

    readyImages(files) {
        const self = this;
        let file_payload = {
            images : [],
            is_album : false,
        };

        self.current_images = [...files];

        if(files.length > 50) {
            toast.rise('Please select fewer than 10 images!', {
                'class' : 'is-warning'
            });
            return false;
        }

        $.each(files, function(ind, file) {


            let name = file.name;
            name = name.split('.').slice(0, -1).join('.')

            const blob = URL.createObjectURL(file);

            let formatted_file = {
                name : name,
                blob : blob,
            };

            file_payload.images.push(formatted_file);
        });


        file_payload.is_album = self.current_images.length > 1;

        self.$parent_container.render('upload-2', file_payload).done(function() {
            window.setTimeout(function() {
                $('.card').addClass('visible');
            }, 2);

            if(bulmaSlider) {
                bulmaSlider.attach();
            }
            self.bindActions();
            album_list.init();
            let $album_container = $('[data-js=album_container]');
            if(file_payload.is_album) {
                $album_container.show();
            }

        });
    },

    bindActions() {
        const self = this;


        const $image = $('[name="image"]');
        $image.unbind();
        $image.on('change', function () {
            let file_input = $('[name="image"]');
            let files = file_input[0].files;

            self.readyImages(files)

        });

        const $upload = $('[data-js=upload]');
        $upload.unbind();
        $upload.on('click', function () {
            const $this = $(this);
            $this.html('<i class="fa fa-circle-notch fa-spin"></i>');
            $this.attr('disabled', 'disabled');

            const files = self.current_images.filter(file => file !== undefined);
            let files_counter = files.length;

            // Get Settings
            const visibility = $('#visibility').val();
            const is_album = files_counter > 1;

            const creating_album = self.createAlbumIfNecessary(is_album)
            let promises = [];

            creating_album.done(function(album_id) {
                let redirect_slug = '';

                let upload_endpoint = '/images';
                redirect_slug = album_id;
                if(is_album) {
                    upload_endpoint = '/albums/' + album_id + '/images';
                }

                let progress_bar = $('[data-js="progress_bar"]').progressBar({
                    max : files.length,
                    value : 0,
                })

                $.each(files, function(ind, file) {
                    if(file === undefined) {
                        return true;
                    }

                    console.log(file)
                    const file_title = file.name.split('.').slice(0, -1).join('.');

                    const title = $('[data-js="image_title_' + file_title + '"]').val();
                    const description = $('[data-js="image_desc_' + file_title + '"]').val();
                    let visibility = $('#visibility').val();

                    let formData = new FormData();
                    formData.append('file', file);
                    formData.append('title', title);
                    formData.append('description', description);
                    formData.append('visibility', visibility);

                    $('span.file-label').html('<i class="fas fa-circle-notch fa-spin"></i>');
                    $('.file-icon').hide();
                    const prms = PImage.upload(upload_endpoint, formData);
                    prms.done(function(r) {
                        progress_bar.increment(1);
                        if(!is_album) {
                            redirect_slug = r.slug;
                        }
                    })
                    prms.catch(function(r) {

                        let response = r.responseJSON;

                        if(response.errors === undefined) {
                            toast.rise('Something bad happened during upload, we don\'t know why. Sorry...' , {
                                class : 'is-danger'
                            });
                            progress_bar.remove();
                            $this.removeClass('is-primary')
                            $this.addClass('is-danger')
                            $this.addClass('error-shake')
                            $this.html('Error <i style="margin-left: 5px;" class="fa fa-times"></i>');
                            window.setTimeout(function() {
                                $this.removeClass('is-danger')
                                $this.addClass('is-primary')
                                $this.removeAttr('disabled');
                                $this.removeClass('error-shake')
                                $this.html('Upload');
                            }, 1000)

                            return false;
                        }

                        for(let i in response.errors) {
                            const error = response.errors[i];
                            toast.rise(error.title, {
                                class: 'is-danger',
                                timeout : 5000,
                            });
                        }

                        return false;
                    })
                    promises.push(prms);
                });

                $.when(...promises).done(function() {

                    $this.removeClass('is-primary')
                    $this.addClass('is-success')
                    $this.html('Uploaded <i style="margin-left: 5px;" class="fa fa-check"></i>');

                    plausible('Image Upload', {
                        callback : function() {
                            // toast.rise('All files uploaded successfully!')
                            window.setTimeout(function() {
                                if(is_album) {
                                    window.location.href = '/albums/' + redirect_slug;
                                } else {
                                    window.location.href = '/images/' + redirect_slug;
                                }
                            }, 1500)
                        },
                        props : {
                            is_album : is_album,
                            image_count : files.length,
                            visibility : visibility,
                        }
                    });

                })

            })

        })

    }


}

const album_list = {

    $parent_container : undefined,

    album_data : {},

    init() {
        const self = this;

        self.$parent_container = $('[data-js=existing_album_list]');
        Album.index(1, 100).done(function(r) {
            console.log(r);

            self.render(r)

        });

    },

    hydrateOptionsModel(album_data) {
        const self = this;
        let options = [];

        $.each(album_data, function(ind, val) {
            options.push({
                value : val.attributes.url[0].slug,
                label : val.attributes.title,
                id : val.id,
            });
            self.album_data[val.id] = val;
        })

        return options;
    },

    render(data) {
        const self = this;
        const options = self.hydrateOptionsModel(data.data)
        self.$parent_container.render('dropdown', {
            options : options,
        }).done(function(r) {
            self.bindActions();
        })
    },

    bindActions() {
        const self = this;

        let $remove_from_upload = $('[data-js=remove_from_upload]');
        $remove_from_upload.unbind();
        $remove_from_upload.on('click', function(r) {

            let $parent = $(this).closest('[data-js="image_upload_card"]');
            const id = $parent.attr('data-image-id');
            delete upload.current_images[id];
            console.log(upload.current_images)
            $parent.fadeOut(300, function() {
                $(this).remove();
                if($('[data-js="image_upload_card"]').length <= 0) {
                    upload.renderStep1();
                }
            });
        })

        let $existing_album = $('[id="existing_album_title"]');
        $existing_album.unbind();
        $existing_album.on('change', function() {
            let val = $(this).val();
            let id = $(this).find('option:selected').attr('data-id');
            let $album_container = $('[data-js=album_container]');
            let $visibility = $('[data-js=visibility]');
            if(val !== '') {
                $album_container.find('input').val('');
                $album_container.find('textarea').val('');
                $album_container.hide();

                $visibility.attr('disabled', 'disabled')
                $visibility.val(self.album_data[id].attributes.url[0].visibility)

            } else {
                $album_container.show();
                $visibility.removeAttr('disabled')
            }
        })
    }
}

modules.upload = upload;