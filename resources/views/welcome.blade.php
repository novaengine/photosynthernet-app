@extends('layouts.main')

@section('content')
    @parent

    <div class="container app-container">

    </div>

@endsection

@section('scripts')

    @if(!empty($schema_data))
        <script type="application/ld+json">
            {!! json_encode($schema_data) !!}
    </script>
    @endif

<script>

    app.settings = {!! json_encode($settings) !!}

    app.init();

</script>

@endsection
