@extends('layouts.main')

@section('content')
    @parent
    <div class="container">
        <div class="section">
            <h1 class="title is-1">Upload a File</h1>
        <div class="columns first-image">

            <div class="column is-3">
                <div class="file is-boxed">
                    <label class="file-label">
                        <input class="file-input" type="file" name="image" multiple accept="image/jpeg,image/png">
                        <span class="file-cta">
                    <span class="file-icon">
                        <i class="fas fa-upload"></i>
                    </span>
                    <span class="file-label">
                        Choose a file…
                    </span>
                </span>
                    </label>
                </div>
                <div class="section">

                <button class="button" id="upload">
                    Upload
                </button>
                </div>
            </div>

            <div class="column is-6 image-repeat">

                <div class="card is-dark">
                    <header class="card-header">
                        <p class="card-header-title">
                            <input id="image_title" type="text" class="input" placeholder="Image title...">
                        </p>
                    </header>
                    <div class="card-image">
                        <figure class="image image-preview"></figure>
                    </div>
                    <div class="card-content">
                        <textarea id="image_description" class="textarea" placeholder="Some description about the image.."></textarea>
                    </div>
                    </div>
                </div>

            <div class="column is-3">
                <div class="card is-dark">
                    <header class="card-header">
                        <p class="card-header-title">
                            Settings
                        </p>
                    </header>
                    <div class="card-content">
                        <label class="label" zfor="visibility">Visibility</label>
                        <div class="select">
                            <select id="visibility" class="is-primary">
                                <option value="private">Private</option>
                                <option value="public">Public</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            </div>

        </div>

    </div>
@endsection

@section('scripts')
    <script>

        const upload = {

            files : undefined,

            init() {

                const self = this;


                self.bindActions();
            },

            bindActions() {
                const self = this;



                const $upload = $('button#upload');
                $upload.unbind();
                $upload.on('click', function () {

                    const fileInput = $('[name="image"]');
                    const files = fileInput[0].files;
                    let files_counter = files.length;

                    // Do we have more than one file to upload?
                    let is_album = files.length > 1;

                    // Create the Global Promise
                    const album_promise = $.Deferred();
                    let album_id = 0;

                    if(is_album) {
                        const prms = self.createAlbum();
                        prms.done(function(r) {
                            album_id = r.slug;
                            album_promise.resolve();
                        })
                    } else {
                        album_promise.resolve();
                    }

                    const image_promise = $.Deferred();
                    let redirect_slug = '';

                    album_promise.done(function(r) {

                        // Once the album promise is resolved we can begin to upload the actual images

                        let upload_endpoint = '/api/images';
                        redirect_slug = album_id;
                        if(is_album) {
                            upload_endpoint = '/api/albums/' + album_id + '/images';
                        }

                        $.each(files, function(ind, file) {

                            const title = $('#image_title').val();
                            const description = $('#image_description').val();
                            let visibility = $('#visibility').val();

                            let formData = new FormData();
                            formData.append('file', file);
                            formData.append('title', title);
                            formData.append('description', description);
                            formData.append('visibility', visibility);


                            $('span.file-label').html('<i class="fas fa-circle-notch fa-spin"></i>');
                            $('.file-icon').hide();
                            $.ajax({
                                url: upload_endpoint,
                                type: 'post',
                                headers : {
                                    Authorization : 'Bearer ' + app.state.user.token,
                                },
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: formData,
                                success: function (r) {

                                    if(!is_album) {
                                        redirect_slug = r.slug;
                                    }

                                    files_counter -= 1;
                                    if(files_counter <= 0) {
                                        image_promise.resolve();
                                    }

                                }
                            })

                        });
                    })

                    image_promise.done(function(r) {
                        $('.file-icon').show();
                        $('span.file-label').html('Uploaded Successfully!');
                        $('.card').fadeOut();
                        window.location.href = '/images/' + redirect_slug;
                        window.setTimeout(function() {
                            $('span.file-label').html('Choose a file...');
                        }, 3000)
                    })

                })

            },

            createAlbum() {
                const visibility = $('#visibility').val();
                console.log(visibility);
                const prms = $.ajax({
                    url: '/api/albums',
                    type: 'post',
                    headers : {
                        Authorization : 'Bearer ' + app.state.user.token,
                    },
                    data: {
                        title : 'test',
                        description : 'test123',
                        visibility : visibility
                    },
                    success: function (r) {

                    }
                })

                return prms;
            },

        }

        upload.init();


    </script>

@endsection
