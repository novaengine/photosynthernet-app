<?php namespace App\Virtual\Controllers;

use OpenApi\Annotations as OA;

class Controller
{
    /**
     * @OA\Info(
     *      version="0.9.1",
     *      title="Photosynthernet API Documentation",
     *      description="Documentation for the Photosynthernet Palette Extraction and Image Hosting API",
     *      @OA\Contact(
     *          email="admin@novaengine.ai"
     *      )
     * )
     *
     * @OA\Server(
     *      url="https://photosynther.net/api",
     *      description="Palette Extraction API"
     * )
     *
     * @OA\Tag(
     *     name="Images",
     *     description="API Endpoints for Image Hosts"
     * )
     */
}