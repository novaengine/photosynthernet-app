<?php namespace App\Traits;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

trait UsesCache {

    /**
     * @return string
     */
    public function getCacheKey(): string
    {
        return $this->cacheKey;
    }

    /**
     * @param string $cacheKey
     */
    public function setCacheKey(string $cacheKey): void
    {
        $this->cacheKey = $cacheKey;
    }

    private function getCache($cache_key, $tags = []) {
        $cached_data = Cache::tags($tags)->get($cache_key);

        return $cached_data;
    }

    private function setCache($cache_key, $data, $tags = []) {
        $cached_data = Cache::tags($tags)->put($cache_key, $data, self::CACHE_EXPIRATION);

        return $cached_data;
    }

    private function generateCacheKey(array $params) {
        $cache_key = md5(json_encode($params));

        $this->setCacheKey($cache_key);

        return $this->getCacheKey();
    }

}
