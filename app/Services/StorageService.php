<?php

namespace App\Services;

use App\DTO\ImageDTO;
use App\Events\ImageUploaded;
use App\Exceptions\FileException;
use App\Libraries\SpacesClient;
use App\Models\Album;
use App\Models\ColorInfo;
use App\Models\Image;
use App\Models\Url;
use App\Repositories\ImageRepository;
use App\Repositories\Interfaces\ImageRepositoryInterface;
use App\Traits\UsesCache;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class StorageService {

    const PROTOCOL = 'https://';

    public function __construct() {

    }

    public static function getServingPath() : string {
        $endpoint = Config::get('filesystems.disks.s3.endpoint');
        $endpoint_len = strlen($endpoint);
        $protocol_len = strlen(self::PROTOCOL);
        $delta_len = $endpoint_len - $protocol_len;

        $bucket = Config::get('filesystems.disks.s3.bucket');
        $short_endpoint = substr($endpoint, $protocol_len, $delta_len);
        $full_endpoint = self::PROTOCOL . $bucket . '.' . $short_endpoint . '/public/images/';

        return $full_endpoint;
    }


}