<?php

namespace App\DTO;

use Illuminate\Database\Eloquent\Model;

class BaseDTO {

    public function __construct($data = []) {

        foreach($data as $property => $value) {
            if(property_exists($this, $property)) {
                $camel_property = ucwords($property, '_');
                $camel_property = str_replace('_', '', $camel_property);
                $method = 'set' . $camel_property;
                $this->$method($value);
            }
        }

    }

    /**
     * @param $method_name
     *
     * If we try to call "set[PropertyName]" on a property with no predefined set method, then we will just assign the
     * property regularly
     *
     * @param $args
     */
    public function __call($method_name, $args) {
        $first_3 = substr($method_name, 0, 3);
        if($first_3 === 'set') {
            $snake_property = $this->decamelize(substr($method_name, 3));
            $this->$snake_property = $args[0];
        }
    }

    /**
     * @param $string
     *
     * Turn camelCase into snake_case.
     * Stolen from : https://stackoverflow.com/a/35719689
     *
     * @return string
     */
    function decamelize($string) {
        return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $string));
    }

    protected function hydrateFromModel(Model $model) {

        foreach($model->getAttributes() as $k => $v) {
            if(property_exists($this, $k)) {
                $this->{$k} = $v;
            }
        }

    }

}