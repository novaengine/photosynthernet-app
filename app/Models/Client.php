<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;


class Client extends Model
{

    protected $table = 'oauth_clients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'name'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
