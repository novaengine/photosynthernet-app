@extends('layouts.main')

@section('content')
    @parent
    <div class="container profile-container">

    </div>
@endsection

@section('scripts')
    <script>

        const profile = {

            data : undefined,

            init() {

                const self = this;

                const images = app.getImages(1);
                const profile = self.getProfile();

                const promises = [images, profile];

                $.when(...promises).done(function(r, r2) {

                    let image_data = r[0];
                    const profile_data = r2[0];

                    self.data = profile_data.data;



                    self.render(profile_data.data).done(function() {
                        image_data = app.processImagesResponse(image_data)
                        self.renderImages(image_data)
                    });
                });

            },

            render(data) {
                const self = this;

                console.log("data", data);

                return $('.profile-container').render('profile', data).done(function (r) {
                    self.bindActions();
                });
            },

            renderImages(data) {
                const self = this;

                return $('.image-container').render('welcome', data).done(function (r) {
                    self.bindActions();
                });
            },

            getProfile() {
                const self = this;

                return $.ajax({
                    url : '/api/users/' + app.state.user.id,
                    type : 'get',
                    headers : {
                        'Authorization' : 'Bearer ' + app.state.user.token
                    }
                })
            },

            getProfileData() {
                const $profile_container = $('.profile-container');
                const inputs = $profile_container.find('input');

                let data = {}

                $.each(inputs, function(ind, val) {
                    val = $(val);
                    const key = val.attr('name');
                    data[key] = val.val();
                });

                return data;

            },

            updateProfile() {
                const self = this;

                const profile_data = self.getProfileData();

                return $.ajax({
                    url : '/api/users/' + app.state.user.id,
                    type : 'PATCH',
                    headers : {
                        'Authorization' : 'Bearer ' + app.state.user.token
                    },
                    data : profile_data

                })
            },

            bindActions() {

                const self = this;

                const $edit_profile = $('[data-js=edit_profile]');
                $edit_profile.unbind();
                $edit_profile.on('click', function() {

                    self.data.edit = true;
                    self.render(self.data);

                });

                const $save_profile = $('[data-js=save_profile]');
                $save_profile.unbind();
                $save_profile.on('click', function() {
                    self.updateProfile().done(function(r) {
                        self.data = r.data;
                        self.data.edit = false;
                        self.render(self.data);
                    });
                });

                const $cancel_edit_profile = $('[data-js=cancel_edit_profile]');
                $cancel_edit_profile.unbind();
                $cancel_edit_profile.on('click', function() {

                    self.data.edit = false;
                    self.render(self.data);

                });

                const $logout = $('#logout');
                $logout.unbind();
                $logout.bind('click', function (e) {
                    app.logout();
                });

            },
        }

        profile.init();

    </script>

@endsection
