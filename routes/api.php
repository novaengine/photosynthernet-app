<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'Auth\LoginController@login');
Route::post('/register', 'Auth\LoginController@register');
Route::get('/search', 'SearchController@index');

Route::get('/colors', 'ColorInfoController@index');

Route::post('/password/reset', 'UserController@createReset');
Route::get('/users/{id}/verify', 'UserController@verify');

Route::group([
    'middleware' => ['optional.auth:api'],
], function() {
    Route::get('/images', 'ImageController@index');
    Route::get('/images/{slug}', 'ImageController@read');

    Route::get('/albums', 'AlbumController@index');
    Route::get('/albums/{slug}', 'AlbumController@read');
    Route::get('/albums/{slug}/images', 'AlbumImageController@index');

    Route::get('/validate', 'HomeController@validateToken');

    Route::patch('/users/{id}', 'UserController@update');

});

Route::group([
    'middleware' => ['auth:api', 'bindings'],
], function() {

    Route::post('/logout', 'Auth\LoginController@logout');
    Route::post('/images', 'ImageController@create')->middleware('verified');
    Route::patch('/images/{slug}', 'ImageController@update')->middleware('verified');
    Route::delete('/images/{slug}', 'ImageController@delete')->middleware('verified');
    Route::post('/images/{id}/_queue', 'ImageController@queue')->middleware(['verified', 'throttle:10,10']);

    Route::get('/users/{id}', 'UserController@read');
    Route::get('/users/{id}/resets', 'UserController@readReset');
    Route::get('/users/{id}/clients', 'ClientController@index');
    Route::post('/users/{id}/clients', 'ClientController@create');

    Route::get('/analysis-profiles', 'AnalysisProfileController@index');
    Route::post('/analysis-profiles', 'AnalysisProfileController@create');

    Route::post('/albums', 'AlbumController@create')->middleware('verified');
    Route::post('/albums/{slug}/images', 'AlbumImageController@create')->middleware('verified');
    Route::post('/index-colors', 'ImageController@indexColors')->middleware('verified');

});
