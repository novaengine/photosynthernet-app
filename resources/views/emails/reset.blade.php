<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8"/>
</head>
<body>
<h2>Password Reset</h2>
<a href="{{$url}}/forgot?token={{$token}}&email={{$email}}">Reset Your Password Now</a>
</body>
</html>