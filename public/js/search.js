const filters = {

    message_channel : 'search_filter_events',

    // Default State
    default_color : '00d1b2',
    default : {

        colors : {
            id : 'colors',
            label : 'Primary Color',
            tab_active : true,
            values : {
                red : {id : 'red'},
                blue : {id : 'blue'},
                teal : {id : 'teal'},
                green : {id : 'green'},
                orange : {id : 'orange'},
                purple : {id : 'purple'},
                yellow : {id : 'yellow'},
            }
        },


        palette : {
            id : 'palette',
            label : 'Palette',
            values : [
                {color : '00d1b2'}
            ]
        },


    },

    init($parent_container) {
        const self = this;

        self.$parent_container = $parent_container;
        self.render();

        self.bindActions();

        // self.filters = app.store.get('filters') || self.filters;
    },

    bindActions() {
        const self = this;

        const $apply_filters = $('[data-js=apply_filters]');
        $apply_filters.unbind();
        $apply_filters.on('click', function(e) {

            $('[data-js=filter_container]').fadeOut();

            observer.emit(self.message_channel, 'apply_filters');

        });

        const $accordion_header = $('.accordion-header.toggle');
        $accordion_header.unbind();
        $accordion_header.on('click', function() {
            $(this).parent('article.accordion').toggleClass('is-active')
            let filter_type = $(this).data('ftype');

            $(this).find('svg').toggleClass('fa-chevron-up');
            $(this).find('svg').toggleClass('fa-chevron-down');

            console.log("Filter Type : ", filter_type, self.filters)

            self.filters[filter_type].tab_active = $(this).parent('article.accordion').hasClass('is-active');
            app.store.put('filters', self.filters);
        });

        const $add_palette_color = $('[data-js=add_palette_color]');
        $add_palette_color.unbind();
        $add_palette_color.on('click', function(e) {
            self.filters.palette.values.push({color : self.default_color});
            app.store.put('filters', self.filters);
            self.render(self.data);
        });


        const $color_picker = $('[data-js=color_picker]');
        $color_picker.unbind();
        $color_picker.each(function () {
            const $c = $(this);
            const data_id = $c.attr('data-id');
            $c.spectrum({
                showPalette: false,
                showInput: true,
                color : $(this).val(),
                move : function(col) {
                    timer.set('color_picker_' + data_id, function() {
                        $c.attr('value', col.toHexString())

                        const color = col.toHexString();
                        const clean_color = color.replace('#', '');
                        const $checkbox = $c.closest('[data-js=palette_container]').find('[data-js=filter_palette]');
                        const id = $c.attr('data-id');
                        let filter_type = $c.data('ftype');
                        $checkbox.attr('data-color', clean_color);
                        self.filters[filter_type].values[id].color = clean_color;
                        timer.set('color_picker_' + id, function() {
                            app.store.put('filters', self.filters);
                        }, 10);
                    }, 15);

                }
            });
        })
        $color_picker.on('blur', function() {
            self.render(self.data);
        })

        const $clear_all = $('[data-js=clear_all]');
        $clear_all.unbind();
        $clear_all.on('click', function(e) {
            e.stopPropagation();
            for(let i in self.filters.colors.values) {
                self.filters.colors.values[i].selected = false;
            }
            self.filters.palette.values = [{color : self.default_color}]
            app.store.put('filters', self.filters);
            self.render(self.data);

        });

        const $delete_palette_row = $('[data-js=delete_palette_row]');
        $delete_palette_row.unbind();
        $delete_palette_row.on('click', function () {
            const index = $(this).data('index');
            self.filters.palette.values.splice(index, 1);
            self.render(self.data);
        })

        const $clear_palette = $('[data-js=clear_palette]');
        $clear_palette.unbind();
        $clear_palette.on('click', function(e) {
            self.filters.palette.values = [{color : self.default_color}]
            app.store.put('filters', self.filters);
            self.render(self.data);

        })

        const $clear_colors = $('[data-js=clear_colors]');
        $clear_colors.unbind();
        $clear_colors.on('click', function(e) {
            e.stopPropagation();
            for(let i in self.filters.colors.values) {
                self.filters.colors.values[i].selected = false;
            }
            app.store.put('filters', self.filters);
            self.render(self.data);

        })

        const $filter = $('input[data-js^=filter_]');
        $filter.unbind();
        $filter.on('input', function(e) {
            const input_type = $(this).attr('type');
            let selected = false;
            let value = $(this).val();
            let filter_type = $(this).data('ftype');
            let color = '';

            switch(input_type) {
                case 'checkbox':
                    selected = $(this).is(':checked');

                    if($(this).attr('data-color') !== null) {
                        color = $(this).attr('data-color');
                    }

                    break;
            }

            self.filters[filter_type].values[value].selected = selected;

            if(color !== '') {
                self.filters[filter_type].values[value].color = color;
            }

            app.store.put('filters', self.filters);

            self.render(self.data);

        });

    },

    hydrateFilterSelectionState(filters) {

        for(let i in filters) {
            const filter = filters[i];
            let selected = 0;

            for(let v in filter.values) {
                if(filter.values[v].selected === true) {
                    selected++;
                }
            }

            filters[i].count = selected;
        }

        return filters;
    },

    hydratePaletteState(filters) {
        const self = this;

        filters.palette.attributes = {
            hex_codes : []
        }
        for(let i in filters.palette.values) {
            const color = filters.palette.values[i];
            if(color.selected === true) {
                filters.palette.attributes.hex_codes.push(color.color);
            }

        }

        return filters;
    },

    hydrateFilters(filters) {
        const self = this;
        filters = self.hydratePaletteState(filters);
        filters = self.hydrateFilterSelectionState(filters);

        return filters;
    },

    render(data) {
        const self = this;
        let filters = data || self.default;
        filters = self.hydrateFilters(filters);
        self.filters = filters;

        console.log(filters);

        self.$parent_container.render('partial_filters', filters).done(function(r) {
            self.bindActions();
        });

    },

}

const search = {

    // Current Search State
    current_query : '',

    data : {
        data : {},
        meta : {}
    },

    // Filter State in regards to the search component (Visible, where to render, etc.)
    is_filter_visible : false,
    $filter_container : undefined,

    init($parent_container) {
        const self = this;

        if(typeof $parent_container === 'object' && $parent_container.jquery) {
            self.$parent_container = $parent_container;
        }

        self.styles = [
            'https://cdn.jsdelivr.net/npm/bulma-divider@0.2.0/dist/css/bulma-divider.min.css',
            'https://cdn.jsdelivr.net/npm/bulma-switch@2.0.0/dist/css/bulma-switch.min.css',
        ]

        self.initUriQuery();

        app.loadLibraries(self.scripts, self.styles).done( function(r) {
            self.render().done(function() {
                self.$filter_container = $('[data-js=filter_container]');
                filters.init(self.$filter_container);
            })
        });
    },

    initUriQuery() {
        const self = this;
        let url = new URL(window.location.href);

        if(url.searchParams.has('q')) {
            self.current_query = url.searchParams.get('q');
        }
    },


    async query(q) {
        const self = this;
        let data = await PImage.search(q, filters.filters);
        self.image_data = data.data;
        self.data.meta.query = q;
        data.meta.query = q;
        return data;
    },

    bindFilterActions() {
        const self = this;

        const $show_filters = $('[data-js=show_filters]');
        $show_filters.unbind();
        $show_filters.on('click', function(e) {

            $('[data-js=filter_container]').fadeIn();
            self.filter_state = true;
        });
    },

    filters_channel_ref : undefined,

    bindActions() {
        const self = this;

        self.bindFilterActions();

        observer.unsubscribe(filters.message_channel, self.filters_channel_ref);
        let sub = observer.subscribe(filters.message_channel, function(message) {
            switch(message) {
                case 'apply_filters':
                    let q = $('[name=main_search]').val();
                    self.query(q)
                    break;
            }
        });
        self.filters_channel_ref = sub['ref']

        const $main_search = $('[name=main_search]');
        $main_search.unbind('input');
        $main_search.on('focus',function(e) {
            const el = $(this)[0]
            setTimeout(function(){ el.selectionStart = el.selectionEnd = 10000; }, 0)
        })
        $main_search.on('input', function(e) {
            const q = $(this).val();
            timer.set('search', async () => {
                let data = await self.query(q)
                self.render(data);
            }, 300 );
        })

    },

    hydrateGlobals(data) {
        const self = this;

        data.is_filter_visible = self.is_filter_visible;
        return data;
    },

    render(data) {
        const self = this;

        if(data === undefined && $.isEmptyObject(self.uri_query)) {
            data = {}
        } else if(data === undefined && self.uri_query !== '') {
            data = (async () => await self.query(self.uri_query))();
        }

        // Meta will handle things like pagination state and
        // meta query information like the query string itself
        data.meta = data.meta || {};

        data.data = self.image_data;

        data = self.hydrateGlobals(data);

        self.data = data;

        return self.$parent_container.render('search', data).done(function() {
            filters.$parent_container = $('[data-js=filter_container]');
            self.bindActions();
            self.bindFilterActions();
            app.bindLazyLoad();
            $('[name=main_search]').focus()
            index.lazyLoadImageRender()
            filters.render();

        });

    }

}

modules.search = search;