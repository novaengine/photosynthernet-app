<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/sitemapindex.xml', 'SitemapController@index');
Route::get('/sitemap{page}.xml', 'SitemapController@map');
Route::get('/robots.txt', 'SitemapController@robots');


Route::get('/search', 'HomeController@search');

Route::get('/images/{slug}/studio', 'HomeController@showStudio');
Route::get('/images/{slug}', 'HomeController@image');
Route::get('/albums/{slug}', 'HomeController@album');
Route::get('/upload', 'HomeController@upload');
Route::get('/profile', 'HomeController@profile');

Route::get('/login', 'HomeController@login');
Route::get('/register', 'HomeController@register');
Route::get('/forgot', 'HomeController@showForgot');

Route::get('/terms', 'Web\TermsAndConditionsController@index');
Route::get('/about-us', 'Web\AboutUsController@index');
Route::get('/about', 'Web\AboutUsController@index');

Broadcast::routes(['middleware' => ['auth:api']]);
