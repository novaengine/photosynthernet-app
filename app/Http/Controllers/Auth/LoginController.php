<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\GridMail;
use App\Mail\VerificationMail;
use App\Models\ColorInfo;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Traits\JSONAPI;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;
use OpenApi\Annotations as OA;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, JSONAPI;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
        if(Auth::check()) {

            $jti = Auth::token()->claims()->get('jti');
            Log::warning($jti);
            $token = $request->user()->tokens->find($jti);

            $token->revoke();

            return response()->api(['data' => []], 205);
        }

        return response()->api(['data' => []], 422);
    }

    /**
     *
     * @OA\Post(
     *      path="/register",
     *      operationId="register@LoginController",
     *      tags={"Authentication"},
     *      summary="Creates and Registers a User",
     *      description="Requires a bare minimum set of information to allow the user to login and upload images.",
     *      @OA\RequestBody(
     *          required=true,
     *          description="Pass user information",
     *          @OA\JsonContent(
     *              required={"email","password", "password_confirmation", "terms"},
     *              ref="#/components/schemas/UserRegistration"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="User Created",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="object",
     *                  format="object",
     *                  ref="#/components/schemas/User"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Unknown Server Error Occurred",
     *          @OA\JsonContent(ref="#/components/schemas/ServerError")
     *      )
     *
     *
     * )
     *
     * @param Request $r
     * @return mixed
     * @throws \Exception
     */
    public function register(Request $request) {
        $rules = [
            'data.email' => 'required|email|unique:users,email',
            'data.password'  => 'required',
            'data.password_confirmation'  => 'required',
            'data.terms' => 'accepted'
        ];

        $request->validate($rules);

        $password = $request->input('data.password');
        $password_confirmation = $request->input('data.password_confirmation');

        if($password !== $password_confirmation) {
            return response()->json([
                'errors' => [
                    'password_confirmation' => 'Password Confirmation must match Password'
                ]
            ], 422);
        }

        $data = [
            'email' => $request->input('data.email'),
            'password'  =>  $password
        ];

        $verification_token = (string) Str::uuid();

        $random_color = ColorInfo::inRandomOrder()->first();

        $created_user = User::create([
            'name' => 'Nameless #' . $random_color->hex,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'status' => 'unverified',
            'verification_token' => $verification_token,
        ]);

        $payload = [
            'token' => $verification_token,
            'email' => $data['email'],
            'url' => Config::get('app.url'),
            'user_id' => $created_user->id
        ];

        Mail::to($data['email'])->send(new VerificationMail($payload));

        if(Auth::attempt($data)) {
            $user = Auth::user();

            try {
                $token = $user->createToken('photosynthernet');
            } catch(\Exception $e) {
                Log::error($e);
                return response()->json([
                    'errors' => [
                        [
                            'title' => 'Error generating auth token'
                        ]
                    ]
                ], 500);
            }

            $user->token = $token->accessToken;


            return response()->json([
                'data' => $user,
            ]);
        }

    }

    /**
     *
     * @OA\Post(
     *      path="/login",
     *      operationId="login@LoginController",
     *      tags={"Authentication"},
     *      summary="Authenticates as a User",
     *      description="Returns a token used to call privileged endpoints locked behind authentication middleware.",
     *      @OA\RequestBody(
     *          required=true,
     *          description="Pass user information",
     *          @OA\JsonContent(
     *              required={"email","password"},
     *              ref="#/components/schemas/UserLogin"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="User Authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="object",
     *                  format="object",
     *                  ref="#/components/schemas/User"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Unknown Server Error Occurred",
     *          @OA\JsonContent(ref="#/components/schemas/ServerError")
     *      )
     *
     *
     * )
     *
     * @param Request $r
     * @return mixed
     * @throws \Exception
     */
    public function login(Request $request)
    {
        $rules = [
            'data.email' => 'required|email',
            'data.password'  => 'required'
        ];

        $request->validate($rules);
        $data = [
            'email' => $request->input('data.email'),
            'password'  =>  $request->input('data.password')

        ];

        if(Auth::attempt($data)) {
            $user = Auth::user();

            try {
                $token = $user->createToken('photosynthernet');
            } catch(\Exception $e) {
                Log::error($e);
                return response()->json([
                    'errors' => [
                        [
                            'title' => 'Error generating auth token'
                        ]
                    ]
                ], 500);
            }

            $user->token = $token->accessToken;

            return response()->json([
                'data' => $user,
            ]);
        }
        else {
            return response()->json([
                'errors' => [
                    'Unauthorized'
                ]
            ], 401);
        }
    }

}
