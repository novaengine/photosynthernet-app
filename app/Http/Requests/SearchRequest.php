<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class SearchRequest extends FormRequest {

    public function rules() {

        return [
            'filter.query' => 'required'
        ];

    }

}