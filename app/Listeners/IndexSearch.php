<?php

namespace App\Listeners;

use App\Events\ImageUploaded;
use App\Events\PaletteGenerated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class IndexSearch implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param PaletteGenerated $event
     * @return void
     */
    public function handle(PaletteGenerated $event) {
        Artisan::call('command:search');
    }

    /**
     * Determine whether the listener should be queued.
     *
     * @param PaletteGenerated $event
     * @return bool
     */
    public function shouldQueue(PaletteGenerated $event)
    {
        return true;
    }
}
