@extends('layouts.main')

@section('content')
    @parent

    <div class="container register-container">

    </div>

@endsection

@section('scripts')

    <script>

        const register = {

            init() {
                const self = this;
                self.render()
            },

            render(data) {
                const self = this;

                if(data === undefined) {
                    data = {}
                }

                $('.register-container').render('register', data).done(function (r) {
                    self.bindActions();
                    app.bindActions();
                })
            },

            registerSuccess() {
                const self = this;
                const email = $('[name=email]').val();

                self.render({
                    success : true,
                    email : email,
                });
                window.setTimeout(function() {
                    window.location.href = '/';
                }, 1000);
            },

            registerFail(e) {
                const self = this;

                console.log(e);

                if(e.status >= 400) {
                    const email = $('[name=email]').val();
                    console.log(email);
                    console.log(self);
                    self.render({
                        email : email,
                        errors : Object.values(e.responseJSON.errors)
                    });
                }
            },

            attemptRegister() {
                const self = this;

                const email = $('[name=email]').val();
                const password = $('[name=password]').val();
                const password_confirmation = $('[name=password_confirmation]').val();

                app.register(email, password, password_confirmation)
                    .done(self.registerSuccess.bind(self))
                    .fail(self.registerFail.bind(self))
                ;
            },

            bindActions() {
                const self = this;

                const $register = $('[id=register]');
                $register.unbind();
                $register.on('click', self.attemptRegister.bind(self))

                const $register_form = $('[name=email], [name=password], [name=password_confirmation]');

                $register_form.on('keyup', function(e) {
                    if(e.which !== 13) {
                        return false;
                    }

                    self.attemptRegister();
                })

            }
        }

        register.init();

    </script>

@endsection
