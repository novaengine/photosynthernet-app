@extends('layouts.main')

@section('content')
    @parent


    <div class="main-container fancy-bg">

        <div class="container app-container">
            <div class="section">

                <div class="columns form-top-card section">

                    <div class="column is-12">
                        <section>

                            <h1 class="title is-1">About Us</h1>
                            <hr />

                            <div class="content">
                                <p>
                                    Welcome to Photosynthernet! An application built around image sharing and exploration.
                                </p>

                                <p>
                                    Our state-of-the-art perceptually uniform color space palette extraction algorithm
                                    enables you to distill your image down to its basest colors. Doing so can facilitate
                                    better color harmony between your natural images and your artificial creative design, or
                                    it can be used to determine what the primary color of your product should really be.
                                </p>

                                <p>
                                    However, this site is primarily about having fun and exploring images with a similar color
                                    palette. Not everything needs to be monetized and not every website needs to boost your
                                    productivity! There can be joy in something existing just for the sake of being fun
                                    and/or interesting. And, that's what we like to think of Photosynthernet, it exists
                                    just because it's kinda neat.
                                </p>

                                <p>

                                    If you want to read about the neat journey we took to build photosynthernet, you can
                                    check out the blog series <a href="https://lostdutchmandevs.com/2020/09/building-photosynthernet-part-1/">available here,</a> and written by our founder.

                                </p>

                                <p>
                                    If you've enjoyed Photosynthernet, or hated it, please feel free to contact us at our primary
                                    email address : <a href="mailto:admin@novaengine.ai">admin@novaengine.ai</a>.
                                </p>


                            </div>

                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
