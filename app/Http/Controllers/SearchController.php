<?php

namespace App\Http\Controllers;

use App\Console\Commands\PaletteSubscriber;
use App\Events\ImageUpdate;
use App\Events\ImageUploaded;
use App\Jobs\SaveImage;
use App\Libraries\Elasticsearch;
use App\Models\Album;
use App\Models\Image;
use App\Models\Url;
use App\Repositories\ImageRepository;
use App\Repositories\Interfaces\ImageRepositoryInterface;
use App\Http\Requests\SearchRequest;
use App\Services\StorageService;
use App\Traits\JSONAPI;
use App\Traits\ManageFiles;
use App\Traits\Pagination;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class SearchController extends Controller
{
    use Pagination, JSONAPI, ManageFiles;

    const DEFAULT_PER_PAGE = 12;
    const DEFAULT_FIRST_PAGE = 1;

    public function __construct() {

    }

    public function index(SearchRequest $r) {

        $query = $r->input('filter.query');

        $client = Elasticsearch::connection();

        $params = [
            'index' => 'image_search',
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'query_string' => [
                                    'query' => $query,
                                    'fields' => [
                                        'colors^6', 'color_names^5', 'title^4', 'description^3', 'album_title^3', 'album_description^3', 'close_colors^3'
                                    ],
                                    'default_operator' => 'AND',
                                    'fuzzy_transpositions' => true,
                                    'fuzziness' => 3,
                                ]
                            ]
                        ]
                    ]
                ]

            ]
        ];

        $results = $client->search($params);

        if(!empty($results['errors'])) {
            return response()->api([
                'errors' => $results['errors']
            ], 500);
        }

        $hits = $results['hits']['hits'];
        $records = [];
        foreach($hits as $hit) {
            $hit['_source']['hex_codes'] = $hit['_source']['colors'];
            $records[] = [
                'id' => $hit['_source']['id'],
                'attributes' => $hit['_source'],
            ];
        }

        return response([
            'data' => $records,
            'meta' => [
                'cloud' => [
                    'url' => StorageService::getServingPath(),
                ],
            ]
        ], 200);
    }

}
