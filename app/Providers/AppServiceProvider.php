<?php

namespace App\Providers;

use App\Libraries\MagicAuth;
use App\Libraries\Elasticsearch;
use App\Libraries\SpacesClient;
use App\Models\Image;
use App\Services\ImageService;
use App\Traits\JSONAPI;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    use JSONAPI;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app['request']->server->set('HTTPS', true);

        $this->app->singleton(Elasticsearch::class, function($app) {
            return new Elasticsearch();
        });

        $this->app->singleton(SpacesClient::class, function($app) {
            return new SpacesClient();
        });

        $this->app->bind(ImageService::class, function ($app) {
            return new ImageService();
        });

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        Response::macro('api', function ($data) {
            if(isset($data['data'])) {
                $data['data'] = JSONAPI::formatResourceResponse($data['data']);
            }

            return $data;
        });

        $url->formatScheme('https');
    }
}
