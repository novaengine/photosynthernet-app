<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;;


class AnalysisProfile extends Model
{

    protected $table = 'analysis_profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'segments',
        'superpixel_algorithm'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function images() {
        return $this->belongsTo(Image::class, 'analysis_profile', 'id');
    }

}
