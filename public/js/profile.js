const profile = {

    $parent_container : undefined,

    current_images : undefined,

    init($parent_container) {
        const self = this;
        self.$parent_container = $parent_container;

        self.render();
    },

    render() {
        const self = this;
        const images = PImage.index(1);

        self.renderProfile(images);
    },

    renderProfile(images) {
        const self = this;

        const profile_data = User.readLoggedInUser();

        profile_data.done(function(r) {

            app.state.user.attributes = r.data;
            app.saveState();

            self.$parent_container.render('profile', r.data).done(function() {
                self.bindActions();
                self.loadAlbums();
            });
        })

    },

    loadAlbums(page) {
        const self = this;

        page = page ? page : 1;

        Album.index(page).done(function(r) {

            let pagination_data = PImage.formatPaginationLinks(r);

            console.log(pagination_data)

            let formatted_data = {
                ...r,
                ...pagination_data
            };


            $('[data-js=profile_section]').render('albums', formatted_data).done(function(r) {
                self.bindActions();
            })
        });
    },

    loadImages() {
        index.$parent_container = $('[data-js=profile_section]');
        PImage.index(1, 'private').done(function(r) {
            index.visibility = 'private';
            index.renderWithData(r);
        });
    },

    renderSettings() {
        const self = this;
        $('[data-js=profile_section]').render('profile_settings', {
            user : app.state.user.attributes
        }).done(function() {
            self.bindActions();
        });
    },

    renderApiKeys() {
        const self = this;

        Clients.index().done(function(r) {
            $('[data-js=profile_section]').render('api_keys', {
                clients : r.data
            }).done(function() {
                self.bindActions();
            });
        })

    },

    getProfileData() {
        const $profile_container = $('[data-js=profile_section]');
        const inputs = $profile_container.find('input');

        let data = {}

        $.each(inputs, function(ind, val) {
            val = $(val);
            const key = val.attr('name');
            data[key] = val.val();
        });

        return data;

    },

    updateProfile() {
        const self = this;

        const profile_data = self.getProfileData();

        return api.patch('/users/' + app.state.user.id, profile_data)
    },

    createClient() {

        const $client_name = $('[data-js="client_name"]');
        const name = $client_name.val();

        return Clients.create({
            name : name
        }).done(function(r) {
            $client_name.val('');
        })


    },

    bindActions() {
        const self = this;

        const $tabs = $('.tabs .tab');
        $tabs.unbind();
        $tabs.on('click', function() {
            const show_container = $(this).data('js');
            const $parent = $(this).closest('[data-js="tabs_parent"]')

            let $clickable_tabs = $parent.find('.tabs .tab');
            $clickable_tabs.removeClass('is-active');
            $(this).addClass('is-active');

            const $selected_container = $parent.find('[data-tab="'+show_container+'"]');

            const $all_tabs = $parent.findExclude('[data-tab]', '[data-js="tabs_parent"]');
            $all_tabs.addClass('is-hidden');

            $selected_container.removeClass('is-hidden');

            const trigger_event = $(this).data('callable');
            if(!$.isEmptyObject(trigger_event) && self[trigger_event] !== undefined) {
                self[trigger_event]();
            }

        });

        const $save_profile = $('[data-js=save_profile]');
        $save_profile.unbind();
        $save_profile.on('click', function() {
            self.updateProfile().done(function(r) {
                self.render();
            });
        });

        const $save_client = $('[data-js=save_client]');
        $save_client.unbind();
        $save_client.on('click', function() {
            self.createClient().done(function(r) {
                self.renderApiKeys();
            });
        });

        const $pagination = $('.pagination a');
        $pagination.unbind();
        $pagination.on('click', function() {
            const page = $(this).data('page');
            self.loadAlbums(page)
        })


        const $logout = $('#logout');
        $logout.unbind();
        $logout.bind('click', function (e) {
            app.logout();
        });
    },

}

modules.profile = profile;