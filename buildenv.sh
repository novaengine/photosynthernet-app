rm -rf /var/www/.env

printf "\nAPP_URL=$APP_URL" >> /var/www/.env
printf "\nAPP_NAME=Photosynthernet" >> /var/www/.env
printf "\nAPP_ENV=Production" >> /var/www/.env
printf "\nAPP_KEY=changeme" >> /var/www/.env
printf "\nAPP_DEBUG=false" >> /var/www/.env
printf "\nAPP_URL=https://k8s.photosynthernet.dev" >> /var/www/.env

printf "\nLOG_CHANNEL=stack" >> /var/www/.env

printf "\nDB_CONNECTION=pgsql" >> /var/www/.env
printf "\nDB_HOST=$DB_HOST" >> /var/www/.env
printf "\nDB_PORT=$DB_PORT" >> /var/www/.env
printf "\nDB_DATABASE=photosynthernet" >> /var/www/.env
printf "\nDB_USERNAME=$DB_USERNAME" >> /var/www/.env
printf "\nDB_PASSWORD=$DB_PASSWORD" >> /var/www/.env

printf "\nCACHE_DRIVER=$CACHE_DRIVER" >> /var/www/.env
printf "\nQUEUE_CONNECTION=redis" >> /var/www/.env

printf "\nREDIS_HOST=$REDIS_HOST" >> /var/www/.env
printf "\nREDIS_PASSWORD=null" >> /var/www/.env
printf "\nREDIS_PORT=6379" >> /var/www/.env

printf "\nELASTICSEARCH_HOST=$ELASTICSEARCH_HOST" >> /var/www/.env

printf "\nPASSPORT_PRIVATE_KEY=$PASSPORT_PRIVATE_KEY" >> /var/www/.env
printf "\nPASSPORT_PUBLIC_KEY=$PASSPORT_PUBLIC_KEY" >> /var/www/.env

printf "\nMAIL_MAILER=smtp" >> /var/www/.env
printf "\nMAIL_HOST=$MAIL_HOST" >> /var/www/.env
printf "\nMAIL_PORT=$MAIL_PORT" >> /var/www/.env
printf "\nMAIL_USERNAME=$MAIL_USERNAME" >> /var/www/.env
printf "\nMAIL_PASSWORD=$MAIL_PASSWORD" >> /var/www/.env
printf "\nMAIL_ENCRYPTION=tls" >> /var/www/.env
printf "\nMAIL_FROM_NAME=NovaReply" >> /var/www/.env
printf "\nMAIL_FROM_ADDRESS=no-reply@novaengine.ai" >> /var/www/.env


printf "\nDIGITALOCEAN_SPACES_KEY=$DIGITALOCEAN_SPACES_KEY" >> /var/www/.env
printf "\nDIGITALOCEAN_SPACES_SECRET=$DIGITALOCEAN_SPACES_SECRET" >> /var/www/.env
printf "\nDIGITALOCEAN_SPACES_ENDPOINT=$DIGITALOCEAN_SPACES_ENDPOINT" >> /var/www/.env
printf "\nDIGITALOCEAN_SPACES_REGION=$DIGITALOCEAN_SPACES_REGION" >> /var/www/.env
printf "\nDIGITALOCEAN_SPACES_BUCKET=$DIGITALOCEAN_SPACES_BUCKET" >> /var/www/.env



printf "\nBUGSNAG_API_KEY=$BUGSNAG_APIKEY" >> /var/www/.env