<?php namespace App\Traits;

use \Illuminate\Database\Eloquent\Model;

trait JSONAPI {

    public static function formatResourceResponse($resources) {
        $data = [];

        if($resources instanceof Model) {
            $item = [
                'id' => $resources->id,
                'attributes' => $resources,
            ];
            return $item;
        }

        foreach($resources as $resource) {

            $item = [
                'id' => $resource->id,
                'attributes' => $resource,
            ];

            $data[] = $item;

        }

        return $data;
    }

}
