<?php

namespace Tests\Unit;

use App\Traits\Pagination;
use Tests\TestCase;

class PaginationTest extends TestCase
{

    public function testPaginationStandardPreviousPage()
    {
        $mock = $this->getMockForTrait(Pagination::class);

        $pagination = $mock::getPagination(5, 'test.com', 500, 20);
        $this->assertArrayHasKey('page', $pagination);

        $page = $pagination['page'];
        $this->assertEquals(4, $page['prev']);
    }

    public function testPaginationPreviousPageWithCurrentAsOne()
    {
        $mock = $this->getMockForTrait(Pagination::class);

        $pagination = $mock::getPagination(1, 'test.com', 500, 20);
        $this->assertArrayHasKey('page', $pagination);

        $page = $pagination['page'];
        $this->assertEquals(1, $page['prev']);
    }

    public function testPaginationPreviousPageWithCurrentAsLast()
    {
        $mock = $this->getMockForTrait(Pagination::class);

        $pagination = $mock::getPagination(25, 'test.com', 500, 20);
        $this->assertArrayHasKey('page', $pagination);

        $page = $pagination['page'];

        $this->assertEquals(24, $page['prev']);
    }

    /**
     * If we set the current page to a number greater than the max possible, then
     * the current_page should be set to the upper bound and by consequence the previous page
     * should return the number before the last.
     *
     * @see PaginationTest::testPaginationPreviousPageWithCurrentAsLast()
     *
     */
    public function testPaginationPreviousPageWithCurrentAsInvalid()
    {
        $mock = $this->getMockForTrait(Pagination::class);

        $pagination = $mock::getPagination(27, 'test.com', 500, 20);
        $this->assertArrayHasKey('page', $pagination);

        $page = $pagination['page'];

        $this->assertEquals(24, $page['prev']);
    }

    public function testPaginationMiddleStandard()
    {
        $mock = $this->getMockForTrait(Pagination::class);

        $pagination = $mock::getPagination(5, 'test.com', 500, 20);
        $this->assertArrayHasKey('page', $pagination);

        $page = $pagination['page'];

        $this->assertEquals([4, 5, 6], $page['middle']);
    }

    /**
     * Since we are on the first page the "middle" pages should only be 2 and 3,
     * since the first page has a reserved property under "first", we dont want
     * to duplicate it in the middle property
     */
    public function testPaginationMiddleFirstPage()
    {
        $mock = $this->getMockForTrait(Pagination::class);

        $pagination = $mock::getPagination(1, 'test.com', 500, 20);
        $this->assertArrayHasKey('page', $pagination);

        $page = $pagination['page'];

        $this->assertEquals([2, 3], $page['middle']);
    }

    /**
     * If we only have 1 page, then there are no "middle" pages and the middle property should be empty
     */
    public function testPaginationMiddleLowerBound()
    {
        $mock = $this->getMockForTrait(Pagination::class);

        $pagination = $mock::getPagination(1, 'test.com', 20, 20);
        $this->assertArrayHasKey('page', $pagination);

        $page = $pagination['page'];

        $this->assertEmpty($page['middle']);
    }

    /**
     * On the second page we still do not want to show the first page as part of the
     * "middle" grouping, since again it has a reserved location in the "first" property
     * In this case the middle property should only contain the second and third pages
     */
    public function testPaginationMiddleSecondPage()
    {
        $mock = $this->getMockForTrait(Pagination::class);

        $pagination = $mock::getPagination(2, 'test.com', 500, 20);
        $this->assertArrayHasKey('page', $pagination);

        $page = $pagination['page'];

        $this->assertEquals([2, 3], $page['middle']);
    }

    /**
     * On the third page we should be back to normal, the "middle" grouping should have a length of
     * 3 and should contain no reserved pages (like first)
     */
    public function testPaginationMiddleThirdPage()
    {
        $mock = $this->getMockForTrait(Pagination::class);

        $pagination = $mock::getPagination(3, 'test.com', 500, 20);
        $this->assertArrayHasKey('page', $pagination);

        $page = $pagination['page'];

        $this->assertEquals([2, 3, 4], $page['middle']);
    }

    /**
     * Similar to the first page test, the last page has a reserved location in the
     * "last" property, so it is not included in the middle grouping.
     * The middle should contain the 2 pages prior to the last in this case.
     */
    public function testPaginationMiddleLastPage()
    {
        $mock = $this->getMockForTrait(Pagination::class);

        $pagination = $mock::getPagination(25, 'test.com', 500, 20);
        $this->assertArrayHasKey('page', $pagination);

        $page = $pagination['page'];

        $this->assertEquals([23, 24], $page['middle']);
    }

}
