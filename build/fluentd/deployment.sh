# Authenticate with GKE
#echo "$SERVICE_ACCOUNT_KEY" > key.json
#gcloud auth activate-service-account --key-file=key.json
#gcloud container clusters get-credentials ps-app-cluster --zone us-west2-b --project gleaming-cove-237218

#kubectl create secret docker-registry regcred --docker-server=https://index.docker.io/v1/ --docker-username=${DOCKER_USER} --docker-password=${DOCKER_PASSWORD} --docker-email=valeness84@gmail.com

# Do the deployment
cat build/fluentd/deployment.yml | envsubst | kubectl apply -f -
