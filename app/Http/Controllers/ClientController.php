<?php

namespace App\Http\Controllers;

use App\Exceptions\UnauthorizedException;
use App\Http\Requests\AnalysisProfileRequest;
use App\Models\Client;
use App\Traits\JSONAPI;
use App\Traits\Pagination;
use App\User;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\ClientRepository;

class ClientController extends Controller
{
    use JSONAPI, Pagination;

    public function index(Request $request, $id){

        $user = User::where('id', $id)->first();
        $authed_user = Auth::user();

        if(empty($user) || $user->id != $authed_user->id) {
            throw new UnauthorizedException('Unauthorized to fetch clients for this user');
        }

        $user_id = Auth::user()->id;
        $data = Client::where('user_id', $user_id)->get();

        return response()->json([
            'data' => $data
        ], Response::HTTP_OK);
    }

    public function read(Request $request, $id){

        return response([], Response::HTTP_NOT_IMPLEMENTED);
    }

    public function create(Request $r, $id) {

        $user = User::where('id', $id)->first();
        $authed_user = Auth::user();

        if(empty($user) || $user->id != $authed_user->id) {
            throw new UnauthorizedException('Unauthorized to create clients for this user');
        }

        $name = $r->input('data.name') ?? "Random Name";

        $repository = new ClientRepository();
        $client = $repository->create(Auth::user()->id, $name, 'https://google.com', null, true);

        return response()->json([
            'data' => [
                'id' => $client->id,
                'secret' => $client->secret,
            ]
        ], Response::HTTP_CREATED);
    }

    public function update(Request $r, $id) {

        return response([], Response::HTTP_NOT_IMPLEMENTED);
    }

    public function delete(Request $r, $id) {

        return response([], Response::HTTP_NOT_IMPLEMENTED);
    }

}
