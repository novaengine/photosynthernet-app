const forgot = {

    $parent_container: undefined,

    current_images: undefined,

    init($parent_container) {
        const self = this;
        self.$parent_container = $parent_container;

        $('.main-container').addClass('fancy-bg');

        self.render(self.settings);
    },

    hasToken() {
        const self = this;
        return self.settings.token !== undefined;
    },

    render(data) {
        const self = this;

        data = data || {};

        self.$parent_container.render('forgot', data).done(function() {
            self.bindActions();
        });
    },

    bindActions() {
        const self = this;

        const $reset = $('[data-js="reset"]');
        $reset.unbind();
        $reset.on('click', function() {

            const $sel = $(this);

            $sel.loading(true);


            if(self.hasToken()) {
                self.performPasswordUpdate().done(function(r) {
                    $sel.loading(false);
                }).catch(function() {
                    $sel.loading(false);
                });
            } else {
                self.performReset().done(function(r) {
                    $sel.loading(false);
                }).catch(function() {
                    $sel.loading(false);
                });
            }

        })



        const $inputs = $('[name=email],[name=password],[name=password_confirmation]');
        $inputs.unbind();
        $inputs.on('keyup', function(e) {

            const $sel = $reset;

            if(e.which === 13) {
                $sel.loading(true);
                if(self.hasToken()) {
                    self.performPasswordUpdate().done(function(r) {
                        $sel.loading(false);
                    }).catch(function() {
                        $sel.loading(false);
                    });
                } else {
                    self.performReset().done(function(r) {
                        $sel.loading(false);
                    }).catch(function() {
                        $sel.loading(false);
                    });
                }
            }
        })

    },

    getData() {
        const email = $('[name=email]').val();

        const password = $('[name=password]').val();
        const password_confirmation = $('[name=password_confirmation]').val();

        return {
            email : email,

            password : password,
            password_confirmation : password_confirmation
        }
    },

    performPasswordUpdate() {
        const self = this;

        const data = self.getData();

        User.update(self.settings.user_id, {
            data : data,
            meta : {
                reset_token : self.settings.token,
            }
        }).done(function(r, success, xhr) {
            let payload = xhr.responseJSON;
            payload.status = xhr.status;
            payload.redirect_to_login = true;

            self.render(payload)
        });
    },

    performReset() {
        const self = this;
        const data = self.getData();

        const prms = User.reset(data.email);

        prms.done(function(r, success, xhr) {
            let payload = xhr.responseJSON;
            payload.email = data.email;
            payload.status = xhr.status;

            self.render(payload)
        });

        return prms;
    }

}

modules.forgot = forgot;